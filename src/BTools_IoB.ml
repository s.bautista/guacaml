(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * BTools_IoB [INTERNAL] : type-oriented facility for the conversion to
 *   and from basic types.
 *)

type 'a o3b = ('a, BTools_BArray.t) O3.o3
type 'a o3s = ('a, BinUtils.stream) O3.o3s

let unit = (BTools_ToB.unit, BTools_OfB.unit)
let option (dump, load) = (BTools_ToB.option dump, BTools_OfB.option load)
let bool = (BTools_ToB.bool, BTools_OfB.bool)
let barray = (BTools_ToB.barray, BTools_OfB.barray)
let list (dump, load) = (BTools_ToB.list dump, BTools_OfB.list load)
let array (dump, load) = (BTools_ToB.array dump, BTools_OfB.array load)
let none_list (dump, load) = (BTools_ToB.none_list dump, BTools_OfB.none_list load)
let int = (
  BTools_ToB.int,
   BTools_OfB.int
)
let sized_int size = (
  BTools_ToB.sized_int size,
   BTools_OfB.sized_int size
)
let closure ((dump, load) : ('a, 's) O3.o3s) : ('a, BTools_BArray.t) O3.o3 =
  (BTools_ToB.closure dump, BTools_OfB.closure load)

let bool_option_list = (
  BTools_ToB.bool_option_list,
   BTools_OfB.bool_option_list
)

let pair (dA, lA) (dB, lB) = (
  BTools_ToB.pair dA dB,
   BTools_OfB.pair lA lB
)

let ( * ) = pair

let trio (dA, lA) (dB, lB) (dC, lC) = (
  BTools_ToB.trio dA dB dC,
   BTools_OfB.trio lA lB lC
)

let quad (dA, lA) (dB, lB) (dC, lC) (dD, lD) = (
  BTools_ToB.quad dA dB dC dD,
   BTools_OfB.quad lA lB lC lD
)

let c2 (d0, l0) (d1, l1) = (
  BTools_ToB.c2 d0 d1,
   BTools_OfB.c2 l0 l1
)
let c3 (d0, l0) (d1, l1) (d2, l2) = (
  BTools_ToB.c3 d0 d1 d2,
   BTools_OfB.c3 l0 l1 l2
)
let c4 (d0, l0) (d1, l1) (d2, l2) (d3, l3) = (
  BTools_ToB.c4 d0 d1 d2 d3,
   BTools_OfB.c4 l0 l1 l2 l3
)
