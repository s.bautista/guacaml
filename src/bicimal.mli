type t = int list
(* positive bicimal *)
(* sorted by increasing order *)
(* [[t]] = sum_{x\in t} 2^x *)

val add_single : ?strict:bool -> int -> t -> t
val add : ?strict:bool -> t -> t -> t
(*  [normalize il = t]
    where [il] is an unsorted list of signed integers
 *)
val normalize : ?sorted:bool -> int list -> t
