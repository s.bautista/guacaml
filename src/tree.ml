(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Tree : Generic implementation of n-ary trees
 *)

type 'a tree =
  | Leaf of 'a
  | Node of 'a tree list

(* [TODO] rewrite 'a tree = (unit, 'a) atree *)

(* Annotated Trees *)
type ('a, 'b) atree =
  | ALeaf of 'b
  | ANode of 'a * (('a, 'b) atree list)

type ('a, 'b) atrees = ('a, 'b) atree list

type 'a aatree = ('a, 'a) atree
type 'a aatrees = 'a aatree list

type stree = string tree
type 'a to_stree = 'a -> stree
type 'a of_stree = stree -> 'a

(* binary trees *)
type 'a btree =
  | BLeaf of 'a
  | BNode of ('a btree) * ('a btree)

type ('leaf, 'link) gnext =
  | GLeaf of 'leaf
  | GLink of 'link
