(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * BTools_OfB [INTERNAL] : type-based conversion from [stream:=bool list]
 *   to basic types
 *)

type stream = BinUtils.stream
type 'a t = 'a BinUtils.load

let unit : unit BinUtils.load = fun stream -> (), stream

let sized_bool_list (n:int) (s:stream) = MyList.hdtl_nth n s
let sized_unit_list (n:int) (s:stream) =
  (MyList.make n (), s)

let map f load stream =
  let item, stream = load stream in
  (f item), stream

let c2 load0 load1 = function
  | false::stream -> Poly.(
    let elem, stream = load0 stream in
    (C2_0 elem), stream
  )
  | true ::stream -> Poly.(
    let elem, stream = load1 stream in
    (C2_1 elem), stream
  )
  | _ -> failwith "[GuaCaml/OfB:c2] parsing error"

let c3 load0 load1 load2 = function
  | false::stream -> Poly.(
    let elem, stream = load0 stream in
    (C3_0 elem), stream
  )
  | true ::false::stream -> Poly.(
    let elem, stream = load1 stream in
    (C3_1 elem), stream
  )
  | true ::true ::stream -> Poly.(
    let elem, stream = load2 stream in
    (C3_2 elem), stream
  )
  | _ -> failwith "[GuaCaml/OfB:c3] parsing error"

let c4 load0 load1 load2 load3 = function
  | false::false::stream -> Poly.(
    let elem, stream = load0 stream in
    (C4_0 elem), stream
  )
  | false::true ::stream -> Poly.(
    let elem, stream = load1 stream in
    (C4_1 elem), stream
  )
  | true ::false::stream -> Poly.(
    let elem, stream = load2 stream in
    (C4_2 elem), stream
  )
  | true ::true ::stream -> Poly.(
    let elem, stream = load3 stream in
    (C4_3 elem), stream
  )
  | _ -> failwith "[GuaCaml/OfB:c4] parsing error"

let choice loadF loadT = function
  | b::stream -> (if b then loadT else loadF) stream
  | _      -> failwith "[GuaCaml/OfB:choice] parsing error"

let option load = choice
  (fun stream -> None, stream)
  (fun stream ->
    let x, stream = load stream in
    Some x, stream)

(* copied from MyList *)
let hdtl_nth n liste =
  let rec aux carry = function
    | 0, next -> (List.rev carry, next)
    | n, [] -> (failwith "[GuaCaml.BTools_OfB] error")
    | n, head::tail -> aux (head::carry) (n-1, tail)
  in
  assert (n>=0);
  assert ((List.length liste)>=n);
  let head, tail = aux [] (n, liste) in
  assert((List.length head)=n);
  head, tail

let sized_barray size stream =
  let x, stream = hdtl_nth size stream in
  (BTools_BArray.of_bool_list x, stream)

let none_list load =
  let rec aux carry stream =
    let opelem, stream = load stream in
    match opelem with
    | None -> carry, stream
    | Some elem -> aux (elem::carry) stream
  in aux []

let while_list func init =
  let rec aux carry state stream =
    let op_elem_op_state, stream = func stream in
    match op_elem_op_state with
      | None -> (List.rev carry), stream
      | Some (elem, op_state) -> match op_state with
        | None -> (List.rev (elem::carry)), stream
        | Some state -> aux (elem::carry) state stream
  in aux [] init

let sized_list (load : 'a BinUtils.load) (size : int) : 'a list BinUtils.load =
  let rec aux carry stream = function
    | 0 -> carry, stream
    | n ->
    (
      let head, stream = load stream in
      aux (head::carry) stream (n-1)
    )
  in (fun stream -> aux [] stream size)

let list (load : 'a BinUtils.load) : 'a list BinUtils.load=
  let rec aux carry = function
    | false::stream  -> carry, stream
    | true::stream  ->
      let elem, stream = load stream in
      aux (elem::carry) stream
    | _      -> failwith "[GuaCaml/OfB:list] parsing error"
  in aux []

let bool : bool BinUtils.load = function
  | b::stream -> (b, stream)
  | _         -> failwith "[GuaCaml/OfB:bool] parsing error"

let unary : int BinUtils.load =
  let rec aux n = function
    | [] -> failwith "[GuaCaml/OfB:unary] parsing error"
    | head::stream -> if head then (n, stream) else (aux (n+1) stream)
  in aux 0

let sized_int size : int BinUtils.load = fun stream ->
  let l, stream = hdtl_nth size stream in
  let rec aux x = function
    | [] -> x
    | head::tail -> aux (x*2 + (if head then 1 else 0)) tail
  in (aux 0 l, stream)

let int_legacy : int BinUtils.load = fun stream ->
  let k, stream = unary stream in
  sized_int k stream

(* [int_vanilla_aux carry stream = (n, stream')]
 *)
let rec int_vanilla_rec ?(c=1) s : int * bool list =
  match s with
  | true::s -> (c, s)
  | false::b::s -> (
    int_vanilla_rec ~c:((if b then 1 else 0) lor (c lsl 1)) s
  )
  | _ -> failwith "[GuaCaml/OfB.int_vanilla_rec] parsing error"

let int_vanilla s : int * bool list =
  let b, s = bool s in
  if b then (0, s) else int_vanilla_rec s

(* let int = int_legacy *)
let int = int_vanilla

let swrap text tos tob n0 s =
  print_endline (text^" [0] n0:"^(tos n0));
  print_string (text^" [0] s :"); STools.SUtils.print_stream s; print_newline();
  let n, s = tob n0 s in
  print_string (text^" [1] s :"); STools.SUtils.print_stream s; print_newline();
  print_endline (text^" [1] n :"^(tos n));
  (n, s)

let unary_le top s =
  let rec unary_le_rec c top s =
    if top = 0
    then (c, s)
    else match s with
      | [] -> (failwith "[GuaCaml.BTools_OfB] error")
      | true ::s -> (c, s)
      | false::s -> (unary_le_rec (succ c) (pred top) s)
  in unary_le_rec 0 top s

let rec sized_bool_list_le ?(carry=[]) top s =
  match top with
  | [] -> (List.rev carry, s)
  | t0::top ->
    if t0
    then (
      let s0, s = bool s in
      let carry = s0 :: carry in
      match s0 with
      | true  -> sized_bool_list_le ~carry top s
      | false -> (
        let bl, s = sized_bool_list (List.length top) s in
        (List.rev_append carry bl, s)
      )
    )
    else (sized_bool_list_le ~carry:(false::carry) top s)

let rec exactly_sized_int_rec ?(carry=0) ?(exp=0) n s =
  if n <= 1
  then (carry lor (1 lsl exp), s)
  else (
    let s0, s = bool s in
    let carry = if s0
      then (carry lor (1 lsl exp))
      else  carry
    in
    exactly_sized_int_rec ~carry ~exp:(succ exp) (pred n) s
  )

let rec exactly_sized_int n s =
  if n <= 0
  then (assert(n=0); (0, s))
  else (exactly_sized_int_rec n s)

let rec sized_int_smart ?(c=0) (i:int) (s:stream) : int * stream =
  if i < 0
  then (c, s)
  else (
    let n0, s = MyList.hdtl s in
    let c = if n0 then (c lor (1 lsl i)) else c in
    sized_int_smart ~c (pred i) s
  )

let rec sized_int_le_smart ?(c=0) (i:int) (top:int) (s:stream) : int * stream =
  if i < 0
  then (c, s)
  else (
    let t0 = top land (1 lsl i) <> 0 in
    if t0
    then (
      let n0, s = MyList.hdtl s in
      let c = if n0 then (c lor (1 lsl i)) else c in
      if n0
      then sized_int_le_smart ~c (pred i) top s
      else sized_int_smart    ~c (pred i)     s
    )
    else (sized_int_le_smart ~c (pred i) top s)
  )

let int_le top s =
  match top with
  | 0 -> (0, s)
  | 1 -> (
    let s0, s = bool s in
    ((if s0 then 1 else 0), s)
  )
  | _ -> (
    let n0 = Tools.math_log2 top in
    let n, s = unary_le n0 s in
    if n = n0
    then (
      assert(n > 0);
      sized_int_le_smart ~c:(1 lsl (n-1)) (n-2) top s
    )
    else (
      exactly_sized_int n s
    )
  )

let int' (min:int) (max:int) (s:stream) : int * stream =
  assert(min <= max);
  let x, s = int_le (max-min) s in
  (x+min, s)

let signed_int (s:stream) : int * stream =
  let b, s = bool s in
  let x, s = int s in
  ((if b then (-1-x) else x), s)

let barray stream =
  let s, stream = int stream in
  let x, stream = sized_barray s stream in
  (x, stream)

let pair (loadA : 'a BinUtils.load) (loadB : 'b BinUtils.load) : ('a * 'b) BinUtils.load = fun stream ->
  let a, stream = loadA stream in
  let b, stream = loadB stream in
  (a, b), stream

let ( * ) = pair

let trio loadA loadB loadC stream =
  let a, stream = loadA stream in
  let b, stream = loadB stream in
  let c, stream = loadC stream in
  (a, b, c), stream

let quad loadA loadB loadC loadD stream =
  let a, stream = loadA stream in
  let b, stream = loadB stream in
  let c, stream = loadC stream in
  let d, stream = loadD stream in
  (a, b, c, d), stream

let closure (load : 'a BinUtils.load) bitv : 'a =
  let objet, stream = bitv |> BTools_BArray.to_bool_list |> load in
  assert(stream = []);
  objet

let list (load : 'a BinUtils.load) : 'a list BinUtils.load = fun stream ->
  let size, stream = int stream in
  sized_list load size stream

let array (load : 'a BinUtils.load) : 'a array BinUtils.load = fun stream ->
  let liste, stream = list load stream in
  (Array.of_list liste, stream)

let bool_option_list = none_list (function
  | b0::b1::stream ->
  ((
    if b0
    then if b1
      then None
      else (Some None)
    else (Some(Some b1))
  ), stream)
  | _ -> failwith "[GuaCaml/OfB:unary] parsing error")

let o3 = snd

(* assumes [sum = \sum_{i\in il} i] *)
(* assumes \forall i\in il, min <= i *)
let rec sum_list_rec ?(carry=[]) ?(min=0) (len:int) (sum:int) (s:stream) : int list * stream =
  assert(len > 0);
  if len = 1
  then (List.rev(sum::carry), s)
  else (
    let max = sum - (Stdlib.( * ) min (len-1)) in
    let i, s = int' min max s in
    sum_list_rec ~carry:(i::carry) ~min (pred len) (sum-i) s
  )

let sum_list ?(min=0) (sum:int) (s:stream) : int list * stream =
  if min > 0
  then (
    if sum = 0
    then ([], s)
    else (
      let len, s = int' 1 (sum/min) s in
      sum_list_rec ~min len sum s
    )
  )
  else (
    let len, s = int s in
    sum_list_rec ~min len sum s
  )
