(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GGLA PMC : detection and extraction of PMC
 *
 * === CONTRIBUTORS ===
 *
 * Joan Thibault
 *  - joan.thibault@irisa.fr
 * Maxime Bridoux
 *  - maxime.bridoux@ens-rennes.fr
 *)

(* [connected_components_exclusion hg idents = [(component, excluded, is_full)]]
    (the notion of non-suppressible node is ignore for computation)
    [TODO] [??] deal with ft-vertices
        (* [??] G = (V u L, E, w) an H-FT-Graph
        we say that K \subset V is an MKVM of G iff
        K u L is an MKVM of G' = (V u L, E u L^2, w) an H-Graph *)
    (we assume [SetList.sorted idents])
    returns a list of triplet, each triplet correspond to one connected component of G\K :
    1. component : list of vertices C
    2. excluded : set S of vertices of K which are adjacent to C
    3. is_full : 'True' iff the component is full (i.e., S = K) *)
val connected_components_exclusion : GraphHFT.Type.hg -> int list -> (int list * int list * bool) list

(* [is_pmc hg idents = bool]
    returns true iff [idents] (i.e., K) is a pmc in the graph [hg] (i.e., G) *)
val is_pmc : GraphHFT.Type.hg -> int list -> bool
