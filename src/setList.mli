(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * SetList : sorted-list-based implementation of sets
 *)

(* return true iff the input list is sorted (i.e. strictly increasing)*)
(* Time Complexity O(n) *)
val sorted :
  ?strict:bool -> (* = true *)
  ?next:(('a option -> bool)option) ->
  'a list -> bool
(* return true iff the input list is sorted (i.e. strictly increasing)
   and all elements are positive
 *)
(* Time Complexity O(n) *)
val sorted_nat :
  ?strict:bool -> (* = true *)
  ?next:((int option -> bool)option) ->
  int list -> bool
val sort : 'a list -> 'a list

(* merges two already sorted list and returns a sorted list *)
(* removes duplicates *)
(* Time Complexity O(nX + nY) *)
(* Tail-Recursive *)
val union : 'a list -> 'a list -> 'a list

(* merges a list of already sorted list and returns a sorted list *)
(* removes duplicates *)
(* Time Complexity O(\sum_i |l_i|) *)
(* Tail-Recursive *)
val union_list : 'a list list -> 'a list

(* returns [lX] - [lY] *)
(* Time Complexity O(nX+nY) *)
val minus : 'a list -> 'a list -> 'a list

(* return [lX] inter [lY] *)
(* Time Complexity O(nX+nY) *)
val inter : 'a list -> 'a list -> 'a list

(* returns (lX inter lY) = emptyset *)
(* Time Complexity O(nX+nY) *)
val nointer : 'a list -> 'a list -> bool

(* return true iff [lX] is a subset of [lY] *)
(* Time Complexity O(nX+nY) *)
val subset_of : 'a list -> 'a list -> bool

(* Section. Fst Cmp SetList *)

(* [union_fst_replace l1 l2 = l]
    if fst-equal, elements of [l2] replace elements of [l1]
 *)
val union_fst_replace : ?carry:('a*'b)list ->
  ('a*'b)list -> ('a*'b)list -> ('a*'b)list

(* [union_fst_stable  l1 l2 = l]
    if fst-equal, elements of [l2] are put after
    elements of [l1]
 *)
val union_fst_stable  : ?carry:('a*'b)list ->
  ('a*'b)list -> ('a*'b)list -> ('a*'b)list

(* [union_fst ~replace] =
    if [replace]
    then [union_fst_replace]
    else [union_fst_stable]

    by default, [replace = false]
 *)
val union_fst: ?replace:bool -> ?carry:('a*'b)list ->
  ('a*'b)list -> ('a*'b)list -> ('a*'b)list
