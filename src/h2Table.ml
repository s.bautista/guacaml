(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * H2Table : implementation of unique-tables for hash-consing
 *)

open Extra
open STools

let default_size = 10_000

type 'a t = {
  access : ('a, int) Hashtbl.t;
  revers : (int, 'a) Hashtbl.t;
  mutable index : int;
}

let create n index =
  {access = Hashtbl.create n; revers = Hashtbl.create n; index}

let memA tbl item = Hashtbl.mem tbl.access item
let memI tbl indx = Hashtbl.mem tbl.revers indx

let push tbl item =
  try
    Hashtbl.find tbl.access item
  with Not_found -> (
    let index = tbl.index in
    tbl.index<-tbl.index+1;
    Hashtbl.add tbl.access item index;
    Hashtbl.add tbl.revers index item;
    index
  )

let push' tbl item = Hashtbl.find tbl.access item

let pull tbl indx =
  try Hashtbl.find tbl.revers indx
  with Not_found -> (
    failwith "[GuaCaml.H2Table.pull] - undefined index"
  )

let length tbl =
  Hashtbl.length tbl.access

let iter tbl fonc =
  Hashtbl.iter fonc tbl.access

let map tbl fonc =
  let stack = ref [] in
  Hashtbl.iter (fun key obj -> stack_push stack (fonc key obj)) tbl.revers;
  !stack

let mapreduce tbl init map reduce =
  let stack = ref init in
  let push obj = stack := reduce obj !stack in
  Hashtbl.iter (fun key obj -> push(map key obj)) tbl.revers;
  !stack

let to_stree (dumpA : 'a -> Tree.stree) (tbl : 'a t) : Tree.stree =
  let stack = ref [] in
  let push obj = stack := obj::(!stack) in
  Hashtbl.iter (fun key objA -> push (Tree.Node [ToSTree.int key; dumpA objA])) tbl.revers;
  Tree.Node ((ToSTree.int tbl.index)::(!stack))

let of_stree hsize (loadA : Tree.stree -> 'a) : Tree.stree -> 'a t = function
  | Tree.Node (index::table) -> (
    let index = OfSTree.int index in
    let access = Hashtbl.create hsize
    and revers = Hashtbl.create hsize in
    List.iter (function
      | Tree.Node [index; item] ->
      (
        let index = OfSTree.int index
        and item  = loadA item in
        Hashtbl.add access item index;
        Hashtbl.add revers index item;
      )
      | _ -> (failwith "[GuaCaml.H2Table.of_stree] error")) table;
    {access; revers; index}
  )
  | _ -> (failwith "[GuaCaml.H2Table.of_stree] error")

open BTools

module ToBStream =
struct
  open ToBStream

  (* [TODO] implement ~destruct=true *)
  let h2table ?(destruct=false) (bwa:'a bw) (cha:Channel.t) tbl : unit =
    int cha tbl.index;
    int cha (Hashtbl.length tbl.revers);
    Hashtbl.iter
      (fun key obj -> int cha key; bwa cha obj)
      tbl.revers;
    ()
end

module OfBStream =
struct
  open OfBStream

  let h2table ?(hsize=default_size) (bra:'a br) (cha:Channel.t) =
    let index = int cha in
    let length = int cha in
    let hsize = max hsize length in
    let access = Hashtbl.create hsize
    and revers = Hashtbl.create hsize in
    Tools.ntimes length (fun () ->
      let key = int cha in
      let obj = bra cha in
      Hashtbl.add access obj key;
      Hashtbl.add revers key obj;
    );
    {access; revers; index}
end

let bw = ToBStream.h2table
let br = OfBStream.h2table

let keep_clean (ht:'a t) (alive:int list) : unit =
  assert(SetList.sorted ~strict:true alive);
  let halive = Hashtbl.create (List.length alive) in
  List.iter (fun index -> Hashtbl.add halive index ()) alive;
  Hashtbl.filter_map_inplace
    (fun _ index -> if Hashtbl.mem halive index then Some index else None)
    ht.access;
  Hashtbl.filter_map_inplace
    (fun index node -> if Hashtbl.mem halive index then Some node else None)
    ht.revers;
  ()

module ToF =
struct
  open Io.ToF

  let h2table (tof:'a t) (cha:stream) tbl : unit =
    int cha tbl.index;
    int cha (Hashtbl.length tbl.revers);
    Hashtbl.iter
      (fun key obj -> int cha key; tof cha obj)
      tbl.revers;
    ()
end

module OfF =
struct
  open Io.OfF

  let h2table ?(hsize=default_size) (off:'a t) (cha:stream) : _ =
    let index = int cha in
    let length = int cha in
    let hsize = max hsize length in
    let access = Hashtbl.create hsize in
    let revers = Hashtbl.create hsize in
    Tools.ntimes length (fun () ->
      let key = int cha in
      let obj = off cha in
      Hashtbl.add access obj key;
      Hashtbl.add revers key obj;
    );
    {access; revers; index}
end
