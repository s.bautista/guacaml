# LGPL-3.0 Linking Exception
#
# Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
#
# GuaCaml : Generic Unspecific Algorithmic in OCaml
#
# format.sh : simpl format script for OCaml code
# - remove consecutive empty lines
# - remove trailing spaces
# - replace '\t' by ' '

targets1="$(grep -Rl $'\t' ./ | grep -v git | grep -v "_build") \
          $(grep -Rl  " $" ./ | grep -v git | grep -v "_build")"
targets2="$(echo $targets1 | tr ' ' '\n' | sort -u)"
targets3="$(echo $targets2 | tr ' ' '\n' | grep "\.ml$") \
          $(echo $targets2 | tr ' ' '\n' | grep "\.mli$")"
for file in $targets3
do
  echo "reformat: " $file
  python format.py $file
done
