(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * H2Array : efficient implementation of unique tables for hash-consing
 *)

open BTools

type t
val default_size : int
(**
create hsize index
@ hsize : the size of the hash-table
@ index : index of the first element
**)
val create : ?h_size:int -> ?a_size:int -> ?index:int -> ?state:int -> unit -> t
val copy : t -> t
val get_index : t -> int
val get_state : t -> int -> int option
val set_state : t -> int -> int -> unit
val fill_state : t -> int -> unit

val memA   : t -> BArray.t -> bool
val memI   : t -> int -> bool
val push   : t -> ?copy:bool -> BArray.t -> int
(* same as [push] but raise an error if the object does not exists *)
val push'  : t -> ?copy:bool -> BArray.t -> int
val pull   : t -> ?copy:bool -> int -> BArray.t
val length : t -> int
val iter   : t -> ?copy:bool -> (BArray.t -> int -> unit) -> unit
val map    : t -> ?copy:bool -> (BArray.t -> int -> 'b  ) -> 'b list
val mapreduce : t -> 'c -> ?copy:bool -> (BArray.t -> int -> 'b) -> ('b -> 'c -> 'c) -> 'c
val filter_map_inplace : t -> ?copy_fst:bool -> ?copy_snd:bool -> (int -> int -> BArray.t -> BArray.t option) -> unit

val to_stree : t Tree.to_stree
val of_stree : t Tree.of_stree
(* can be used for debug purpose (not storage) *)
val to_stree_retro : t Tree.to_stree

open BTools

val bw : ?destruct:bool -> t bw
val br :                   t br

(* [keep_clean t il] where [SetList.sorted ~strict:true il = true]
   undefined behaviour if [il] are not valid identifiers *)
val keep_clean : t -> ?hr:bool -> int list -> unit
val keep_clean_barray : t -> ?hr:bool -> BArray.t -> unit
val keep_clean_smart  : ?hr:bool -> t -> unit

(*  [compact_smart t = tc] uses the state field as input and output
    input state  : state
    output state : state'
      state.(i) <  0 => dot not copy this node
                     => state'.(i) = state.(i)
      state.(i) >= 0 => copy this node
                     => state'.(i) >= 0
                     => tc.(state'.(i)) = t.(i)
                     => tc.(state'.(i)).state = i
    \forall i. t'.(i) = t.(i)
 *)
val compact_smart : ?hr:bool -> t -> ?copy_fst:bool -> ?copy_snd:bool -> (int -> int -> BArray.t -> BArray.t) -> t

(* [compact_fmap t map = ia] where
    - [t'] is the state of [t] after applying [compact_map]
    - [ia.(i)] is the new index of the old index [i]
      => ia.(i) = -1 means that the node has been discarded (or was Empty)
    - [Array.length ia = t.index]
    - [t'.index = t.length]
    - [t'.length = t.length]
 *)
val compact_fmap : t -> ?hr:bool -> ?copy_fst:bool -> ?copy_snd:bool -> (int -> int -> BArray.t -> BArray.t option) -> int array

val sort : t -> ?hr:bool -> unit -> int array

module ToF :
sig
  val h2array : t Io.ToF.t
end

module OfF :
sig
  val h2array : t Io.OfF.t
end
