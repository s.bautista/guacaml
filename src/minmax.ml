(*
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * minmax : Minmax algorithm for two player games.
 *
 * LGPL-3.0 Linking Exception
 *
 * Copyright 2021 Univ Rennes
 *
 * @author Santiago Bautista <Santiago.Bautista@ens-rennes.fr>
 *)


(** The signature expected for games between a "player" and an "oponent". *)
module type Game = sig
  (** Type for ongoing matches. *)
  type t

  (** [players_turn match] is true if it's the player's turn,
      and false if it's the oponent's turn.
  *)
  val players_turn : t -> bool
  
  (** Starting position (i.e. empty match) *)
  val initial: unit -> t
  
  (** Type for individual moves *)
  type move

  (** [play move match] advances in match [match] by performing
      move [move].
  *)
  val play: move -> t -> t

  (** In order to be able to take maps of games, and do memoization. *)
  val compare : t -> t -> int


  (** [eval match] is [Some(n)] if the game
      has ended and the result is [n].

      If [n] is 0, the match is a draw.
      If [n] is positive, the player wins.
      If [n] is negative, the oponent wins.
  *)
  val eval : t -> int option

  (** [next_moves match] returns a list of all
      possible moves in the current situation.
  *)
  val next_moves : t -> move list

  (** Some printing functions. *)

  (** Pretty printer for games. *)
  val pp: Format.formatter -> t -> unit

  (** Pretty printer for moves. *)
  val pp_move: Format.formatter -> move -> unit
end

module Make (G: Game) = struct
  open G

  type strategy =
    {
      suggested_move : move option;
      expected_score : int;
    }
  
  let rec minmax (t : G.t) : strategy =
    match eval t with
    | Some score -> { expected_score = score; suggested_move = None}
    | None ->
      let moves = next_moves t in
      if moves = []
      then
        let () =
          Format.fprintf
          Format.str_formatter  
          "Game is blocked:@ @[%a@]"
          pp t
        in
        let err_msg = Format.flush_str_formatter () in
        failwith err_msg
      else if players_turn t
      then
        List.fold_left
          (fun old_strategy move ->
             let new_strategy = minmax (play move t) in
             if new_strategy.expected_score > old_strategy.expected_score
             then {new_strategy with suggested_move = Some(move)}
             else old_strategy
          )
          { suggested_move = None; expected_score = min_int }
          moves
      else
        List.fold_left
          (fun old_strategy move ->
             let new_strategy = minmax (play move t) in
             if new_strategy.expected_score < old_strategy.expected_score
             then {new_strategy with suggested_move = Some(move)}
             else old_strategy
          )
          { suggested_move = None; expected_score = max_int }
          moves

  let pp_strategy (ppf: Format.formatter) (s: strategy) : unit =
    match s.suggested_move with
    | None ->
      Format.fprintf
        ppf
        "Game has ended and you scored %d"
        s.expected_score
    | Some move ->
      Format.fprintf
        ppf
        "@[If you move at@ @[%a@],@]@ @[you can expect to score @[%d@]@]@."
        pp_move move
        s.expected_score
    
end
