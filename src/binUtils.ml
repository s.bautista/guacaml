(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * BinUtils : generic type for handling binary streams
 *)

type stream  = bool list
type 't dump = 't -> stream -> stream
type 't load = stream -> 't  * stream
type 't o3s  = ('t, stream) O3.o3s
type 't o3b  = ('t, BTools_BArray.t) O3.o3
