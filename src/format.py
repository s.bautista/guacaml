# LGPL-3.0 Linking Exception
#
# Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
#
# GuaCaml : Generic Unspecific Algorithmic in OCaml
#
# format.py : simpl format script for OCaml code
# - remove consecutive empty lines
# - remove trailing spaces
# - replace '\t' by ' '

import sys
import subprocess

def do(target) :
  FILE = open(target, 'r')
  TEXT0 = FILE.read()
  TEXT = TEXT0
  FILE.close()
  TEXT = TEXT.replace('\t', '  ')
  while TEXT.find(' \n') >= 0 :
    TEXT = TEXT.replace(' \n', '\n')
  while TEXT.find('\n\n\n') >= 0 :
    TEXT = TEXT.replace('\n\n\n', '\n\n')
  if TEXT != TEXT0 :
    FILE = open(target, 'w')
    FILE.write(TEXT)
    FILE.close()

def doml(target): #WIP
    assert(False) #Not Implemented Yet!!
    print('beginning...')
    find = subprocess.Popen(['/bin/find', '-wholename', target+'/*\\.ml?'], stdout=subprocess.PIPE)
    (out, _) = find.communicate()
    files = [ afile for afile in out.split('\n') if (afile != "") and ('_build' not in afile) and ('/bin/' not in afile)]
    print(files)
    print('grep tabulation...')
    grep = subprocess.Popen(['/bin/grep', '-l', '\\t']+files, stdout=subprocess.PIPE)
    (out, _) = grep.communicate()
    files_tab = set(afile for afile in out.split('\n') if afile != '')
    print('grep tailling spaces...')
    grep = subprocess.Popen(['/bin/grep', '-l', ' $']+files, stdout=subprocess.PIPE)
    (out, _) = grep.communicate()
    files_trail = set(afile for afile in out.split('\n') if afile != '')
    files = files_tab.union(files_trail)
    print(files)

if __name__ == "__main__" :
    if   sys.argv[1] == '--doml' :
        print('sys.argv[0] = '+sys.argv[0])
        doml(sys.argv[2])
    elif sys.argv[1] == '--single' :
        do(sys.argv[2])
    else:
        do(sys.argv[1])
