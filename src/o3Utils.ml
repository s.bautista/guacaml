(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * O3Utils : toolbox for generic reversible conversion
 *)

open O3

let from_a_bc_de_to_abd_c_e : (('a*('b*'c)*('d*'e)), (('a*'b*'d)*'c*'e)) o3 =
(
  (fun (a, (b, c), (d, e)) -> ((a, b, d), c, e)),
  (fun ((a, b, d), c, e) -> (a, (b, c), (d, e)))
)
