# LGPL-3.0 Linking Exception
#
# Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
#
# GuaCaml : Generic Unspecific Algorithmic in OCaml
#
# format_assert.sh : tries to automatically replace 'assert'
#   by appropriate 'failwith'

targets1="$(grep -Rl 'assert' ./ | grep -v git | grep -v "_build/" | grep "\.ml$" )"
for file in $targets1
do
  echo "reformat(assert): " $file
  #python format_assert.py $file $(echo $file | sed "s/\.ml$/-alt.ml/")
  python format_assert.py $file $file
done
