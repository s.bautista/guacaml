(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * IntHeap : facility for serialization and deserialization of int list
 *)

open BTools
module BW =
struct
  open ToBStream

  let delta
    ?(diff=(fun _ x -> x)) bw
     (cha:Channel.t) (l:'a list) : unit =
    list bw cha (MyList.delta diff l)

  (* [l] must be sorted with non-decreasing order *)
  let delta_int ?(bw_int=int)
     (cha:Channel.t) (l:int list) : unit =
    list bw_int cha (MyList.delta_int l)

  (* [l] must be sorted with increasing order *)
  let delta'_int ?(bw_int=int)
     (cha:Channel.t) (l:int list) : unit =
    list bw_int cha (MyList.delta'_int l)

  let intlist_core (l:int list) : int t =
    let rl = ref l in
    let remove x =
    let check y = if x = y
      then Some y else None
    in
    let op_pos, l' = MyList.iremove check !rl in
    rl := l';
    match op_pos with
    | None -> (failwith "[GuaCaml.IntHeap] error")
    | Some(pos, _) -> pos
    in
    let encode cha x =
    int cha (remove x)
    in
    encode

  let intlist
    ?(delta=(fun _ x -> x))
     (l:int list) (cha:Channel.t) : int t =
    list int cha l;
    intlist_core l

  let intheap (l:int list) (cha:Channel.t) : int t =
    let l = List.sort Stdlib.compare l in
    delta_int cha l;
    intlist_core l

  (* same as intheap but the int list given as input has no replicate *)
  let intheap' (l:int list) (cha:Channel.t) : int t =
    let l = List.sort Stdlib.compare l in
    delta'_int cha l;
    intlist_core l
end

module BR =
struct
  open OfBStream

  let delta br   ?(plus=(fun _ x -> x))
     (cha:Channel.t) : 'a list =
    MyList.undelta plus (list br cha)

  let delta_int  ?(br_int=int) (cha:Channel.t) : int list =
    MyList.undelta_int (list br_int cha)

  let delta'_int ?(br_int=int) (cha:Channel.t) : int list =
    MyList.undelta'_int (list br_int cha)

  let intlist_core (l:int list) : int t =
    let rl = ref l in
    let remove n =
    let op_x, l' = MyList.nth_pop !rl n in
    rl := l';
    match op_x with
    | None -> (failwith "[GuaCaml.IntHeap] error")
    | Some x -> x
    in
    let decode cha =
    let pos = int cha in
    if true then (Channel.prn_meta cha (">>> "^(string_of_int pos)));
    remove pos
    in
    decode

  let intlist  cha : int t =
    intlist_core (list int cha)

  let intheap  cha : int t =
    intlist_core (delta_int cha)

  let intheap' cha : int t =
    intlist_core (delta'_int cha)
end
