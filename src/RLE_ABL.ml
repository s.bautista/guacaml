(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * RLE_ABL : Run Length Encoded AB-List
 *)

open AB
open RLE

type ('a, 'b) abt = ('a, 'b) ab t

module ToS =
struct
  include ToS
  let abt a b = rle (AB.ToS.ab a b)
end

(* [isAB false (A _) = true ]
 * [isAB true  (A _) = false]
 * [isAB false (B _) = false]
 * [isAB true  (B _) = true ]
 * [isAB b ab = b <> (isA ab)]
 * [abt_countA = abt_count ~a:false]
 * [abt_countB = abt_count ~a:true ]
 *)
(* [IMPROVE] *)
let abt_count ?(a=true) (t:('a, 'b)abt) : int =
  let rec abt_count_rec a carry = function
    | [] -> carry
    | (n, ab)::tail -> (
      let carry' = if AB.isAB a ab then n + carry else carry in
      abt_count_rec a carry' tail
    )
  in abt_count_rec a 0 t

let abt_countA t : int = abt_count ~a:false t
let abt_countB t : int = abt_count ~a:true  t

let split_abt (abt:('a, 'b) abt) : (bool * int list * 'a t * 'b t) =
  let rec split_abt_rec is_a (n0:int) (cN:int list) (al:'a rle) (bl:'b rle) =
    function
    | [] -> (List.rev (n0::cN), rev al, rev bl)
    | (n, ab)::abt -> (
      match ab with
      | A a -> (
        if is_a
        then split_abt_rec true  (n+n0) cN  (push ~n a al) bl abt
        else split_abt_rec true  n (n0::cN) (push ~n a al) bl abt
      )
      | B b -> (
        if is_a
        then split_abt_rec false n (n0::cN) al (push ~n b bl) abt
        else split_abt_rec false (n+n0) cN  al (push ~n b bl) abt
      )
    )
  in
  match abt with
  | [] -> assert false (* invalid argument *)
  | (n, ab)::abt -> (
    let is_a, (il, al, bl) = match ab with
      | A a -> (true , split_abt_rec true  n [] [(n, a)] [] abt)
      | B b -> (false, split_abt_rec false n [] [] [(n, b)] abt)
    in
    (is_a, il, al, bl)
  )

let split_abt_unitA (abt:(unit, 'b) abt) : (bool * int list * int * 'b t) =
  let rec aux is_a (n0:int) (cN:int list) nA (bl:'b rle) = function
    | [] -> (List.rev (n0::cN), nA, rev bl)
    | (n, ab)::abt -> (
      match ab with
      | A () -> (
        if is_a
        then aux true  (n+n0)  cN  (n+nA) bl abt
        else aux true   n (n0::cN) (n+nA) bl abt
      )
      | B b -> (
        if is_a
        then aux false n (n0::cN) nA (push ~n b bl) abt
        else aux false (n+n0) cN  nA (push ~n b bl) abt
      )
    )
  in
  match abt with
  | [] -> assert false (* invalid argument *)
  | (n, ab)::abt -> (
    let is_a, (il, nA, bl) = match ab with
      | A () -> (true , aux true  n [] n []  abt)
      | B b  -> (false, aux false n [] 0 [(n, b)] abt)
    in
    (is_a, il, nA, bl)
  )

let split_abt_unitAB (abt:(unit, unit) abt) : (bool * int list * int * int) =
  let rec aux is_a (n0:int) (cN:int list) nA nB = function
    | [] -> (List.rev (n0::cN), nA, nB)
    | (n, ab)::abt -> (
      match ab with
      | A () -> (
        if is_a
        then aux true  (n+n0)  cN  (n+nA) nB abt
        else aux true   n (n0::cN) (n+nA) nB abt
      )
      | B () -> (
        if is_a
        then aux false  n (n0::cN) nA (n+nB) abt
        else aux false (n+n0)  cN  nA (n+nB) abt
      )
    )
  in
  match abt with
  | [] -> assert false (* invalid argument *)
  | (n, ab)::abt -> (
    let is_a, (il, nA, bl) = match ab with
      | A () -> (true , aux true  n [] n 0 abt)
      | B () -> (false, aux false n [] 0 n abt)
    in
    (is_a, il, nA, bl)
  )

let combine_abt ((is_a, il, al, bl):(bool * int list * 'a rle * 'b rle)) : ('a, 'b) abt =
  let move_ab is_a n ((c, al, bl) : ('a, 'b) abt * 'a rle * 'b rle) : ('a, 'b) abt * 'a rle * 'b rle =
    if is_a
    then (
      let al', c' = move_map (fun _ x -> A x) al n c in
      (c', al', bl )
    )
    else (
      let bl', c' = move_map (fun _ x -> B x) bl n c in
      (c', al , bl')
    )
  in
  let rec loop is_a il c_al_bl =
    match il with
    | [] -> (
      let c, al, bl = c_al_bl in
      assert(al = []);
      assert(bl = []);
      rev c
    )
    | n::il -> (
      loop (not is_a) il (move_ab is_a n c_al_bl)
    )
  in
  loop is_a il ([], al, bl)

let combine_abt_unitA ((is_a, il, nA, bl):(bool * int list * int * 'b rle)) : (unit, 'b) abt =
  let move_ab is_a n (c, nA, bl) =
    if is_a
    then (
      assert(n <= nA);
      let nA' = nA - n in
      let c' = push ~n (A()) c in
      (c', nA', bl )
    )
    else (
      let bl', c' = move_map (fun _ x -> B x) bl n c in
      (c', nA , bl')
    )
  in
  let rec loop is_a il c_nA_bl =
    match il with
    | [] -> (
      let c, nA, bl = c_nA_bl in
      assert(nA = 0 );
      assert(bl = []);
      rev c
    )
    | n::il -> (
      loop (not is_a) il (move_ab is_a n c_nA_bl)
    )
  in
  loop is_a il ([], nA, bl)

let combine_abt_unitAB ((is_a, il, nA, nB):(bool * int list * int * int)) : (unit, unit) abt =
  let move_ab is_a n (c, nA, nB) =
    if is_a
    then (
      assert(n <= nA);
      let nA' = nA - n in
      let c' = push ~n (A()) c in
      (c', nA', nB )
    )
    else (
      assert(n <= nB);
      let nB' = nB - n in
      let c' = push ~n (B()) c in
      (c', nA , nB')
    )
  in
  let rec loop is_a il c_nA_nB =
    match il with
    | [] -> (
      let c, nA, nB = c_nA_nB in
      assert(nA = 0);
      assert(nB = 0);
      rev c
    )
    | n::il -> (
      loop (not is_a) il (move_ab is_a n c_nA_nB)
    )
  in
  loop is_a il ([], nA, nB)

module ToB =
struct
  open BTools.ToB
  include ToB

  let sized_abt (sal:'a rle t) (sbl:'b rle t) (abt:('a, 'b) abt) (s:stream) : stream =
    let len = length abt in
    if len = 0
    then s
    else (
      let is_a, il, al, bl = split_abt abt in
      s |> sbl bl |> sal al |> sum_list ~min:1 len il |> bool is_a
    )

  let sized_abt_unitA (sbl:'b rle t) (abt:(unit, 'b) abt) (s:stream) : stream =
    let len = length abt in
    if len = 0
    then s
    else (
      let is_a, il, _, bl = split_abt_unitA abt in
      s |> sbl bl |> sum_list ~min:1 len il |> bool is_a
    )

  let sized_abt_unitAB (abt:(unit, unit) abt) (s:stream) : stream =
    let len = length abt in
    if len = 0
    then s
    else (
      let is_a, il, _, _ = split_abt_unitAB abt in
      OUnit.print_error_endline (fun()->"[MyRLE.ToB.sized_abt_unitAB] il:"^STools.ToS.(list int il));
      OUnit.print_error_endline (fun()->"[MyRLE.ToB.sized_abt_unitAB] len:"^STools.ToS.(int len));
      OUnit.print_error_endline (fun()->"[MyRLE.ToB.sized_abt_unitAB] [0] stream:"^(STools.SUtils.string_of_bool_list s));
      let s = sum_list ~min:1 len il s in
      OUnit.print_error_endline (fun()->"[MyRLE.ToB.sized_abt_unitAB] [1] stream:"^(STools.SUtils.string_of_bool_list s));
      let s = bool is_a s in
      OUnit.print_error_endline (fun()->"[MyRLE.ToB.sized_abt_unitAB] [2] stream:"^(STools.SUtils.string_of_bool_list s));
      s
    )

  let abt sal sbl abt s : stream =
    let n = length abt in
    let s = sized_abt sal sbl abt s in
    let s = int n s in
    s
end

module OfB =
struct
  open BTools.OfB
  include OfB

  let sized_abt_countAB (len:int) (sal:int -> 'a rle t) (sbl:int -> 'b rle t) (s:stream) : (('a, 'b) abt * (int * int)) * stream =
    if len = 0
    then (([], (0, 0)), s)
    else (
      let is_a, s = bool s in
      let il, s = sum_list ~min:1 len s in
      let nA, nB = alt_sum is_a il in
      assert(nA + nB = len);
      let al, s = sal nA s in
      let bl, s = sbl nB s in
      let abt = combine_abt (is_a,il, al, bl) in
      ((abt, (nA, nB)), s)
    )

  let sized_abt len sal sbl s : (_, _) abt * stream =
    let (abt, _), s = sized_abt_countAB len sal sbl s in
    (abt, s)

  let sized_abt_countA len sal sbl s : ((_, _) abt * int) * stream =
    let (abt, nAB), s = sized_abt_countAB len sal sbl s in
    ((abt, fst nAB), s)
  let sized_abt_countB len sal sbl s : ((_, _) abt * int) * stream =
    let (abt, nAB), s = sized_abt_countAB len sal sbl s in
    ((abt, snd nAB), s)

  let sized_abt_unitA_countAB (len:int) (sbl:int -> 'b rle t) (s:stream) : ((unit, 'b) abt * (int * int)) * stream =
    if len = 0
    then (([], (0, 0)), s)
    else (
      let is_a, s = bool s in
      let il, s = sum_list ~min:1 len s in
      let nA, nB = alt_sum is_a il in
      assert(nA + nB = len);
      let bl, s = sbl nB s in
      let abt = combine_abt_unitA (is_a,il, nA, bl) in
      ((abt, (nA, nB)), s)
    )

  let sized_abt_unitA len sbl s : (_, _) abt * stream =
    let (abt, _), s = sized_abt_unitA_countAB len sbl s in
    (abt, s)

  let sized_abt_unitA_countA len sbl s : ((_, _) abt * int ) * stream =
    let (abt, nAB), s = sized_abt_unitA_countAB len sbl s in
    ((abt, fst nAB), s)
  let sized_abt_unitA_countB len sbl s : ((_, _) abt * int ) * stream =
    let (abt, nAB), s = sized_abt_unitA_countAB len sbl s in
    ((abt, snd nAB), s)

  let sized_abt_unitAB_countAB (len:int) (s:stream) : ((unit, unit) abt * (int * int)) * stream =
    if len = 0
    then (([], (0, 0)), s)
    else (
      OUnit.print_error_endline (fun()->"[MyRLE.OfB.sized_abt_unitAB] [2] stream:"^(STools.SUtils.string_of_bool_list s));
      let is_a, s = bool s in
      OUnit.print_error_endline (fun()->"[MyRLE.OfB.sized_abt_unitAB] len:"^STools.ToS.(int len));
      OUnit.print_error_endline (fun()->"[MyRLE.OfB.sized_abt_unitAB] [1] stream:"^(STools.SUtils.string_of_bool_list s));
      let il, s = sum_list ~min:1 len s in
      OUnit.print_error_endline (fun()->"[MyRLE.OfB.sized_abt_unitAB] il:"^STools.ToS.(list int il));
      OUnit.print_error_endline (fun()->"[MyRLE.OfB.sized_abt_unitAB] [0] stream:"^(STools.SUtils.string_of_bool_list s));
      let nA, nB = alt_sum is_a il in
      assert(nA + nB = len);
      let abt = combine_abt_unitAB (is_a,il, nA, nB) in
      ((abt, (nA, nB)), s)
    )

  let sized_abt_unitAB len s : (_, _) abt * stream =
    let (abt, _), s = sized_abt_unitAB_countAB len s in
    (abt, s)

  let sized_abt_unitAB_countA len s : ((_, _) abt * int ) * stream =
    let (abt, nAB), s = sized_abt_unitAB_countAB len s in
    ((abt, fst nAB), s)
  let sized_abt_unitAB_countB len s : ((_, _) abt * int ) * stream =
    let (abt, nAB), s = sized_abt_unitAB_countAB len s in
    ((abt, snd nAB), s)

  let abt (sal:int -> 'a rle t) (sbl:int -> 'b rle t) (s:stream) : ('a, 'b) abt * stream =
    let n, s = int s in
    sized_abt n sal sbl s
end

module ToBStream =
struct
  open BTools.ToBStream
  include ToBStream

  let abt (a:'a t) (b:'b t) cha (abt:('a, 'b)abt) : unit =
    rle (AB.ToBStream.ab a b) cha abt

  let sized_abt (sal:'a rle t) (sbl:'b rle t) cha (abt:('a, 'b) abt) : unit =
    let len = length abt in
    if len = 0 then ()
    else (
      let is_a, il, al, bl = split_abt abt in
      assert(List.length il <= List.length abt);
      assert(length al + length bl = length abt);
      bool cha is_a;
      sum_list ~min:1 len cha il;
      sal cha al;
      sbl cha bl;
    )

  let abt' sal sbl cha abt : unit =
    int cha (length abt);
    sized_abt sal sbl cha abt;
    ()
end

module OfBStream =
struct
  open BTools.OfBStream
  include OfBStream

  let abt (a:'a t) (b:'b t) cha : ('a, 'b)abt =
    rle (AB.OfBStream.ab a b) cha

  let sized_abt (len:int) (sal:int -> 'a rle t) (sbl:int -> 'b rle t) cha : ('a, 'b) abt  =
    if len = 0 then []
    else (
      let is_a = bool cha in
      let il = sum_list ~min:1 len cha in
      let nA, nB = alt_sum is_a il in
      assert(nA + nB = len);
      let al = sal nA cha in
      assert(length al = nA);
      let bl = sbl nB cha in
      assert(length bl = nB);
      let abt = combine_abt (is_a,il, al, bl) in
      assert(length abt = len);
      abt
    )

  let abt' (sal:int -> 'a rle t) (sbl:int -> 'b rle t) cha : ('a, 'b) abt  =
    let n = int cha in
    sized_abt n sal sbl cha
end
