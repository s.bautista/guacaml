(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * H2Array : efficient implementation of unique tables for hash-consing
 *)

open Extra
open STools
open BTools

(* let default_size = 10_000 *)
let default_size = 10

(* if one needs to retrieve predecessor just re-hash [cell.data_data] *)

type cell =
| Empty
| Cell of {
mutable index     : int;  (* reflective index of the cell in [revers] *)
mutable state     : int;  (* state is not stored *)
        data_len  : int;
        data_data : bytes;
mutable  next      : cell; (* indicates the next cell with the same hash *)
}

type t = {
  mutable access : cell array; (* hash-table [cell.data_data -> cell] *)
  mutable revers : cell array; (* store-table [cell array] *)
  mutable index : int;
  mutable state : int; (* state.(i) = true <-> i-th state's bit is used *)
  mutable length : int; (* number of stored cell *)
  mutable hashed : bool; (* [hashed = true] iff the hash-table is currently consistent *)
  mutable sorted : bool; (* [sorted = true] iff nodes are sorted according to [sort] *)
}

(* [TODO] let check (t:t) : bool = *)

let is_compact t = t.length = t.index

let get_index t = t.index

let get_state t i =
  match t.revers.(i) with
  | Empty -> None
  | Cell{state} -> Some state

let set_state t i s =
  match t.revers.(i) with
  | Empty -> (failwith "[GuaCaml.H2Array] error")
  | Cell cell' -> cell'.state <- s

let fill_state t s : unit =
  for i = 0 to t.index -1
  do
    match t.revers.(i) with
    | Empty -> ()
    | Cell cell' -> cell'.state <- s
  done

let to_stree_retro (t:t) : Tree.stree =
  let stack = ref [] in
  let get_ident = function
    | Empty -> None
    | Cell{index} -> Some index
  in
  Array.iter (function
    | Empty -> ()
    | Cell {index; state; data_len; data_data; next} -> (
      let ba = BArray.of_bytes ~copy:false (data_len, data_data) in
      stack_push stack (Tree.Node[ToSTree.int index; ToSTree.int state; BArray.to_stree ba; ToSTree.(option int) (get_ident next)])
    )
  ) t.revers;
  let access =
    Array.map get_ident t.access
    |> Array.to_list
    ||> ToSTree.(option int)
  in
  Tree.Node [ToSTree.int t.index; Tree.Node (List.rev !stack); Tree.Node access]

let create ?(h_size = default_size) ?(a_size = default_size) ?(index = 0) ?(state = 0) () : t =
  assert(h_size > 0);
  assert(a_size > 0);
  assert(index >= 0);
  let h_size = Tools.power_2_above 1 h_size in
  let a_size = Tools.power_2_above 1 a_size in
  let access = Array.make h_size Empty in
  let revers = Array.make a_size Empty in
  {access; revers; index; state; length = 0; hashed = true; sorted = true}

let clear (t:t) : unit =
  Array.fill t.access 0 (Array.length t.access) Empty;
  Array.fill t.revers 0 (Array.length t.revers) Empty;
  t.index  <- 0;
  t.length <- 0;
  t.state  <- 0;
  t.hashed <- true;
  ()

let copy (t:t) : t =
  let revers' = Array.make (Array.length t.revers) Empty in
  let rec copy_cell (cell:cell) : cell = match cell with
    | Empty -> Empty
    | Cell {index; state; data_len; data_data; next} -> (
      match revers'.(index) with
      | Empty -> (
        let next' = copy_cell next in
        let data_data' = Bytes.copy data_data in
        let cell = Cell {index; state; data_len; data_data = data_data'; next = next'} in
        revers'.(index) <- cell;
        cell
      )
      | _ -> cell
    )
  in
  let access' = Array.map copy_cell t.access in
  {access = access'; revers = revers'; index = t.index; state = t.state; length = t.length; hashed = t.hashed; sorted = t.sorted}

let length t = t.length

let loc_of_data (t:t) (data:bytes) : int =
  let h = Hashtbl.hash data in
  let hk = h land (Array.length t.access -1) in
  assert(0 <= hk && hk < Array.length t.access); (* [DEBUG] *)
  hk

let rec find_cell (len:int) (data:bytes) (cell:cell) : int option =
  match cell with
  | Empty -> None
  | Cell {index; state; data_len; data_data; next} -> (
    if len = data_len && data = data_data
    then (Some index)
    else find_cell len data next
  )

let find (t:t) ?(copy=true) (b:BArray.t) : int option =
  let len, data = BArray.to_bytes ~copy b in
  let hk = loc_of_data t data in
  find_cell len data t.access.(hk)

(* recomputes hash-lists, used when uncompressing and resizing
   initializes [t.access] with [h_size]
   ignores current value of [cell.next]
 *)
let hash_reset (t:t) : unit =
  Array.fill t.access 0 (Array.length t.access) Empty;
  for i = 0 to t.index -1
  do
    let cell = t.revers.(i) in
    match cell with
    | Empty -> ()
    | Cell ({index; state; data_len; data_data; next} as cell') -> (
      let hk = loc_of_data t data_data in
      cell'.next <- t.access.(hk);
      t.access.(hk) <- t.revers.(i);
    )
  done;
  t.hashed <- true

let push (t:t) ?(copy=true) (b:BArray.t) : int =
  assert(t.hashed);
  t.sorted <- false;
  let len, data = BArray.to_bytes ~copy b in
  let hk = loc_of_data t data in
  let hk_cell = t.access.(hk) in
  match find_cell len data hk_cell with
  | Some i -> i
  | None -> (
    if t.index >= Array.length t.revers
    then (
      t.access <- Array.make (2*(Array.length t.access)) Empty;
      let revers' = Array.make (2*(Array.length t.revers)) Empty in
      Array.blit t.revers 0 revers' 0 (Array.length t.revers);
      t.revers <- revers';
      hash_reset t;
    );
    let index = t.index in
    t.index  <- succ t.index ;
    t.length <- succ t.length;
    let hk = loc_of_data t data in (* changed during hash_reset *)
    let hk_cell = t.access.(hk) in (* might have change during hash_reset *)
    let cell = Cell {index; state = 0; data_len = len; data_data = data; next = hk_cell} in
    t.revers.(index) <- cell;
    t.access.(hk) <- cell;
    index
  )

let push' (t:t) ?(copy=true) (b:BArray.t) : int =
  assert(t.hashed);
  t.sorted <- false;
  let len, data = BArray.to_bytes ~copy b in
  let hk = loc_of_data t data in
  let hk_cell = t.access.(hk) in
  match find_cell len data hk_cell with
  | Some i -> i
  | None -> (failwith "[GuaCaml.H2Array] error")

let memA (t:t) (b:BArray.t) : bool =
  match find t b with
  | Some _ -> true
  | None   -> false

let memI (t:t) (i:int) : bool =
  match t.revers.(i) with
  | Empty  -> false
  | Cell _ -> true

let pull (t:t) ?(copy=true) (i:int) : BArray.t =
  (* assert(t.hashed); (* not required *) (* do not use *) *)
  match t.revers.(i) with
  | Empty -> (failwith "[GuaCaml.H2Array] error")
  | Cell {index; state; data_len; data_data; next} -> (
    BArray.of_bytes ~copy (data_len, data_data)
  )

let iter (t:t) ?(copy=true) (f:(BArray.t -> int -> unit)) : unit =
  Array.iter (function
    | Empty -> ()
    | Cell {index; state; data_len; data_data; next} ->
      (f (BArray.of_bytes ~copy (data_len, data_data)) index)
  ) t.revers

let map (t:t) ?(copy=true) (f:(BArray.t -> int -> 'b)) : 'b list =
  let stack = ref [] in
  Array.iter (function
    | Empty -> ()
    | Cell {index; state; data_len; data_data; next} ->
      stack_push stack (f(BArray.of_bytes ~copy (data_len, data_data)) index)
  ) t.revers;
  List.rev(!stack)

let mapreduce (t:t) init ?(copy=true) (map:(BArray.t -> int -> 'b)) reduce =
  let stack = ref init in
  Array.iter (function
    | Empty -> ()
    | Cell {index; state; data_len; data_data; next} ->
      stack := reduce (map(BArray.of_bytes ~copy (data_len, data_data)) index) !stack
  ) t.revers;
  !stack

let filter_map_inplace (t:t) ?(copy_fst=true) ?(copy_snd=true) fmap : unit =
  t.hashed <- false;
  for i = 0 to (Array.length t.revers -1)
  do
    match t.revers.(i) with
    | Empty -> ()
    | Cell {index; state; data_len; data_data; next} -> (
      let ba = BArray.of_bytes ~copy:copy_fst (data_len, data_data) in
      match fmap index state ba with
      | None -> t.revers.(i) <- Empty
      | Some ba' -> (
        let data_len, data_data = BArray.to_bytes ~copy:copy_snd ba' in
        t.revers.(i) <- Cell{index; state; data_len; data_data; next = Empty}
      )
    )
  done;
  hash_reset t

let to_stree (t:t) : Tree.stree =
  let core =
    Array.map (function
      | Empty -> Tree.Node[]
      | Cell {index; state; data_len; data_data; next} ->
        Tree.Node[BArray.to_stree (BArray.of_bytes ~copy:false (data_len, data_data))]
    ) t.revers
    |> Array.to_list
  in
  Tree.Node ((ToSTree.int t.index)::(ToSTree.int(Array.length t.access))::core)

let of_stree (s:Tree.stree) : t =
  match s with
  | Tree.Node (s_index::s_h_size::core) -> (
    let index = OfSTree.int s_index in
    let h_size = OfSTree.int s_h_size in
    let length = ref 0 in
    let revers = Array.mapi (fun index s ->
      match s with
      | Tree.Node[] -> Empty
      | Tree.Node[s] -> (
        let data_len, data_data = BArray.to_bytes ~copy:false (BArray.of_stree s) in
        incr length;
        Cell {index; state = 0; data_len; data_data; next = Empty}
      )
      | _ -> (failwith "[GuaCaml.H2Array] error")
    ) (Array.of_list core) in
    let t = {access = Array.make h_size Empty; revers; index; state = 0; length = !length; hashed = false; sorted = false} in
    hash_reset t;
    t
  )
  | _ -> (failwith "[GuaCaml.H2Array] error")

open BTools

module ToBStream =
struct
  open ToBStream

  (* [TODO] implement [~destruct=true] *)
  let cell ?(destruct=false) (cha:Channel.t) : cell -> unit =
    function
    | Empty -> bool cha false
    | Cell {index; state; data_len; data_data; next} -> (
      bool cha true;
      barray cha (BArray.of_bytes ~copy:false (data_len, data_data))
    )

  (* [TODO] implement [~destruct=true] *)
  let h2array ?(destruct=false) (cha:Channel.t) t : unit =
    int cha t.index;
    int cha (Array.length t.access);
    array (cell ~destruct) cha t.revers
end

module OfBStream =
struct
  open OfBStream

  let cell (cha:Channel.t) : cell =
    if bool cha
    then (
      let data_len, data_data = (BArray.to_bytes ~copy:false (barray cha)) in
      Cell {index = 0; state = 0; data_len; data_data; next = Empty}
    )
    else Empty

  let h2array (cha:Channel.t) : _ =
    let index = int cha in
    let h_size = int cha in
    let length = ref 0 in
    let revers = Array.mapi (fun index c ->
      match c with
      | Empty -> Empty
      | Cell{state; data_len; data_data; next} -> (
        incr length;
        Cell{index; state; data_len; data_data; next}
      )
    ) (array cell cha) in
    let t = {access = Array.make h_size Empty; revers; index; state = 0; length = !length; hashed = false; sorted = false} in
    hash_reset t;
    t
end

let bw = ToBStream.h2array
let br = OfBStream.h2array

let keep_clean (t:t) ?(hr=true) (alive:int list) : unit =
  t.hashed <- false;
  let aalive = Array.make (Array.length t.revers) false in
  List.iter (fun i -> aalive.(i) <- true) alive;
  let length = ref 0 in
  Array.iteri (fun i a -> if a
    then (incr length)
    else t.revers.(i) <- Empty) aalive;
  t.length <- !length;
  if hr then hash_reset t

let keep_clean_barray (t:t) ?(hr=true) (alive:BArray.t) : unit =
  assert(BArray.length alive = t.index);
  t.hashed <- false;
  let length = ref 0 in
  for k = 0 to t.index -1
  do
    if BArray.get alive k
    then (
      match t.revers.(k) with
      | Empty -> ()
      | Cell _ -> incr length;
    )
    else (t.revers.(k) <- Empty)
  done;
  t.length <- !length;
  if hr then hash_reset t

let keep_clean_smart ?(hr=true) (t:t) : unit =
  t.hashed <- false;
  for i = 0 to t.index -1
  do
    match t.revers.(i) with
      | Cell {state} when state < 0 -> (
        t.revers.(i) <- Empty;
        t.length <- pred t.length
      )
      | _ -> ()
  done;
  if hr then hash_reset t

let compact_smart ?(hr=true) (t:t) ?(copy_fst=true) ?(copy_snd=true) (map:int -> int -> BArray.t -> BArray.t) : t =
  let rec loop0 carry i =
    if i < 0 then carry
    else (
      match t.revers.(i) with
      | Empty ->
        loop0 carry  (pred i)
      | Cell {state} -> (
        let carry' =
          if state >= 0 then succ carry else carry
        in
        loop0 carry' (pred i)
      )
    )
  in
  let t_index = loop0 0 (t.index -1) in
  let n = Tools.power_2_above 2 t_index in
  let t_revers = Array.make n Empty in
  let rec loop1 index i =
    if i >= t.index then (assert(index = t_index);())
    else (
      match t.revers.(i) with
      | Empty -> loop1 index (succ i)
      | Cell ({state; data_len; data_data} as cell) -> (
        if state >= 0
        then (
          let data_len, data_data =
             (data_len, data_data)
            |> BArray.of_bytes ~copy:copy_fst
            |> (map i state)
            |> BArray.to_bytes ~copy:copy_snd
          in
          t_revers.(index) <-
            (Cell{index; state = i; data_len; data_data; next = Empty});
          cell.state <- index;
          loop1 (succ index) (succ i)
        )
        else (loop1 index (succ i))
      )
    )
  in
  loop1 0 0;
  let t = {
    access = Array.make n Empty;
    revers = t_revers;
    index  = t_index;
    state  = 0;
    length = t_index;
    hashed = false;
    sorted = t.sorted;
  } in
  if hr then hash_reset t;
  t

(* [compact_fmap t map = ia] where
    - [t'] is the state of [t] after applying [compact_map]
    - [ia.(i)] is the new index of the old index [i]
      => ia.(i) = -1 means that the node has been discarded (or was Empty)
    - [Array.length ia = t.index]
    - [t'.index = t.length]
    - [t'.length = t.length]
 *)
let compact_fmap (t:t) ?(hr=true) ?(copy_fst=true) ?(copy_snd=true) (fmap:int -> int -> BArray.t -> BArray.t option) : int array =
  assert(t.length <= t.index);
  t.hashed <- false;
  let index = ref 0 in
  let rename = Array.make t.index (-1) in
  for i = 0 to t.index -1
  do
    match t.revers.(i) with
    | Empty -> ()
    | Cell{state; data_len; data_data; next} -> (
      let ba = BArray.of_bytes ~copy:copy_fst (data_len, data_data) in
      match fmap i state ba with
      | None -> ()
      | Some ba' -> (
        let index = !++ index in
        let data_len, data_data = BArray.to_bytes ~copy:copy_snd ba' in
        t.revers.(index) <-
          Cell{index; state; data_len; data_data; next = Empty};
        assert(rename.(i) < 0);
        rename.(i) <- index;
      )
    )
  done;
  if not (!index <= t.length)
  then (
    print_endline ("[compact_fmap]  !index :"^ToS.(int  !index ));
    print_endline ("[compact_fmap] t.index :"^ToS.(int t.index ));
    print_endline ("[compact_fmap] t.length:"^ToS.(int t.length));
    (failwith "[GuaCaml.H2Array] error")
  );
  t.length <- !index;
  t.index  <- !index;
  if t.length <= (Array.length t.revers) / 2
  then (
    let n = Tools.power_2_above 1 t.length in
    assert(n >= t.length);
    let revers' = Array.make n Empty in
    Array.blit t.revers 0 revers' 0 t.index;
    t.revers <- revers';
    t.access <- Array.make n Empty;
  )
  else (
    Array.fill t.revers t.index (Array.length t.revers - t.index) Empty;
  );
  if hr then hash_reset t;
  rename

let sort (t:t) ?(hr=true) () : int array =
  let max_len = mapreduce t 0 ~copy:false (fun ba _ -> BArray.length ba) max in
  let alloc d = if d < 0 then (succ max_len) else 256 in
  let depth = -1 in
  let start_with_none = false in
  let get d cell =
    match cell with
    | Empty -> (assert(d <0); None)
    | Cell{data_len; data_data} -> (
      if d < 0
      then Some(data_len)
      else if d >= Bytes.length data_data
        then None
        else Some(Char.code(Bytes.unsafe_get data_data d))
    )
  in
  MyArray.lex_radix_sort t.revers ~depth ~start_with_none alloc get 0 t.index;
  let rename = Array.make t.index (-1) in
  for i = 0 to t.index -1
  do
    match t.revers.(i) with
    | Empty -> ()
    | Cell ({index} as cell') -> (
      rename.(index) <- i;
      cell'.index <- i;
    )
  done;
  t.index <- t.length;
  if t.index <= (Array.length t.revers) / 2
  then (
    let n = Tools.power_2_above 1 t.index in
    assert(n >= t.length);
    t.revers <- Array.sub t.revers 0 n;
    t.access <- Array.make n Empty;
  );
  if hr then hash_reset t;
  rename

module ToF =
struct
  open Io.ToF
  open BArray.ToF

  let cell (cha:stream) = function
    | Empty -> output_byte cha 0
    | Cell {index; state; data_len; data_data; next} -> (
      output_byte cha 1;
      barray cha (BArray.of_bytes ~copy:false (data_len, data_data))
    )

  let h2array (cha:stream) t : unit =
    int cha t.index;
    int cha (Array.length t.access);
    array cell cha t.revers
end

module OfF =
struct
  open Io.OfF
  open BArray.OfF

  let cell (cha:stream) : cell =
    match input_byte cha with
    | 0 -> Empty
    | 1 -> (
      let data_len, data_data = BArray.to_bytes ~copy:false (barray cha) in
      Cell{index = 0; state = 0; data_len; data_data; next = Empty}
    )
    | _ -> (failwith "[GuaCaml.H2Array] error")

  let h2array (cha:stream) : _ =
    let index = int cha in
    let h_size = int cha in
    let length = ref 0 in
    let revers = Array.mapi (fun index cell ->
      match cell with
      | Empty -> Empty
      | Cell{state; data_len; data_data; next} -> (
        incr length;
        Cell{index; state; data_len; data_data; next}
      )
    ) (array cell cha) in
    let t = {access = Array.make h_size Empty; revers; index; state = 0; length = !length; hashed = false; sorted = false} in
    hash_reset t;
    t
end
