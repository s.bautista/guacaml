(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * ParseUtils : Simpl lexers and parsers
 *)

(* Single Symbol Based Lexing (SSBL) *)
(* The core idea of SSBL, is that each character is mapped to an integer.
   The file is splitted into contiguous chunks of symbols which are mapped to the same integer.
   Then depending on the signe of this integers this chunk is dealt differently :
   - positive integers ( > 0 ) for word
    => the chunk is concatenated back into a string
   - zero              ( = 0 ) for symbols
    => the chunk contains a single character which is stored as is
   - negative integers ( < 0 ) for spaces
    => the chunk content is discarded, only the number of elements is stored
 *)

open STools

module SSBL =
struct
  type file_loc = {
    floc_line : int;
    floc_char : int;
  }

  type file_range = {
    frg_fst : file_loc;
    frg_lst : file_loc;
    frg_len : int;
  }

  type ttoken =
    | Word   of string
    | Symbol of char
    | Unary  of int

  type token = {
    range  : file_range;
    tkind  : int;
    ttoken : ttoken;
  }

  type stream = token Stream.t

  let reverse_token (rU:int -> char) token : string =
    match token.ttoken with
    | Word s -> s
    | Symbol c -> String.make token.range.frg_len c
    | Unary i -> String.make token.range.frg_len (rU i)

  let reverse_token_list ?(rev=false) (rU:int -> char) (l:token list) : string =
    let rl = if rev then l else (List.rev l) in
    let sl = List.rev_map (reverse_token rU) rl in
    String.concat "" sl

  let file_loc_add_char (c:char) (fl:file_loc) : file_loc =
    if c = '\n'
    then {floc_line = succ fl.floc_line; floc_char = 0}
    else {fl with floc_char = succ fl.floc_char}

  let file_range_add_char (c:char) (fr:file_range) : file_range =
    {
      frg_fst = fr.frg_fst;
      frg_lst = file_loc_add_char c fr.frg_lst;
      frg_len = succ fr.frg_len;
    }

  let lexer (kind:char -> int) (stream:char Stream.t) : token Stream.t =
    let rec parse_word (carry:char list) (fr:file_range) (tkind:int) =
      match Stream.peek stream with
      | Some c when kind c = tkind -> (
        Stream.junk stream;
        parse_word (c::carry) (file_range_add_char c fr) tkind
      )
      | _ -> (fr, SUtils.implode(List.rev carry))
    in
    let rec parse_unary (fr:file_range) (tkind:int) =
      match Stream.peek stream with
      | Some c when kind c = tkind -> (
        Stream.junk stream;
        parse_unary (file_range_add_char c fr) tkind
      )
      | _ -> fr
    in
    let file_loc = ref {floc_line = 0; floc_char = 0} in
    let rec aux _ : token option =
      match Stream.peek stream with
      | None -> None
      | Some c -> (
        Stream.junk stream;
        let tkind = kind c in
        let range0 = {
          frg_fst = !file_loc;
          frg_lst = !file_loc;
          frg_len = 0;
        } in
        let range = file_range_add_char c range0 in
        let token =
               if   tkind = 0    (* Symbol Parsing *)
          then
            {range; tkind; ttoken = Symbol c}
          else if   tkind > 0    (* Word   Parsing *)
          then (
            let range, value = parse_word [c] range tkind in
            {range; tkind; ttoken = Word value}
          )
          else ( (* tkind < 0 *) (* Unary  Parsing *)
            let range = parse_unary range tkind in
            {range; tkind; ttoken = Unary range.frg_len}
          )
        in
        file_loc := token.range.frg_lst;
        Some token
      )
    in
    Stream.from aux
end

module BasicLexer =
struct
  type token =
    | Str of string
    | Int of int

  type stream = token Stream.t

  let string_of_token = function
    | Str str -> "Str "^str
    | Int int -> "Int "^(string_of_int int)

  let python_is_rmline =
    function
    | "#" -> true
    |  _  -> false

  let dimacs_is_rmline =
    function
    | "c" -> true
    |  _  -> false

  let lexer
     ?(is_rmline=(fun (s:string) -> false))
      (char_stream:char Stream.t) : token Stream.t =
    (* kind = function
      | white caracters -> -1 (unary  kind) (i.e. spaces)
      | newline         -> 0  (symbol kind)
      | everything else -> 1  (word   kind)
     *)
    let kind = function
      | ' ' | '\t' | '\r' -> -1
      | '\n'       ->  0
      | _          ->  1
    in
    let stream = SSBL.lexer kind char_stream in
    let rec rmline () =
      try
        let token : SSBL.token = Stream.next stream in
        match token.SSBL.ttoken with
        | SSBL.Symbol '\n' -> ()
        | _ -> rmline()
      with _ -> ()
    in
    let rec aux x : token option =
      match Stream.peek stream with
      | None -> None
      | Some token -> (
        Stream.junk stream;
        match token.SSBL.ttoken with
          (* '\n' is the only character declared as symbol *)
        | SSBL.Symbol '\n' -> aux x
        | SSBL.Symbol   _  -> (failwith "[GuaCaml.ParseUtils] error")
        | SSBL.Word word -> (
          if is_rmline word
          then (rmline(); aux x)
          else (
            let token =
              try Int(int_of_string word)
              with _ -> Str word
            in
            Some token
          )
        )
        (* spaces are discarded *)
        | SSBL.Unary _ -> aux x
      )
    in
    Stream.from aux

  let parse_str stream = match Stream.next stream with
    | Str x -> x
    | token ->
      failwith ("[GuaCaml.BasicLexer.parse_str ("^(string_of_token token)^")")

  let parse_int stream = match Stream.next stream with
    | Int x -> x
    | token ->
      failwith ("[GuaCaml.BasicLexer.parse_int ("^(string_of_token token)^")")
end

(* Similar to BasicLexer but allows for line-parser based detection of comments *)
module CmtLexer =
struct
  type token =
    | Cmt of string
    | Str of string
    | Int of int

  type stream = token Stream.t

  let string_of_token = function
    | Cmt str -> "Str "^(String.escaped str)
    | Str str -> "Str "^(String.escaped str)
    | Int int -> "Int "^(string_of_int int)

  let python_is_rmline =
    function
    | "#" -> true
    |  _  -> false

  let dimacs_is_rmline =
    function
    | "c" -> true
    |  _  -> false

  let lexer
     ?(is_cmtline=(fun (s:string) -> false))
      (char_stream:char Stream.t) : token Stream.t =
    (* kind = function
      | white caracters -> -1 (unary  kind) (i.e. spaces)
      | newline         -> 0  (symbol kind)
      | everything else -> 1  (word   kind)
     *)
    let kind = function
      | ' ' | '\t' | '\r' -> -1
      | '\n'       ->  0
      | _          ->  1
    in
    let stream = SSBL.lexer kind char_stream in
    let rec cmtline_rec (carry:SSBL.token list): string =
      try
        let token : SSBL.token = Stream.next stream in
        match token.SSBL.ttoken with
        | SSBL.Symbol '\n' ->
          (SSBL.reverse_token_list ~rev:true (fun _ -> ' ') carry)
        | _ -> cmtline_rec (token::carry)
      with _ ->
        SSBL.reverse_token_list ~rev:true (fun _ -> ' ') carry
    in
    let cmtline () : string = cmtline_rec [] in
    let rec aux x : token option =
      match Stream.peek stream with
      | None -> None
      | Some token -> (
        Stream.junk stream;
        match token.SSBL.ttoken with
          (* '\n' is the only character declared as symbol *)
        | SSBL.Symbol '\n' -> aux x
        | SSBL.Symbol   _  -> (failwith "[GuaCaml.ParseUtils] error")
        | SSBL.Word word -> (
          if is_cmtline word
          then (Some(Cmt(cmtline ())))
          else (
            let token =
              try Int(int_of_string word)
              with _ -> Str word
            in
            Some token
          )
        )
        (* spaces are discarded *)
        | SSBL.Unary _ -> aux x
      )
    in
    Stream.from aux

  let parse_str stream = match Stream.next stream with
    | Str x -> x
    | token ->
      failwith ("[GuaCaml.BasicLexer.parse_str ("^(string_of_token token)^")")

  let parse_int stream = match Stream.next stream with
    | Int x -> x
    | token ->
      failwith ("[GuaCaml.BasicLexer.parse_int ("^(string_of_token token)^")")
end
