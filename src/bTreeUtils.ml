(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * AB : A Generic Sum Type with two constructors [A of 'a] and [B of 'b]
 *)

(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * BTreeUtils : Toolbox for binary trees (as defined in module Tree)
 *)

open Tree

(* return the number of nodes
   (the number of leaf is 1+(number of nodes)) *)
let size t =
  let rec aux carry = function
    | BLeaf _ -> carry
    | BNode(t0, t1) -> aux (aux carry t0) t1
  in aux 0 t

let rec height = function
  | BLeaf _ -> 0
  | BNode(t0, t1) -> succ(max (height t0) (height t1))

let iter (f:bool list -> 'a -> unit) (t:'a btree) : unit =
  let rec aux addr = function
    | BLeaf a -> f (List.rev addr) a
    | BNode(t0, t1) -> (
      aux (false::addr) t0;
      aux (true ::addr) t1;
    )
  in aux [] t

let reverse (t:'a btree) : ('a, bool list) Hashtbl.t =
  let h = Hashtbl.create (size t) in
  iter (fun addr x -> Hashtbl.add h x addr) t;
  h

let to_stree (dump:'a -> stree) (bt:'a btree) =
  let rec aux = function
    | BLeaf x        -> Node [dump x]
    | BNode (t0, t1) -> Node [aux t0; aux t1]
  in aux bt
let of_stree (load:stree -> 'a) (stree:stree) =
  let rec aux = function
    | Node [leaf]   -> BLeaf (load leaf)
    | Node [t0; t1] -> BNode (aux t0, aux t1)
    | _ -> (failwith "[GuaCaml.BTreeUtils.of_stree] error")
  in aux stree
