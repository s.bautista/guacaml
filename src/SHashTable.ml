(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * SHashTable : Single Hash Table
 *
 * === NOTES ===
 *
 * Dynamic hashing, and resize the table and rehash the
 * elements when buckets become too long.
 * Add an 'hash_key' field to buckets to reduce the number
 * of equality test on keys.
 * the hash function is computed only once for each component
 * Inspired by OCaml's Standard's Library Hashtbl
 *)

type ('a, 'b) t =
  { mutable size: int;                        (* number of entries *)
    mutable data: ('a, 'b) bucketlist array;  (* the buckets *)
    mutable seed: int;                        (* for randomization *)
    mutable initial_size: int;                (* initial array size *)
  }

and ('a, 'b) bucketlist =
    Empty
  | Cons of { mutable  key : 'a;
                  hash_key : int;
              mutable data : 'b;
              mutable next : ('a, 'b) bucketlist }

(* To pick random seeds if requested *)

let randomized_default =
  let params =
    try Sys.getenv "OCAMLRUNPARAM" with Not_found ->
    try Sys.getenv "CAMLRUNPARAM" with Not_found -> "" in
  String.contains params 'R'

let randomized = ref randomized_default

let randomize () = randomized := true
let is_randomized () = !randomized

let prng = lazy (Random.State.make_self_init())

(* Functions which appear before the functorial interface must either be
   independent of the hash function or take it as a parameter (see #2202 and
   code below the functor definitions. *)

(* Creating a fresh, empty table *)

(* [REUSE] Tools.power_2_above *)
let rec power_2_above x n =
  if x >= n then x
  else if x * 2 > Sys.max_array_length then x
  else power_2_above (x * 2) n

let create ?(random = !randomized) initial_size =
  let initial_size = power_2_above 16 initial_size in
  let seed = if random then Random.State.bits (Lazy.force prng) else 0 in
  { initial_size; size = 0; seed; data = Array.make initial_size Empty }

let clear h =
  if h.size > 0 then begin
    h.size <- 0;
    Array.fill h.data 0 (Array.length h.data) Empty
  end

let reset h =
  let len = Array.length h.data in
  if len = h.initial_size then clear h
  else (
    h.size <- 0;
    h.data <- Array.make (abs h.initial_size) Empty
  )

let copy_bucketlist = function
  | Empty -> Empty
  | Cons {key; hash_key; data; next} ->
      let rec loop prec = function
        | Empty -> ()
        | Cons {key; hash_key; data; next} ->
            let r = Cons {key; hash_key; data; next} in
            begin match prec with
            | Empty -> assert false
            | Cons prec ->  prec.next <- r
            end;
            loop r next
      in
      let r = Cons {key; hash_key; data; next} in
      loop r next;
      r

let copy h = { h with data = Array.map copy_bucketlist h.data }

let length h = h.size

external seeded_hash_param :
  int -> int -> int -> 'a -> int = "caml_hash" [@@noalloc]

let hash_key_of_key h key =
  seeded_hash_param 10 100 h.seed key

let index_of_hash_key h hash_key =
  hash_key land (Array.length h.data - 1)

let index_of_key h key =
  let hash_key = hash_key_of_key h key in
  let k = index_of_hash_key h hash_key in
  (hash_key, k)

let internal_add_hash_key h key hash_key data : unit =
  let i = index_of_hash_key h hash_key in
  let next = h.data.(i) in
  h.data.(i) <- Cons{key; hash_key; data; next}

let internal_add h key data : unit =
  let hash_key = hash_key_of_key h key in
  internal_add_hash_key h key hash_key data

let rec insert_bucketlist h : _ -> unit = function
  | Empty -> ()
  | Cons{key; hash_key; data; next} -> (
    internal_add_hash_key h key hash_key data;
    insert_bucketlist h next
  )

let insert_data h data : unit =
  Array.iter (insert_bucketlist h) data

let insert h h' = insert_data h h'.data

let upsize h : unit =
  let odata = h.data in
  let osize = Array.length odata in
  let nsize = osize * 2 in
  if nsize < Sys.max_array_length then (
    h.data <- Array.make nsize Empty;
    insert_data h odata
  )

let rec iter_bucketlist f = function
  | Empty -> ()
  | Cons{key; hash_key; data; next} ->
    (f key data; iter_bucketlist f next)

let iter f h =
  Array.iter (iter_bucketlist f) h.data

let set_data bucket data : unit =
  match bucket with
  | Empty -> assert false
  | Cons cell -> cell.data <- data

let set_prec bucket prec : unit =
  match bucket with
  | Empty -> assert false
  | Cons cell -> cell.next <- prec

let rec filter_map_inplace_bucketlist_inner f h prec bucket : unit =
  match bucket with
  | Empty -> ()
  | Cons ({key; hash_key; data; next} as cell) -> (
    match f key data with
    | None -> filter_map_inplace_bucketlist_inner f h prec next
    | Some data -> (
      cell.data <- data;
      set_prec prec bucket;
      filter_map_inplace_bucketlist_inner f h bucket next
    )
  )

let rec filter_map_inplace_bucketlist_outer f h i bucket =
  match bucket with
  | Empty -> h.data.(i) <- Empty
  | Cons ({key; hash_key; data; next} as cell) -> (
    match f key data with
    | None -> filter_map_inplace_bucketlist_outer f h i next
    | Some data -> (
      cell.data <- data;
      h.data.(i) <- bucket;
      filter_map_inplace_bucketlist_inner f h bucket next
    )
  )

let filter_map_inplace f h : unit =
  Array.iteri (fun i bk -> filter_map_inplace_bucketlist_outer f h i bk) h.data

let rec fold_bucketlist f init = function
  | Empty -> init
  | Cons{key; hash_key; data; next} ->
    fold_bucketlist f (f key data init) next

let fold f h init =
  Array.fold_left (fold_bucketlist f) init h.data

type statistics = {
  num_bindings: int;
  num_buckets: int;
  max_bucket_length: int;
  bucket_histogram: int array
}

let rec bucketlist_length accu = function
  | Empty -> accu
  | Cons{next} -> bucketlist_length (succ accu) next

let stats h =
  let ha = Array.map (bucketlist_length 0) h.data in
  let mbl = Array.fold_left max 0 ha in
  let histo = Array.make (mbl + 1) 0 in
  Array.iter (fun h -> histo.(h) <- histo.(h) + 1) ha;
  { num_bindings = h.size;
    num_buckets = Array.length h.data;
    max_bucket_length = mbl;
    bucket_histogram = histo }

let hash x = seeded_hash_param 10 100 0 x
let hash_param n1 n2 x = seeded_hash_param n1 n2 0 x
let seeded_hash seed x = seeded_hash_param 10 100 seed x

let add h key data =
  internal_add h key data;
  h.size <- succ h.size;
  if h.size > Array.length h.data * 2 then upsize h

let rec remove_bucketlist_inner h key0 hash_key0 prec bucket =
  match bucket with
  | Empty -> ()
  | Cons{key; hash_key; data; next} -> (
    if hash_key = hash_key0 && key = key0
    then (
      h.size <- pred h.size;
      set_prec prec next
    )
    else remove_bucketlist_inner h key0 hash_key0 bucket next
  )

let remove_bucketlist_outer h key0 hash_key0 i =
  let bucket = h.data.(i) in
  match bucket with
  | Empty -> ()
  | Cons{key; hash_key; data; next} -> (
    if hash_key = hash_key0 && key = key0
    then (
      h.size <- pred h.size;
      h.data.(i) <- next
    )
    else (
      remove_bucketlist_inner h key0 hash_key0 bucket next
    )
  )

let remove h key =
  let hash_key, i = index_of_key h key in
  remove_bucketlist_outer h key hash_key i

let rec find_bucketlist key0 hash_key0 = function
  | Empty -> None
  | Cons{key; hash_key; data; next} -> (
      if hash_key == hash_key0 && key = key0
      then Some data
      else find_bucketlist key0 hash_key0 next
  )

let find_opt h key : _ option =
  let hash_key, i = index_of_key h key in
  find_bucketlist key hash_key h.data.(i)

let find h key : _ =
  match find_opt h key with
  | Some data -> data
  | None      -> raise Not_found

let rec find_all_bucketlist carry key0 hash_key0 = function
  | Empty -> List.rev carry
  | Cons{key; hash_key; data; next} -> (
    if hash_key = hash_key0 && key = key0
    then find_all_bucketlist (data::carry) key0 hash_key0 next
    else find_all_bucketlist        carry  key0 hash_key0 next
  )

let find_all h key : _ list =
  let hash_key, i = index_of_key h key in
  find_all_bucketlist [] key hash_key h.data.(i)

let rec replace_bucketlist key0 hash_key0 data0 = function
  | Empty -> None
  | Cons ({key; hash_key; data; next} as cell) -> (
    if hash_key = hash_key0 && key = key0
    then (cell.key <- key0; cell.data <- data; Some data)
    else (replace_bucketlist key0 hash_key0 data0 next)
  )

let replace h key data =
  let hash_key, i = index_of_key h key in
  ignore(replace_bucketlist key hash_key data h.data.(i))

let mem h key = find_opt h key <> None
