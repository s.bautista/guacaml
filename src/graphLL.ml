(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraphLL : represents graphs as a list of adjacacency list
 *)

open Extra
open STools
open BTools

type vertex = {
  index : int;
  edges : int list;
}

type graph = vertex list

let get_vertices (graph:graph) : int list =
  graph
  ||> (fun v -> v.index)
  |> Tools.check SetList.sorted_nat

(* [LATER]
let to_GraphLA (graph:graph) : GraphLA.graph =
  let vertices = get_vertices graph in
  let
 *)

