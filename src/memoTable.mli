(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * MemoTable : generic memoization facility
 *)

type ('a, 'b) t

val create : int -> ('a, 'b)t
val default_size : int
val make   : int -> ('a, 'b)t * (('a -> 'b) -> 'a -> 'b)

val test : ('a, 'b) t -> 'a -> bool
val push : ('a, 'b) t -> 'a -> 'b -> unit
val memo : ('a, 'b) t -> 'a -> 'b -> 'b
val pull : ('a, 'b) t -> 'a -> 'b

val apply : ('a, 'b) t -> ('a -> 'b) -> 'a -> 'b

val print_stats : ('a, 'b) t -> unit
val dump_stats : ('a, 'b) t -> Tree.stree

val clear : ('a, 'b) t -> unit
val reset : ('a, 'b) t -> unit
