(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * BTools_BArray_Nat [INTERNAL] : arbitrary length natural numbers
 * remark : recommended alias BNat
 *)

open STools
open BTools_BArray
open BTools_BArray.DEBUG

let show_debug = ref false

(* \forall n>=0, length(sized_zero n) = n *)
let sized_zero n = make n false
(* length(zero()) = 0 *)
let zero () = sized_zero 0
(* \forall n>=1, length(sized_unit n) = n *)
let sized_unit n =
  assert(n>=1);
  let ba = make n false in
  unsafe_set ba (n-1) true;
  ba
(* length(unit()) = 1 *)
let unit () = sized_unit 1

let compare (ba1:t) (ba2:t) : int =
  assert BTools_BArray.(checked ba1);
  assert BTools_BArray.(checked ba2);
  let len1 = Bytes.length ba1.bytes
  and len2 = Bytes.length ba2.bytes in
  let rec tail_is_zero (ba:Bytes.t) (i:int) : bool = if i < (Bytes.length ba)
    then if get_byte ba i = 0
      then (tail_is_zero ba (i+1))
      else false
    else true
  in
  let rec aux i = if i >= 0 then (
      let cmp = Stdlib.compare (get_byte ba1.bytes i) (get_byte ba2.bytes i) in
      if cmp = 0
        then (aux (i-1))
        else cmp
    ) else 0
  in
         if len1 < len2 then (
      if tail_is_zero ba2.bytes len1
      then aux (len1-1)
      else -1
  ) else if len1 > len2 then (
      if tail_is_zero ba1.bytes len2
      then aux (len2-1)
      else 1
  ) else (* len1 = len2 *)        (aux (len1-1))

let eq (ba1:t) (ba2:t) : bool = compare ba1 ba2 = 0
let ( =/ ) = eq
let lt (ba1:t) (ba2:t) : bool = compare ba1 ba2 < 0
let ( </ ) = lt
let gt (ba1:t) (ba2:t) : bool = compare ba1 ba2 > 0
let ( >/ ) = gt
let le (ba1:t) (ba2:t) : bool = compare ba1 ba2 <= 0
let ( <=/ ) = le
let ge (ba1:t) (ba2:t) : bool = compare ba1 ba2 >= 0
let ( >=/ ) = ge

let normalize (ba:t) =
  let ba' = match lst_true ba with
    | None -> zero ()
    | Some lst -> (
      let len, ba' = bytes_create "BArray.Nat.normalize" (lst+1) in
      Bytes.unsafe_blit ba.bytes 0 ba'.bytes 0 len;
      ba'
    )
  in
  assert(ba =/ ba');
  ba'

(* \forall ba, length (resize ba n) = n *)
let resize   (ba:t) (n:int) : t =
  if n < ba.length then (
      assert(sub_is_zero ba (n+1) (ba.length-(n+1)));
      sub ba 0 n
    ) else if n > ba.length then (
      append_zeros_last ba (n-ba.length)
    ) else (copy ba)

let truncate (ba:t) (n:int) : t =
  sub ba 0 n

let of_sized_int (x:int) (n:int) : t =
  (* FIXME for 32-bits machines *)
  if x < 0 || (n <= 63 && x >= (1 %+ n)) then invalid_arg "BArray.of_sized_int";
  (* FIXME raise LT0 exception if [x < 0] *)
  let len, ba = bytes_create "BArray.of_sized_int" n in
  let rec aux carry i = if i < len then (
      let carry, modulo = Tools.int_quomod carry 256 in
      set_byte ba.bytes i modulo;
      aux carry (i+1)
    ) else carry
  in
  assert(aux x 0 = 0);
  ba

(* \forall x:int, length(of_int x) = 63 *)
let of_int (x:int) : t =
  if x < 0 then invalid_arg "BArray.of_int";
  (* FIXME for 32-bits machines *)
  let len, ba = bytes_create "BArray.of_int" 63 in
  let rec aux carry i = if i < len then (
      let carry, modulo = Tools.int_quomod carry 256 in
      set_byte ba.bytes i modulo;
      aux carry (i+1)
    ) else carry
  in
  assert(aux x 0 = 0);
  ba

let is_int (ba:t) =
  (* FIXME of 32-bits machines *)
  ba.length <= 63

let to_int (ba:t) =
  if ba.length > 63 then invalid_arg "BArray.to_int";
  let len = Bytes.length ba.bytes in
  (* OPTME? unroll (as len <= 4) *)
  let rec aux carry i = if i < len then (
      aux (carry lor ((get_byte ba.bytes i) %+ (i %+ 3))) (i+1)
    ) else carry
  in aux 0 0

let print_base2 (ba:t) =
  let ba = normalize ba in
  print_string "0b"; print_string(to_bool_string (ba |> rev));
  (* FIXME deal with 32-bit achitecture *)
  if ba.length < 62 then (
    print_string " ( "; print_int(to_int ba); print_string " )"
  )

exception CarryOverflow of int

let inplace_carry (ba:t) shift carry : unit =
  let bytes = ba.bytes in
  let len = Bytes.length bytes in
  let rec aux carry i =
    if carry = 0 then ()
    else if i < len then (
      let bytes_i = get_byte bytes i in
      let value = carry + bytes_i in
      let carry', modulo = Tools.int_quomod value 256 in
      set_byte bytes i modulo;
      aux carry' (i+1)
    ) else raise(CarryOverflow carry)
  in aux carry shift

let inplace_add (ba1:t) factor2 shift2 carry2 (ba2:t) : unit =
  let bytes1 = ba1.bytes in
  let bytes2 = ba2.bytes in
  let len1 = Bytes.length bytes1 in
  let len2 = Bytes.length bytes2 in
  assert(0<=shift2 && len2+shift2<=len1);
  let rec aux carry i = if i < len2 then (
      let cell_bytes1 = get_byte bytes1 (shift2+i)
      and cell_bytes2 = get_byte bytes2 i in
      let value = carry + cell_bytes1 + factor2 * cell_bytes2 in
      let carry', modulo = Tools.int_quomod value 256 in
      set_byte bytes1 (shift2+i) modulo;
      aux carry' (i+1)
    ) else (inplace_carry ba1 (shift2+len2) carry)
  in
  aux carry2 0

let add     (ba1:t) (ba2:t) : t =
  (* implemented add assuming ba1.length >= ba2.length *)
  let aux_add (ba1:t) (ba2:t) : t =
    let ba1' = append_zeros_last ba1 1 in
    (* inplace_add (ba1:t) factor2 shift2 carry2 (ba2:t) : unit *)
    inplace_add ba1' 1 0 0 ba2;
    normalize ba1'
  in
  if ba2.length > ba1.length
  then aux_add ba2 ba1
  else aux_add ba1 ba2

(* Lower Than Zero *)
exception LowerThanZero of t

exception LT0

(* \forall ba x, length(addi ba x) = min(length ba, 63)+1 *)
let addi    (ba:t)  (x:int) : t =
  (* works fine if x < 0 *)
  assert(x < max_int-256);
  let length' = max (ba.length+1) 64 in
  let ba' = append_zeros_last ba (length'-ba.length) in
  try
    inplace_carry ba' 0 x;
    ba'
  with
    CarryOverflow x -> (
      assert(x<0);
      raise LT0
    )

let ( +/ ) = add

(* assume [ba1] >= [ba2] (otherwise raise exception [LT0]) *)
let error_minus (ba1:t) (ba2:t) : t =
  let ba2 = if ba2.length > ba1.length
    then if sub_is_zero ba2 ba1.length (ba2.length-ba1.length)
      then (normalize ba2)
      else (raise LT0)
    else ba2 in
  let ba1' = copy ba1 in
  try
    inplace_add ba1' (-1) 0 0 ba2;
    ba1'
  with
    CarryOverflow _ -> (raise LT0)

(* if [ba1] >= [ba2]
   then [[ba1]-[ba2]]
   else (raise LowerThanZero [[ba2]-[ba1]]
 *)
let minus   (ba1:t) (ba2:t) : t =
  assert BTools_BArray.(checked ba1);
  assert BTools_BArray.(checked ba2);
  try
    let ba3 = error_minus ba1 ba2 in
    assert((ba3+/ba2)=/ba1);
    normalize ba3
  with
  | LT0 ->
    try
      let ba3 = error_minus ba2 ba1 in
      assert((ba3+/ba1)=/ba2);
      raise (LowerThanZero ba3)
    with
    | LT0 -> (failwith "[GuaCaml.BTools_BArray_Nat] error")

let ( -/ ) = minus
let minusi  (ba:t)  (x:int) : t =
  let ba' = copy ba in
  try
    inplace_carry ba' 0 (-x);
    ba'
  with
    CarryOverflow x -> (
      assert(x<0);
      raise (LowerThanZero(of_int(x-(to_int ba))))
    )

(* \forall ba1 ba2, N1:=length ba1, N2:=length ba2, M:=length(mult ba1 ba2),
  M = match N1, N2 with
    | 0, _ | _, 0 -> 0
    | 1, N | N, 1 -> N
    | _ -> N1 + N2

   \forall ba1 ba2, [mult ba1 ba2] = [ba1] * [ba2]
 *)
let mult    (ba1:t) (ba2:t) : t =
  (* [ba1] <= 2^{|ba1|} - 1 /\ [ba2] <= 2^{|ba2|}-1
     [ba1]*[ba2] <= 2^{|ba1|+|ba2|} - 2^{|ba1|} - 2^{|ba2|} + 1 (denoted UB)
     cmp UB 2^{|ba1|+|ba2|} = cmp 1 \left(2^{|ba1|} + 2^{|ba2|}\right)
                            = [ < ]
     if |ba1| = |ba2| = 0 then |[[ba1]*[ba2]]| = 0
     else if |ba1| = 1 then |[[ba1]*[ba2]]| = |ba2|
     else if |ba2| = 1 then |[[ba1]*[ba2]]| = |ba1|
     otherwise (i.e. |ba1| > 1 /\ |ba2| > 1)
   *)
  let length1 = ba1.length
  and length2 = ba2.length in
  let bytes1 = ba1.bytes in
  let len1 = Bytes.length bytes1 in
       if length1 = 0 then (copy ba1)
  else if length2 = 0 then (copy ba2)
  else if length1 = 1 then (if unsafe_get ba1 0
    then (copy ba2)
    else (sized_zero length2))
  else if length2 = 1 then (if unsafe_get ba2 0
    then (copy ba1)
    else (sized_zero length1))
  else (
    let ba' = make (length1+length2) false in
    for shift = 0 to len1 - 1
    do
      let factor = get_byte bytes1 shift in
      if factor > 0 then
        (inplace_add ba' factor shift 0 ba2)
    done;
    ba'
  )

let multi   (ba:t)  (x:int) : t =
  if x = 0 then (zero())
  else if x = 1 then (copy ba)
  else (mult ba (of_int x))

let ( */ ) = mult

(* we assume 0 <= [i]
   length(shift_lefti ba i) = (length ba) + [i]
   [shift_lefti ba i] = [ba]*(2^[i])
 *)
let shift_lefti  (ba:t) (i:int) : t =
  assert(0 <= i);
  append_zeros_first ba i

(* we assume 0 <= [i]
   length(shift_righti ba i) = max((length ba) - [i], 0)
   [shift_righti ba i] = [ba]/(2^[i])
 *)
let shift_righti (ba:t) (i:int) : t =
  if i >= ba.length
  then (create 0)
  else (unsafe_drop_first ba i)

(* we assume -oo < [i] < +oo
  length(shift ba i) = max((length ba) + i, 0)
  [shift ba i] = [ba]*(2^[i])
 *)
let shifti (ba:t) (i:int) : t =
       if i = 0 then (copy ba)
  else if i > 0 then (shift_lefti ba i)
  else (* i < 0 *)   (shift_righti ba (-i))

let pow2 (x:int) : t =
  let ba = make (x+1) false in
  unsafe_set ba x true;
  ba

(* [quomod ba1 ba2 = (div12, mod12) where:
  - div12 = ba1  /  ba2
  - mod12 = ba1 mod ba2
 *)
let quomod  (ba1:t) (ba2:t) : t * t   =
  let ba' = make ba1.length false in
  let length2 = ba2.length in
  let ba2 = normalize ba2 in
  assert(get ba2 (ba2.length-1) = true);
  assert(ba2.length > 0); (* thus [ba2 >/ 0 = true] *)
  let rec aux ba1 = (* we assume ba1 to be normalized *)
    assert(ba1.length = 0 || get ba1 (ba1.length-1) = true);
    if ba1.length < ba2.length || ba1 </ ba2
    then (resize ba1 length2)
    else (
      assert(ba1.length >= ba2.length);
      (* we define ba2' as ba2 with its highest bit aligned
         with the highest bit of ba1 *)
      let delta = ba1.length - ba2.length in
      let ba2' = shift_lefti ba2 delta in
      assert(ba1.length = ba2'.length);
      assert(get ba2' (ba1.length-1) = true);
      (* OPTME use the exception raised by error_minus to save a comparison *)
      if ba2' >/ ba1
      then (
        assert(get ba' (delta-1) = false);
        set ba' (delta-1) true;
        let ba2'' = shift_lefti ba2 (delta-1) in
        let ba1' = normalize (error_minus ba1 ba2'') in
        assert(ba1'.length < ba1.length);
        assert(ba1' </ ba2'');
        aux ba1'
      )
      else (
        assert(get ba' delta = false);
        set ba' delta true;
        let ba1' = normalize(error_minus ba1 ba2') in
        assert(ba1'.length < ba1.length);
        assert(ba1' </ ba2');
        aux ba1'
      )
    )
  in
(*
  let q, r = (ba', aux (normalize ba1)) in
  assert(ba1 =/ ((q */ ba2) +/ r));
  assert(zero() <=/ r && r </ ba2);
  (q, r)
*)
  (ba', aux(normalize ba1))

let quomodi (ba:t)  (x:int) : t * int =
  let q, r = quomod ba (of_int x) in
  (q, to_int r)
let div     (ba1:t) (ba2:t) : t = fst (quomod  ba1 ba2)
let divi    (ba:t)  (x:int) : t = fst (quomodi ba  x  )
let ( // ) = div
let modulo  (ba1:t) (ba2:t) : t = snd (quomod ba1 ba2)
let ( mod ) = modulo
let modi    (ba:t)  (x:int) : int = snd (quomodi ba x)
let succ    (ba:t) : t = addi ba 1
let pred    (ba:t) : t = minusi ba 1
let log2_inf  (ba:t) : int =
  match lst_true ba with
  | None -> (failwith "[GuaCaml.BTools_BArray_Nat] error") (* log 0 is undefined *)
  | Some log -> log
let log2_sup  (ba:t) : int =
  match lst_true ba with
  | None -> (failwith "[GuaCaml.BTools_BArray_Nat] error")
  | Some log ->
    if sub_is_zero ba 0 log
    then log
    else (log+1)

let to_string (ba:t) : string =
  let rec aux carry ba =
    if ba =/ (zero())
    then (SUtils.implode carry)
    else (
      let ba', r = quomodi ba 10 in
      let c = Char.chr((Char.code '0')+r) in
      aux (c::carry) ba'
    )
  in
  if ba =/ (zero())
  then "0"
  else (aux [] ba)

let to_pretty_string (ba:t) : string =
  ba |> to_string |> Tools.prettyfy_nat

let print (ba:t) : unit =
  print_string(to_string ba)

let of_string (s:string) : t =
  let rec aux (carry:t) i = if i < String.length s
    then
      let x = (Char.code (String.unsafe_get s i)) -
        (Char.code '0') in
      assert(0<=x && x<10);
      aux (carry */ (of_int 10) +/ (of_int x)) (i+1)
    else carry
  in
  aux (zero()) 0

let max (ba1:t) (ba2:t) : t =
  if (ge ba1 ba2) then ba1 else ba2
let min (ba1:t) (ba2:t) : t =
  if (ge ba1 ba2) then ba2 else ba1

(* mm ba1 ba2 = \in \{ (ba1, ba2), (ba2, ba1) \}
   mm ba1 ba2 = (max ba1 ba2, min ba1 ba2)
   fst (mm ba1 ba2) >= snd (mm ba1 ba2)
 *)
let mm  (ba1:t) (ba2:t) : t * t =
  if (ge ba1 ba2) then (ba1, ba2) else (ba2, ba1)

(* (* With Memoization *)
let factorial_memo : (int, t) Hashtbl.t = Hashtbl.create 100

let rec factorial (n:int) =
  assert(n>=0);
  if n <= 1 then unit() else (
    try
      copy(Hashtbl.find factorial_memo n)
    with Not_found ->
      let res = multi (factorial (n-1)) n in
      Hashtbl.add factorial_memo n res;
      copy res
  )
 *)

(* (* Without Memoization *) *)
let rec factorial (n:int) =
  assert(n>=0);
  if n <= 1 then unit() else (
    multi (factorial (n-1)) n
  )

let to_bicimal (t:t) : int list = to_list t
let of_bicimal (il:int list) : t =
  assert(SetList.sorted_nat il);
  of_list il
