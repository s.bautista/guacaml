(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * TreeUtils : tools for generic n-ary trees
 *)

open Tree

module ToS =
struct
  open STools.ToS
  let atree (fa:'a t) (fb:'b t) (t:('a, 'b) atree) : string =
    let rec aux = function
    | ALeaf  b -> "ALeaf ("^(fb b)^")"
    | ANode (a, l) -> "ANode "^((fa * (list aux)) (a, l))
    in aux t

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl

  let gnext (fa:'a t) (fb:'b t) : ('a, 'b) gnext -> string =
    function
    | GLeaf a -> "GLeaf ("^(fa a)^")"
    | GLink b -> "GLink ("^(fb b)^")"
end

module ToSTree =
struct
  open STools.ToSTree
  let atree (fa:'a t) (fb:'b t) (t:('a, 'b) atree)  =
    let rec aux : ('a, 'b) atree t = function
    | ALeaf  b ->
      Node [Leaf "ALeaf"; fb b]
    | ANode (a, l) ->
      Node [Leaf "ANode"; (fa * (list aux)) (a, l)]
    in aux t

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl
end

module OfSTree =
struct
  open STools.OfSTree
  let atree (fa:'a t) (fb:'b t) stree =
    let rec aux = function
    | Node[Leaf "ALeaf"; data] ->
      let b = fb data in
      ALeaf b
    | Node[Leaf "ANode"; data] ->
      let a, l = (fa * (list aux)) data in
      ANode (a, l)
    | _ -> (failwith "[GuaCaml.TreeUtils] error")
    in aux stree

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl
end

module ToBStream =
struct
  open BTools.ToBStream

  let gnext (aa:'a t) (bb:'b t)
    (cha:Channel.t) (next:('a, 'b) gnext) : unit =
    match next with
    | GLeaf a ->
    (
      bool cha false;
      aa cha a;
    )
    | GLink b ->
    (
      bool cha true;
      bb cha b;
    )
end

module OfBStream =
struct
  open BTools.OfBStream

  let gnext (aa:'a t) (bb:'b t)
    (cha:Channel.t) : ('a, 'b) gnext =
    match bool cha with
    | false -> GLeaf (aa cha)
    | true  -> GLink (bb cha)
end

module ToB =
struct
  open BTools.ToB

  let gnext (aa:'a t) (bb:'b t) (g:('a, 'b)gnext) (s:stream) =
    match g with
    | Tree.GLeaf a -> (bool false(aa a s))
    | Tree.GLink b -> (bool true (bb b s))
end

module OfB =
struct
  open BTools.OfB

  let gnext (aa:'a t) (bb:'b t) (s:stream) : ('a, 'b)gnext * stream =
    let b, s = bool s in
    match b with
    | false -> (
      let a, s = aa s in
      (Tree.GLeaf a, s)
    )
    | true  -> (
      let b, s = bb s in
      (Tree.GLink b, s)
    )
end

module IoB =
struct
open BTools.IoB

  let gnext a b = (ToB.gnext (fst a) (fst b), OfB.gnext (snd a) (snd b))
end


let rec aatree_normalized = function
  | ALeaf _ -> true
  | ANode (_, []) -> false
  | ANode (_, tl) -> aatrees_normalized tl
and    aatrees_normalized (tl:_ aatrees) =
  List.for_all aatree_normalized tl

let rec aatree_normalize = function
  | ALeaf x
  | ANode (x, []) -> ALeaf x
  | ANode (x, tl) -> ANode (x, aatrees_normalize tl)
and    aatrees_normalize (tl: _ aatrees) =
  Tools.map aatree_normalize tl

(* @santiago.bautista *)
let aatree_of_ialist_insert ((i0, e0): int * 'a) (l: (int*('a aatree)) list) : (int*('a aatree)) list =
  let heads, tails =
    MyList.find_onehash_prefix ~p:(fun (i, _) -> i > i0) fst l
  in
  (i0, ANode(e0, Tools.map snd heads))::tails

(* @santiago.bautista *)
let aatree_of_ialist (l:(int * 'a) list) : 'a aatrees =
  let rec aux (left: (int * 'a) list) (l: (int*('a aatree)) list) : _ aatrees =
    match left with
    | [] ->
    ( (* finalizer *)
      assert(MyList.onehash fst l);
      Tools.map snd l
    )
    | line::left -> aux left (aatree_of_ialist_insert line l)
  in
  aatrees_normalize (aux (List.rev l) [])

let unGLeaf = function
  | GLeaf lf -> lf
  | GLink _  -> (failwith "[GuaCaml.TreeUtils] error")
let unGLink = function
  | GLeaf _  -> (failwith "[GuaCaml.TreeUtils] error")
  | GLink lk -> lk

let ungnext = function GLeaf x | GLink x -> x

let filterGLeaf (l:('a, 'b) gnext list) : 'a list =
  let rec filterGLeaf_rec carry = function
    | [] -> List.rev carry
    | (GLeaf a)::t -> filterGLeaf_rec (a::carry) t
    | (GLink _)::t -> filterGLeaf_rec     carry  t
  in filterGLeaf_rec [] l

let filterGLink (l:('a, 'b) gnext list) : 'b list =
  let rec filterGLink_rec carry = function
    | [] -> List.rev carry
    | (GLink b)::t -> filterGLink_rec (b::carry) t
    | (GLeaf _)::t -> filterGLink_rec     carry  t
  in filterGLink_rec [] l

let filterGLeaf_array (a:('a, 'b) gnext array) : 'a list =
  let rec filterGLeafa_rec carry array i =
    if i < 0 then carry else (
      match array.(i) with
      | GLeaf a -> filterGLeafa_rec (a::carry) array (pred i)
      | GLink _ -> filterGLeafa_rec     carry  array (pred i)
    )
  in filterGLeafa_rec [] a (Array.length a -1)

let filterGLink_array (a:('a, 'b) gnext array) : 'b list =
  let rec filterGLinka_rec carry array i =
    if i < 0 then carry else (
      match array.(i) with
      | GLink b -> filterGLinka_rec (b::carry) array (pred i)
      | GLeaf _ -> filterGLinka_rec     carry  array (pred i)
    )
  in filterGLinka_rec [] a (Array.length a -1)
