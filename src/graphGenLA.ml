(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraGenLA : Generic Adjacency List Graph
 *
 * === NOTE ===
 *
 * neighbors : prefer "neighbors" rather than "neighbours" or "neighborhood"
 *)

open Extra
open STools
open BTools

module Type =
struct

  type ('v, 'e) vertex = {
    index : int;
    tag   : 'v;
    edges : (int * 'e) list;
  }
  (* adjacency list representation
    - [vertex.edges] is sorted by (strictly) increasing order
    - each element [d] in [vertex.edges] is :
      + d >= 0
      + d < Array.length graph
   *)

  type ('v, 'e) graph = ('v, 'e) vertex array
  (* adjacency list representation.
    - for each index i \in [0 ... n-1] :
      + graph.(i).index = i
    - G = (V, E) where:
      + V = {0; ...; Array.length graph - 1}
      + E = {(s, d) for d in v.edges for v in graph}
   *)

   type ('v, 'e) opgraph = ('v, 'e) vertex option array
   (* same as graph but with potential dead vertices *)
end

open Type

module ToS =
struct
  open ToS

  let vertex (sv:'v t) (se:'e t) (v:('v, 'e) vertex) : string =
    "{index= "^(int v.index)^
     "; tag= "^(sv v.tag)^
   "; edges= "^(list(int * se)v.edges)^"}"

  let graph sv se (g:('v, 'e) graph) : string =
    "{GGLA}"^(array(vertex sv se)g)

  let pprint_graph sv se (g:('v, 'e)graph) : unit =
    MyArray.pprint_array (vertex sv se) g

  let pprint_opgraph sv se (g:('v, 'e)opgraph) : unit =
    MyArray.pprint_array (option (vertex sv se)) g
end

module ToB =
struct
  open ToB

  let edges (se:'e t) (edges:(int * 'e) list) (s:stream) : stream =
    list (int * se) edges s

  let vertex (sv:'v t) (se:'e t) (v:('v, 'e)vertex) (s:stream) : stream =
    trio int sv (edges se) (v.index, v.tag, v.edges) s

  let graph (sv:'v t) (se:'e t) (g:('v, 'e)graph) (s:stream) : stream =
    array (vertex sv se) g s

  let opgraph (sv:'v t) (se:'e t) (g:('v, 'e)opgraph) (s:stream) : stream =
    array (option (vertex sv se)) g s
end

module OfB =
struct
  open OfB

  let edges (se:'e t) (s:stream) : (int * 'e) list * stream =
    list (int * se) s

  let vertex (sv:'v t) (se:'e t) (s:stream) : ('v, 'e) vertex * stream =
    let (index, tag, edges), stream = trio int sv (edges se) s in
    {index; tag; edges}, stream

  let graph (sv:'v t) (se:'e t) (s:stream) : ('v, 'e) graph * stream =
    array (vertex sv se) s

  let opgraph (sv:'v t) (se:'e t) (s:stream) : ('v, 'e) opgraph * stream =
    array (option (vertex sv se)) s
end

module Check =
struct
  (* Time Complexity : O([List.length v.edges]) *)
  let vertex ?(multi_edge=false) (len:int) (idx:int) (v:(_,_)vertex) : bool =
    let p = if multi_edge
      then (fun u v -> fst u <= fst v)
      else (fun u v -> fst u <  fst v)
    in
    v.index = idx &&
    (MyList.for_all_succ
      ~fst:(fun (vi, _) -> 0 <= vi)
      ~lst:(fun (vi, _) -> vi < len)
           p
           v.edges
    )

  (* Time Complexity O( |V| + |E| ) *)
  let graph ?(multi_edge=false) (g:(_, _)graph) : bool =
    let len = Array.length g in
    MyArray.for_alli (vertex ~multi_edge len) g
end

module Normalize =
struct

  let vertex (v:('v, 'e)vertex) : ('v, 'e) vertex =
    let edges = List.stable_sort
      (fun u v -> Stdlib.compare (fst u) (fst v))
      v.edges
    in
    {v with edges}

  let graph (g:('v, 'e)graph) : ('v, 'e) graph =
    Array.map vertex g

  (* turn multi-edge vertex into single-edge vertex *)
  let vertex_collapse (v:('v, 'e)vertex) : ('v, 'e list) vertex =
    let edges = MyList.collapse_first v.edges in
    { index = v.index;
      tag   = v.tag;
      edges }

  (* turn multi-edge graph into single-edge graph *)
  let graph_collapse (g:('v, 'e)graph) : ('v, 'e list) graph =
    Array.map vertex_collapse g
end

module Utils =
struct
  let vertex_map_vtag (fv:'va -> 'vb) (v:('va, 'e)vertex) : ('vb, 'e)vertex =
    {
      index = v.index;
      tag = fv v.tag;
      edges = v.edges
    }

  let graph_map_vtag (fv:'va -> 'vb) (g:('va, 'e)graph):('vb, 'e)graph =
    Array.map (vertex_map_vtag fv) g

  let vertex_map_etag (fe:'ea -> 'eb) (v:('v, 'ea)vertex) : ('v, 'eb)vertex =
    {
      index = v.index;
      tag = v.tag;
      edges = List.map (fun (i, e) -> (i, fe e)) v.edges;
    }

  let graph_map_etag (fe:'ea -> 'eb) (g:('v, 'ea)graph) : ('v, 'eb)graph =
    Array.map (vertex_map_etag fe) g

  let vertex_map_both (fv:'va -> 'vb) (fe:'ea -> 'eb) (v:('va, 'ea)vertex) : ('vb, 'eb)vertex =
    {
      index = v.index;
      tag = fv v.tag;
      edges = List.map (fun (i, e) -> (i, fe e)) v.edges;
    }

  let graph_map_both (fv:'va -> 'vb) (fe:'ea -> 'eb) (g:('va, 'ea)graph) : ('vb, 'eb)graph =
    Array.map (vertex_map_both fv fe) g

  (*  transpose ~reverse=false g = tg
        if [reverse] = [true] then  (
          edge lists are reversed before transposition
          which allows for the relative order of
          same destination multi-edges to be preserved
        )
   *)
  let transpose
      ?(reverse=false)
      (g:('v,'e)graph) : ('v,'e)graph =
    let len = Array.length g in
    let tg = Array.make len [] in
    for i = len-1 downto 0
    do
      List.iter
        (fun(j, e) -> tg.(j) <- (i, e)::tg.(j))
        (
          if reverse
          then (List.rev g.(i).edges)
          else           g.(i).edges
        )
    done;
    Array.map2 (fun v edges -> {v with edges}) g tg

  (*  [sons_set ~reflexive g set = neigh]
        ensures [SetList.sorted_nat neigh]
        match reflexive with
        | Some true  -> neigh = reachable in at most one step
        | None       -> neigh = reachable in exactly one step
        | Some false -> neigh = neigh(None) \setminus set
   *)
  let vertex_list_sons
     ?(reflexive=None)
      (g:('v, 'e)graph)
      (set:int list) : int list =
    assert(SetList.sorted_nat set);
    let len  = Array.length g in
    let vect = BArray.make len false in
    List.iter (fun u ->
      assert(0 <= u && u < len);
      List.iter
        (fun (v, _) -> BArray.set vect v true)
        g.(u).edges
    ) set;
    (match reflexive with
    | Some b -> (
      List.iter (fun u -> BArray.set vect u b) set
    )
    | None -> ());
    vect |> BArray.to_list

  let neighbors_set g set = vertex_list_sons g set
  [@@ocaml.deprecated "Use [vertex_list_sons g set]."]

  (* [TODO] *)
  (* 'neigh(None) \setminus set' *)
  let vertex_list_adjacent g set : int list =
    vertex_list_sons ~reflexive:(Some false) g set

  (* [TODO] *)
  (* returns 'reachable in at most one step' *)
  let vertex_list_neighbors g set : int list =
    vertex_list_sons ~reflexive:(Some true) g set

  (* [internal_vertex_edges_rename g r = g']
     apply a renaming vector [r] to all edges destination.
     if [r] is monotonous then [g'] is normalized
     otherwise, one can use [Normalize.graph g' = g'']
     to compute a normalized graph
   *)
  let internal_vertex_edges_rename
      (lenr:int)
      (r:int array)
      (v:('v, 'e)vertex) : ('v, 'e) vertex =
    let index = r.(v.index) in
    let edges = List.map
      (fun (u, e) ->
        let u' = r.(u) in
        assert(0 <= u' && u' < lenr);
        (u', e)
      ) v.edges
    in
    {v with index; edges}

  (* [internal_graph_edges_rename g r = g']
     apply a renaming vector [r] to all edges destination.
     if [r] is monotonous then [g'] is normalized
     otherwise, one can use [Normalize.graph g' = g'']
     to compute a normalized graph
   *)
  let internal_graph_edges_rename
      (g:('v, 'e)graph)
      (r:int array) : ('v, 'e) graph =
    let len = Array.length g in
    Array.map (internal_vertex_edges_rename len r) g

  let unop_opgraph (opg:('v, 'e)opgraph) : ('v, 'e) graph =
    (* we compute an intermediary graph, and an injective
       renaming function, where [None] vertex have been
       removed *)
    let g1, rename = MyArray.unop_rename opg in
    internal_graph_edges_rename g1 rename

  (* [graph_rma_vertex rma v = w]
      where :
        - rma : int array
           + rma.(i) < 0 => the [i]-th vertex has been removed
           + rma.(i) = j >= 0 => the new newman of [i] is [j]
           deleted, thus
           + if rma is increasing (on positives values) then
             the resulting vertex is [Check.vertex]
             otherwise, use [Normalize.vertex]
        - v : ('v, 'e)vertex
        - w : ('v, 'e)vertex
           w.index = rma.(v.index),
           if w.index < 0 then [w] is not a valid vertex
   *)
  let vertex_rma_vertices (rma:int array) (v:('v, 'e)vertex) : ('v, 'e)vertex =
    {
      index = rma.(v.index);
      tag = v.tag;
      edges = List.filter_map (fun (u, e) ->
        let u' = rma.(u) in
        if u' < 0 then None else Some(u', e)
      ) v.edges
    }

  let graph_rma_vertices (rma:int array) (g:('v, 'e)graph) : ('v, 'e)graph =
    let len = Array.length g in
    assert(len = Array.length rma);
    let lenr = MyArray.count (fun x -> x >= 0) rma in
    let opgr = Array.make lenr None in
    let idxr = ref 0 in
    for i = 0 to len - 1
    do
      if rma.(i) >= 0 then (
        opgr.(!idxr) <- Some(vertex_rma_vertices rma g.(i));
        incr idxr;
      )
    done;
    MyArray.map_unop opgr

  let rma_of_rml (len:int) (rml:int list) : int array =
    let rma = Array.make len 0 in
    List.iter (fun i -> assert(0 <= i && i < len); rma.(i) <- -1) rml;
    let acc = ref 0 in
    for i = 0 to len - 1
    do
      if rma.(i) >= 0 then (
        rma.(i) <- !acc;
        incr acc;
      )
    done;
    (* [DEBUG] *)
    assert(!acc = MyArray.count (fun x -> x >= 0) rma);
    rma

  let graph_rml_vertices (rml:int list) (g:('v, 'e)graph) : ('v, 'e)graph =
    let len = Array.length g in
    let rma = rma_of_rml len rml in
    graph_rma_vertices rma g

  let graph_rm_vertex (rm:int) (g:('v, 'e)graph) : ('v, 'e)graph =
    graph_rml_vertices [rm] g

  (* [internal_vertex_edges_oprename g r = g']
     apply a renaming vector [r] to all edges destination.
     if [r] is monotonous then [g'] is normalized
     otherwise, one can use [Normalize.graph g' = g'']
     to compute a normalized graph
   *)
  let internal_vertex_edges_oprename
      (lenr:int)
      (r:int option array)
      (v:('v, 'e)vertex) : ('v, 'e) vertex =
    let edges = MyList.opmap
      (fun (u, e) ->
        match r.(u) with
        | Some u' -> (
          assert(0 <= u' && u' < lenr);
          Some (u', e)
        )
        | None -> None
      ) v.edges
    in
    {v with edges}

  (* [internal_graph_edges_oprename g r = g']
     apply a renaming vector [r] to all edges destination.
     if [r] is monotonous then [g'] is normalized
     otherwise, one can use [Normalize.graph g' = g'']
     to compute a normalized graph
   *)
  let internal_graph_edges_oprename
      (g:('v, 'e)graph)
      (r:int option array) : ('v, 'e) graph =
    let len' = MyArray.count_Some r in
    Array.map2 (fun v r_v ->
      if r_v = None then None else
      Some(internal_vertex_edges_oprename len' r v)) g r
    |> MyArray.unop
    |> Array.mapi (fun index v -> {v with index})

  (* sub.(i) = true iff i-th vertex is in the sub-graph *)
  let subgraph (sub:bool array) (g:('v, 'e)graph) : ('v, 'e) graph * int option array * int array =
    let rename, rename' = MyArray.true_rename sub in
    let graph = internal_graph_edges_oprename g rename in
    (graph, rename, rename')

  let subgraph_array (suba:bool array array) (g:('v, 'e)graph) : ('v, 'e)graph array * (int * int) list array * (int array array) =
    let k = Array.length suba in
    let result = Array.map (fun sub -> subgraph sub g) suba in
    assert(Array.length result = k);
    let ga = Array.map (fun (g, r, r') -> g) result in
    let opra = Array.map (fun (g, r, r') -> r) result in
    let r'a = Array.map (fun (g, r, r') -> r') result in
    let n = Array.length g in
    let ral = MyArray.flatten_option_matrix n opra in
    (ga, ral, r'a)

  let vertex_union_edges
     ?(replace=false)
     ?(reflexive_false=false)
      (v:('v, 'e)vertex)
      (el:(int*'e)list) : ('v, 'e)vertex =
    let el = if reflexive_false
      then List.filter (fun (i, _) -> i<>v.index) el
      else el
    in
    {v with edges = SetList.union_fst ~replace v.edges el}

  let graph_union_edges
     ?(replace=false)
     ?(inplace=false)
     ?(reflexive_false=false)
      (g:('v, 'e)graph)
      (vl:int list)
      (el:(int*'e)list) : ('v, 'e)graph =
    let g' = if inplace then g else Array.copy g in
    List.iter (fun iv ->
      g'.(iv) <- vertex_union_edges ~replace ~reflexive_false g'.(iv) el
    ) vl;
    g'

  let graph_add_clique
     ?(replace=false)
     ?(inplace=false)
     ?(reflexive_false=false)
      (g:('v, 'e)graph)
      (el:(int*'e)list) : ('v, 'e)graph =
    let g' = if inplace then g else Array.copy g in
    List.iter (fun (iv, _) ->
      g'.(iv) <- vertex_union_edges ~replace ~reflexive_false g'.(iv) el
    ) el;
    g'

  (*  [graph_is_undirected g = b] with:
        - [b] = [true] iff [g] is equal to its transposed
   *)
  let graph_is_undirected (g:('v, 'e)graph) : bool =
    let tg = transpose ~reverse:true g in
    g = tg

  let vertex_is_reflexive ?(refl=true) (v:('v, 'e)vertex) : bool =
    match List.assoc_opt v.index v.edges with
    | None   -> refl = false
    | Some _ -> refl = true

  let graph_is_reflexive ?(refl=true) (g:('v, 'e)graph) : bool =
    Array.for_all (vertex_is_reflexive ~refl) g

  let vertex_rm_reflexive (v:('v, 'e)vertex) : ('v, 'e)vertex =
    {v with edges = List.filter
      (fun (i, _) -> i <> v.index)
      v.edges}

  let graph_rm_reflexive (g:('v, 'e)graph) : ('v, 'e)graph =
    Array.map vertex_rm_reflexive g

  (*
     Time Complexity : O(K x (K + D)) (where D is the max-degree of the graph)
   *)
  let graph_is_clique (g:('v, 'e)graph) (il:int list) : bool =
    assert(SetList.sorted il);
    List.for_all
      (fun i -> SetList.subset_of il (SetList.union [i] (g.(i).edges ||> fst)))
      (* [IMRPOVE] reduced time constant *)
      il

end

module Component =
struct
  type t = {
    vertex_which : int array;
    which_vertex : int list array;
    (* [int list] sorted list of vertices
        defines a partition of the graph's vertices *)
  }

  type ('v, 'e) qt = {
    component : t;
    qgraph    : ('v list, 'e list) graph;
  }

  module ToS =
  struct
    open STools.ToS

    let t t : string =
      "{vertex_which="^(array int t.vertex_which)^
      "; which_vertex="^(array(list int) t.which_vertex)^"}"
  end

  let debug = ref false

  let check_vertex_root (v:int array) : bool =
    let len = Array.length v in
    let rec loop i : bool =
      if i < len
      then (
        let j = v.(i) in
        (0 <= j && j < len) &&
        ((i = j) || (j < i && v.(j) = j)) &&
        (loop (succ i))
      )
      else true
    in loop 0

  (*  [LATER]
      [normalize vertex_tag = vertex_root] where
        - vertex_tag : int array
        - vertex_root : int array
      where
        - [vertex_root] is equivalent to [vertex_tag] in
          terms of partition
        - [check_vertex_root vertex_root = true]
   *)

  let check_vertex_which (vertex_which:int array) : bool =
    let len = Array.length vertex_which in
    let rec loop i maxi =
      if i < len
      then (
        let v = vertex_which.(i) in
        if v > maxi
        then false
        else (
          let maxi' =
            if v = maxi
            then (succ maxi)
            else maxi
          in
          loop (succ i) maxi'
        )
      )
      else true
    in
    loop 0 0

  let compute (vertex_root:int array) : t =
    assert(check_vertex_root vertex_root);
    let len = Array.length vertex_root in
    (* compute [vertex_which] *)
    let vertex_which = Array.make len (-1) in
    let count = ref 0 in
    for index = 0 to len - 1
    do
      let root = vertex_root.(index) in
      assert(0 <= root && root <= index);
      let root' =
        if root = index
        then (!++ count) (* [equiv C] count++ *)
        else vertex_which.(root)
      in
      vertex_which.(index) <- root';
    done;
    assert(check_vertex_which vertex_which);
    (* reverse [vertex_which] into [which_vertex] *)
    let which_vertex = Array.make !count []  in
    for index = len - 1 downto 0
    do
      let which = vertex_which.(index) in
      which_vertex.(which) <- (index::(which_vertex.(which)));
    done;
    {vertex_which; which_vertex}

  (* [internal_quotient g p = g'] where:
      - [g] is a graph
      - [p] is a component-partition of [g]
      - [g'] is the graph [g] where:
        - vertices in the same partition have been merged
        - edges have redirected (and potentially merged)
   *)
  let internal_quotient
      (g:('v, 'e)graph)
      (lenr:int)
      (rename:int array) : ('v list, 'e list)graph =
    let len = Array.length g in
    assert(len = Array.length rename);
    assert(lenr <= len);
    let qg1 = Array.make lenr [] in
    for iu = len-1 downto 0
    do
      let u = g.(iu) in
      let ju = rename.(iu) in
      let u' =
        Utils.internal_vertex_edges_rename lenr rename u
      in
      qg1.(ju) <- (u'::(qg1.(ju)));
    done;
    Array.mapi (fun index vl ->
      let tag = List.map (fun v -> v.tag) vl in
      let edges =
        vl
        |> List.map (fun v -> v.edges)
        |> MyList.flatten
      in
      {index; tag; edges}
      |> Normalize.vertex
      |> Normalize.vertex_collapse
    ) qg1

  let quotient
      (g:('v, 'e)graph)
      (vertex_root:int array) : ('v, 'e) qt =
    let component = compute vertex_root in
    let lenr = Array.length component.which_vertex in
    let rename = component.vertex_which in
    let qgraph = internal_quotient g lenr rename in
    {component; qgraph}

end

(* Depth First Search *)
module DFS =
struct
  type t = {
    vertex_post : int array;
    (* [vertex_post] int : post value *)
    vertex_ante : int array;
    (* [vertex_ante] int : ante value *)
    vertex_root : int array;
    (* [vertex_root] int : vertex index *)
    sorted_post : int array;
    (* [sorted_post] int : vertex index *)
    sorted_ante : int array;
    (* [sorted_ante] int : vertex index *)
  }

  type v = {
    post : int;
    ante : int;
    root : int;
  }

  let check (len:int) (t:t) : bool =
    (* Check Bounds Conformity *)
    (MyArray.between 0 (pred len) t.vertex_post) &&
    (MyArray.between 0 (pred len) t.vertex_ante) &&
    (MyArray.between 0 (pred len) t.vertex_root) &&
    (MyArray.between 0 (pred len) t.sorted_post) &&
    (MyArray.between 0 (pred len) t.sorted_ante) &&
    (* Check Unique Conformity *)
    (MyArray.is_permut t.vertex_post) &&
    (MyArray.is_permut t.vertex_ante) &&
    (MyArray.is_permut t.sorted_post) &&
    (MyArray.is_permut t.sorted_ante)

  let debug = ref false

  let compute
     ?(vertex_order=(None:(int array option)))
      (g:('v, 'e)graph) : t =
    let len = Array.length g in
    let vertex_post = Array.make len (-1) in
    let vertex_ante = Array.make len (-1) in
    let vertex_root = Array.make len (-1) in
    let sorted_post = Array.make len (-1) in
    let post_value  = ref 0 in
    let sorted_ante = Array.make len (-1) in
    let ante_value = ref 0 in
    let root_value = ref 0 in
    let rec explore index =
      if vertex_root.(index) >= 0 then ()
      else (
        (if !debug
        then (
          assert(vertex_post.(index) < 0);
          assert(vertex_ante.(index) < 0);
          assert(vertex_root.(index) < 0);
        ));
        (* we set [index]'s root to !root_value *)
        vertex_root.(index) <- !root_value;
        (* we update {ante} value *)
        vertex_ante.(index) <- !ante_value;
        sorted_ante.(!ante_value) <- index;
        incr ante_value;
        (* we go through [index]'s sons *)
        List.iter (fun (j, _) -> explore j) g.(index).edges;
        (* we update {post} value *)
        vertex_post.(index) <- !post_value;
        sorted_ante.(!post_value) <- index;
        incr post_value;
      )
    in
    (* we go through each vertex in increasing order *)
    (match vertex_order with
    | None -> (
      Array.iteri
        (fun i v ->
          root_value := i;
          explore i
        )
        g;
    )
    | Some order -> (
      Array.iter
        (fun v ->
          root_value := v;
          explore v
        )
        order;
    ));
    assert(!post_value = len);
    assert(!ante_value = len);
    let t : t = {
      vertex_post;
      vertex_ante;
      vertex_root;
      sorted_post;
      sorted_ante;
    } in
    (if !debug then assert(check len t));
    t
end

(* Compute SCC components of a graph using Kosaraju's Algorithm *)
module SccKosaraju =
struct
  type t = Component.t

  let compute (g:('v, 'e)graph) : t =
    let dfs1 = DFS.compute g in
    let order = MyArray.rev dfs1.DFS.vertex_post in
    let tg = Utils.transpose g in
    let dfs2 = DFS.compute ~vertex_order:(Some order) tg in
    let vertex_root = dfs2.DFS.vertex_root in
    Component.compute vertex_root
end

(* returns [true] iff the input if true-twin-free ( [false] otherwise )*)
module TrueTwinsFree =
struct

  (* Naive Version : O(|V|^2 x D) where:
      - D is the graph's maximum degree
      Requires ['mt] to be canonical
   *)
  let compute_naive
      (g:('v, 'e)graph)
      (vtag:('v, 'e)vertex -> 'mt) : bool =
    let len = Array.length g in
    let vtag : _ array = Array.map vtag g in
    (* we add the node itself *)
    let elist : int list array = Array.map
      (fun v ->
        let elist = v.edges ||> fst in
        assert(SetList.sorted_nat elist);
        SetList.union [v.index] elist
        |> Tools.check (SetList.sorted_nat)
      )
      g
    in
    (* component root for each tree-twin component *)
    let rec loop_j i it ie j : bool =
      if j < len
      then (
        let jt = vtag.(j)
        and je = elist.(j) in
        if it = jt && ie = je
        then (
          OUnit.print_error (fun () ->
            let open OUnit.ToS in
            ("[TrueTwinsFree.compute_naive] i:"^(int i)^" j:"^(int j)^"\n")
          );
          false
        )
        else loop_j i it ie (succ j)
      )
      else true
    in
    let rec loop_i i : bool =
      if i < len
      then (
        let it = vtag.(i)
        and ie = elist.(i) in
        loop_j i it ie (succ i) &&
        loop_i (succ i)
      )
      else true
    in
    loop_i 0

(*
  (* Naive Version : O(|V| x D^2) where:
      - D is the graph's maximum degree
      Requires ['mt] to be canonical
   *)
  let compute_naiveD
      (g:('v, 'e)graph)
      (vtag:('v, 'e)vertex -> 'mt) : ('v, 'e) t =
    let len = Array.length g in
    let vtag = Array.map vtag g in
    let elist = Array.map
      (fun v -> SetList.union [v.index] (v.edges ||> fst))
      g
    in
    (* component root for each tree-twin component *)
    let vertex_root = Array.make len (-1) in
    for iu = 0 to len - 1
    do
      (* [iu] has not been visited yet *)
      if vertex_root.(iu) <= 0
      then (
        (* then it is necessarily the root of
           its own true-twin component *)
        vertex_root.(iu) <- iu;
        let tu = vtag.(iu) in
        let eu = elist.(iu) in
        List.iter (fun iv ->
          if iv > iu &&
              (* [iv] has not been visited yet *)
             vertex_root.(iv) <= 0 &&
              (* [iv] has not been visited yet *)
             vtag.(iv) = tu &&
              (* [iv] has the same merge-tag as [iu] *)
             eu = elist.(iv)
              (* [iu]'s neighbors are [iv]'s neighbors *)
          then vertex_root.(iv) <- iu
        ) eu
      )
    done;
    assert(Array.for_all (fun v -> v >= 0) vertex_root);
    assert(Component.check_vertex_root vertex_root);
    Component.quotient g vertex_root

  (* Naive Pseudo-Quadratic Version : O(|V|^2 x log(|V|) )
      Uses lexsort to accelerate the search of equivalence classes
      Requires : ['mt] to be comparable
   *)
  let compute_naive_pseudoquad
      (g:('v, 'e)graph)
      (vtag:('v, 'e)vertex -> 'mt) : ('v, 'e) t =
    let len = Array.length g in
    let vertex_pack v =
      (* neighbors : N_g(v) *)
      let ngv = SetList.union [v.index] (v.edges ||> fst) in
      let tv = vtag v in
      (* [v]'s degree (+1) *)
      let dv = List.length ngv in
      ((tv, dv, ngv), v.index)
    in
    (* True Twin Components List *)
    let ttc_list =
      g |> Array.to_list
        ||> vertex_pack
        |> MyList.stable_sort_fst Stdlib.compare
        |> MyList.collapse_first
        ||> snd
        ||> Tools.check SetList.sorted_nat
    in
    let vertex_root = Array.make len (-1) in
    let update_vertex_root (bl:int list) : unit =
      assert(SetList.sorted_nat bl);
      let b0 = List.hd bl in
      List.iter (fun b -> vertex_root.(b) <- b0) bl
    in
    List.iter update_vertex_root ttc_list;
    assert(Array.for_all (fun v -> v >= 0) vertex_root);
    assert(Component.check_vertex_root vertex_root);
    Component.quotient g vertex_root

  (* Pseudo-Linear Version : O(|V| + |E| x log(|V|) )
      Uses lexsort to accelerate the search of equivalence classes
      Requires : ['mt] to be comparable
   *)
  let compute_pseudolinear
      (g:('v, 'e)graph)
      (vtag:('v, 'e)vertex -> 'mt) : ('v, 'e) t =
    let len = Array.length g in
    let vertex_pack v =
      (* neighbors : N_g(v) *)
      let ngv = SetList.union [v.index] (v.edges ||> fst) in
      let tv = vtag v in
      (* [v]'s degree (+1) *)
      let dv = List.length ngv in
      (tv, (dv, (ngv, v.index)))
    in
    let vertex_root = Array.make len (-1) in
    let update_vertex_root (bl:int list) : unit =
      assert(SetList.sorted_nat bl);
      assert(not(MyList.is_nil bl));
      let b0 = List.hd bl in
      List.iter (fun b -> vertex_root.(b) <- b0) bl
    in
    let sort_ngv (l:(int list * int)list) : unit =
      (* print_newline(); print_string STools.ToS.(list((list int)*int) l); print_newline(); print_newline(); *)
      l |> MyList.lexsort Stdlib.compare
        (* |> Tools.check (fun bll -> print_string STools.ToS.(list(list int)bll); print_newline(); true) *)
        |> List.iter update_vertex_root
    in
    let sort_dv (l:(int*(int list * int))list) : unit =
      l |> MyList.lexsort_fst Stdlib.compare
        |> List.iter sort_ngv
    in
    let sort_tv (l:('mt*(int*(int list * int)))list) : unit =
      l |> MyList.lexsort_fst Stdlib.compare
        |> List.iter sort_dv
    in
    g |> Array.to_list ||> vertex_pack |> sort_tv;
    assert(Array.for_all (fun v -> v >= 0) vertex_root);
    assert(Component.check_vertex_root vertex_root);
    Component.quotient g vertex_root

  (* Linear Version : O(|V|+|E|) *)
  (* [TODO] *)

  (* Requires : ['mt] to be canonical *)
  let compute_eq (g:('v,'e)graph)
      (vtag:('v,'e)vertex -> 'mt) : ('v, 'e) t =
    compute_naiveD g vtag

  (* Requires : ['mt] to be comparable *)
  let compute_cmp (g:('v,'e)graph)
      (vtag:('v,'e)vertex -> 'mt) : ('v, 'e) t =
    compute_pseudolinear g vtag
*)
end

module TrueTwins =
struct

  type ('v, 'e) t = ('v, 'e) Component.qt

  (* Naive Version : O(|V|^2 x D) where:
      - D is the graph's maximum degree
      Requires ['mt] to be canonical
   *)
  let compute_naive
      (g:('v, 'e)graph)
      (vtag:('v, 'e)vertex -> 'mt) : ('v, 'e) t =
    let len = Array.length g in
    let vtag : _ array = Array.map vtag g in
    (* we add the node itself *)
    let elist : int list array = Array.map
      (fun v -> SetList.union [v.index] (v.edges ||> fst))
      g
    in
    (* component root for each tree-twin component *)
    let vertex_root = Array.make len (-1) in
    for iu = 0 to len - 1
    do
      (* [iu] has not been visited yet *)
      if vertex_root.(iu) <= 0
      then (
        (* then it is necessarily the root of
           its own true-twin component *)
        vertex_root.(iu) <- iu;
        let tu = vtag.(iu) in
        let eu = elist.(iu) in
        (* for every vertex with higher index
           (lower one have alreay been visited
            and discarded as potential candidates)
         *)
        for iv = iu+1 to len -1
        do
          if vertex_root.(iv) <= 0 &&
              (* [iv] has not been visited yet *)
             vtag.(iv) = tu &&
              (* [iv] has the same merge-tag as [iu] *)
             eu = elist.(iv)
              (* [iu]'s neighbors are [iv]'s neighbors *)
          then vertex_root.(iv) <- iu
        done
      )
    done;
    assert(Array.for_all (fun v -> v >= 0) vertex_root);
    assert(Component.check_vertex_root vertex_root);
    Component.quotient g vertex_root

  (* Naive Version : O(|V| x D^2) where:
      - D is the graph's maximum degree
      Requires ['mt] to be canonical
   *)
  let compute_naiveD
      (g:('v, 'e)graph)
      (vtag:('v, 'e)vertex -> 'mt) : ('v, 'e) t =
    let len = Array.length g in
    let vtag = Array.map vtag g in
    let elist = Array.map
      (fun v -> SetList.union [v.index] (v.edges ||> fst))
      g
    in
    (* component root for each tree-twin component *)
    let vertex_root = Array.make len (-1) in
    for iu = 0 to len - 1
    do
      (* [iu] has not been visited yet *)
      if vertex_root.(iu) <= 0
      then (
        (* then it is necessarily the root of
           its own true-twin component *)
        vertex_root.(iu) <- iu;
        let tu = vtag.(iu) in
        let eu = elist.(iu) in
        List.iter (fun iv ->
          if iv > iu &&
              (* [iv] has not been visited yet *)
             vertex_root.(iv) <= 0 &&
              (* [iv] has not been visited yet *)
             vtag.(iv) = tu &&
              (* [iv] has the same merge-tag as [iu] *)
             eu = elist.(iv)
              (* [iu]'s neighbors are [iv]'s neighbors *)
          then vertex_root.(iv) <- iu
        ) eu
      )
    done;
    assert(Array.for_all (fun v -> v >= 0) vertex_root);
    assert(Component.check_vertex_root vertex_root);
    Component.quotient g vertex_root

  (* Naive Pseudo-Quadratic Version : O(|V|^2 x log(|V|) )
      Uses lexsort to accelerate the search of equivalence classes
      Requires : ['mt] to be comparable
   *)
  let compute_naive_pseudoquad
      (g:('v, 'e)graph)
      (vtag:('v, 'e)vertex -> 'mt) : ('v, 'e) t =
    let len = Array.length g in
    let vertex_pack v =
      (* neighbors : N_g(v) *)
      let edges = v.edges ||> fst in
      assert(SetList.sorted edges);
      let ngv = SetList.union [v.index] edges in
      let tv = vtag v in
      (* [v]'s degree (+1) *)
      let dv = List.length ngv in
      ((tv, dv, ngv), v.index)
    in
    (* True Twin Components List *)
    let ttc_list =
      g |> Array.to_list
        ||> vertex_pack
        |> MyList.stable_sort_fst Stdlib.compare
        |> MyList.collapse_first
        ||> snd
        ||> Tools.check SetList.sorted_nat
    in
    let vertex_root = Array.make len (-1) in
    let update_vertex_root (bl:int list) : unit =
      assert(SetList.sorted_nat bl);
      let b0 = List.hd bl in
      List.iter (fun b -> vertex_root.(b) <- b0) bl
    in
    List.iter update_vertex_root ttc_list;
    assert(Array.for_all (fun v -> v >= 0) vertex_root);
    assert(Component.check_vertex_root vertex_root);
    Component.quotient g vertex_root

  (* Pseudo-Linear Version : O(|V| + |E| x log(|V|) )
      Uses lexsort to accelerate the search of equivalence classes
      Requires : ['mt] to be comparable
   *)
  let compute_pseudolinear
      (g:('v, 'e)graph)
      (vtag:('v, 'e)vertex -> 'mt) : ('v, 'e) t =
    let len = Array.length g in
    let vertex_pack v =
      (* neighbors : N_g(v) *)
      let edges = v.edges ||> fst in
      assert(SetList.sorted edges);
      let ngv = SetList.union [v.index] edges in
      let tv = vtag v in
      (* [v]'s degree (+1) *)
      let dv = List.length ngv in
      (tv, (dv, (ngv, v.index)))
    in
    let vertex_root = Array.make len (-1) in
    let update_vertex_root (bl:int list) : unit =
      assert(SetList.sorted_nat bl);
      assert(not(MyList.is_nil bl));
      let b0 = List.hd bl in
      List.iter (fun b -> vertex_root.(b) <- b0) bl
    in
    let sort_ngv (l:(int list * int)list) : unit =
      l |> MyList.lexsort Stdlib.compare
        |> List.iter update_vertex_root
    in
    let sort_dv (l:(int*(int list * int))list) : unit =
      l |> MyList.lexsort_fst Stdlib.compare
        |> List.iter sort_ngv
    in
    let sort_tv (l:('mt*(int*(int list * int)))list) : unit =
      l |> MyList.lexsort_fst Stdlib.compare
        |> List.iter sort_dv
    in
    g |> Array.to_list ||> vertex_pack |> sort_tv;
    assert(Array.for_all (fun v -> v >= 0) vertex_root);
    assert(Component.check_vertex_root vertex_root);
    Component.quotient g vertex_root

  (* Linear Version : O(|V|+|E|) *)
  (* [TODO] *)

  (* Requires : ['mt] to be canonical *)
  let compute_eq (g:('v,'e)graph)
      (vtag:('v,'e)vertex -> 'mt) : ('v, 'e) t =
    compute_naiveD g vtag

  (* Requires : ['mt] to be comparable *)
  let compute_cmp (g:('v,'e)graph)
      (vtag:('v,'e)vertex -> 'mt) : ('v, 'e) t =
    compute_pseudolinear g vtag
end

module Connected =
struct
  let internal_compute (alive:bool array) (g:('v, 'e)graph) : bool array array =
    let len = Array.length alive in
    assert(Array.length g = len);
    let global_visited = Array.make len false in
    let visited = Array.make len false in
    let rec explore index : unit =
      if visited.(index) then () else (
        global_visited.(index) <- true;
        visited.(index) <- true;
        if alive.(index)
        then (List.iter (fun (j, _) -> explore j) g.(index).edges)
        else ()
      )
    in
    let stack = ref [] in
    for i = 0 to len-1
    do
      if global_visited.(i) then () else (
        explore i;
        stack_push stack (Array.copy visited);
        Array.fill visited 0 len false;
      )
    done;
    Array.of_list(List.rev !stack)

  (* extended version :
      alive = true  -> regular vertex
      alive = false -> does not count for reachability
      [compute alive g = (subgraphs, assoc, reverse)]
      => subgraphs : array of connected sub-graphs
      => assoc : (int * int) list array
        (i, j) \in assoc[k] : the i-th vertex of the input graph is related the j-th vertex of the k-th sub-graph.
      => reverse : int array array
        reverse.(i).(j) = k : the j-th vertex of the i-th subgraph is related to the k-th vertex of the initial graph
      => assumes that g is undirected
        => at minima each connected component is strongly connected
   *)
  let compute (alive:('v, 'e)vertex -> bool) (g:('v, 'e)graph) : (('v, 'e) graph array) * ((int * int) list array) * (int array array) =
    let alive = Array.map alive g in
    let subarray = internal_compute alive g in
    Utils.subgraph_array subarray g

  let compute_cc (g:('v, 'e)graph) : (('v, 'e) graph array) * ((int * int) list array) * (int array array) =
    compute (fun _ -> true) g
end
