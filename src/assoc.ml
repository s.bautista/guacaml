(* Work In Progress *)
(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Assoc : Sorted association list
 *)

open Extra

(* [update_with ~noconflict:false l1 l2 = l3] where :
 * - l1, l2, l3 are sorted association list
 * - l3 is l1 updated with the value of l2
 * - if noconflict = true raises an exception if l1 and l2 have keys in common
 *)
let rec update_with ?(noconflict=false) ?(carry=[]) (l1:('a * 'b) list) (l2:('a * 'b) list) : ('a * 'b) list =
  match l1, l2 with
  | [], l | l, [] -> List.rev_append carry l
  | (((k1, _) as e1)::l1'), (((k2, _) as e2)::l2') -> (
    if k1 = k2
    then if noconflict && (snd e1 <> snd e2)
      then failwith "[GuaCaml.Assoc.update_with ~noconflict:true] conflict detected"
      else update_with ~noconflict ~carry:(e2::carry) l1' l2'
    else if k1 < k2
      then update_with ~noconflict ~carry:(e1::carry) l1' l2
      else update_with ~noconflict ~carry:(e2::carry) l1  l2'
  )

(* [restr l1 r = l1'] where :
 * - l1, l1' are sorted association list
 * - r is a sorted list
 * - l1' is l1 with its domain restricted to r
 *)
let rec restr_rec carry (l1:('a * 'b)list) (r:'a list) : ('a * 'b)list =
  match l1, r with
  | [], _ | _, [] -> List.rev carry
  | (((k1, _) as e1)::l1'), kr::r' -> (
         if k1 = kr
    then restr_rec (e1::carry) l1' r'
    else if k1 < kr
    then restr_rec      carry  l1' r
    else restr_rec      carry  l1  r'
  )

let restr (l1:('a * 'b)list) (r:'a list) : ('a * 'b)list =
  restr_rec [] l1 r

let rec sorted_rec (k0:'a) : ('a* 'b)list -> bool =
  function
  | [] -> true
  | (k1, _)::l -> (k0 < k1) && sorted_rec k1 l

let sorted : ('a * 'b) list -> bool =
  function
  | [] -> true
  | (k0, _)::l -> sorted_rec k0 l
