(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * AB : A Generic Sum Type with two constructors [A of 'a] and [B of 'b]
 *)

open STools

type ('a, 'b) ab = A of 'a | B of 'b

type ('a, 'b) abl = ('a, 'b) ab list

let isA = function A _ -> true | B _ -> false
let isB = function B _ -> true | A _ -> false
let isAB b = function B _ -> b | A _ -> not b
let swapAB = function A x -> B x | B x -> A x
let mergeAB = function A x -> x | B x -> x
let listAB_allA l = List.for_all isA l
let listAB_allB l = List.for_all isB l

let listAB_countA l = MyList.count isA l
let listAB_countB l = MyList.count isB l

let filterA (l:('a, 'b) abl) : 'a list =
  let rec filterA_rec carry = function
    | [] -> List.rev carry
    | (A a)::t -> filterA_rec (a::carry) t
    | (B _)::t -> filterA_rec     carry  t
  in filterA_rec [] l

let filterB (l:('a, 'b) abl) : 'a list =
  let rec filterB_rec carry = function
    | [] -> List.rev carry
    | (B b)::t -> filterB_rec (b::carry) t
    | (A _)::t -> filterB_rec     carry  t
  in filterB_rec [] l

let filterA_array (a:('a, 'b) ab array) : 'a list =
  let rec filterAa_rec carry array i =
    if i < 0 then carry else (
      match array.(i) with
      | A a -> filterAa_rec (a::carry) array (pred i)
      | B _ -> filterAa_rec     carry  array (pred i)
    )
  in filterAa_rec [] a (Array.length a -1)

let filterB_array (a:('a, 'b) ab array) : 'a list =
  let rec filterBa_rec carry array i =
    if i < 0 then carry else (
      match array.(i) with
      | B b -> filterBa_rec (b::carry) array (pred i)
      | A _ -> filterBa_rec     carry  array (pred i)
    )
  in filterBa_rec [] a (Array.length a -1)

module ToS =
struct
  open STools.ToS
  let ab a b = function
    | A aa -> "(A "^(a aa)^")"
    | B bb -> "(B "^(b bb)^")"

  let abl a b = list (ab a b)
end

(* assumes [List.length abl > 0] *)
let split_ablist (abl:('a, 'b) ab list) : (bool * int list * 'a list * 'b list) =
  let rec aux is_a (n:int) (cN:int list) (al:'a list) (bl:'b list) = function
    | [] -> (List.rev (n::cN), List.rev al, List.rev bl)
    | ab::abl -> (
      match ab with
      | A a -> (
        if is_a
        then aux true  (succ n) cN  (a::al) bl abl
        else aux true    1  (n::cN) (a::al) bl abl
      )
      | B b -> (
        if is_a
        then aux false   1  (n::cN) al (b::bl) abl
        else aux false (succ n) cN  al (b::bl) abl
      )
    )
  in
  match abl with
  | [] -> (failwith "[GuaCaml.AB] error") (* invalid argument *)
  | ab::abl -> (
    let is_a, (il, al, bl) = match ab with
      | A a -> (true , aux true  1 [] [a] [] abl)
      | B b -> (false, aux false 1 [] [] [b] abl)
    in
    (is_a, il, al, bl)
  )

let split_ablist_unitA (abl:(unit, 'b) ab list) : (bool * int list * int * 'b list) =
  let rec aux is_a (n:int) (cN:int list) nA (bl:'b list) = function
    | [] -> (List.rev (n::cN), nA, List.rev bl)
    | ab::abl -> (
      match ab with
      | A () -> (
        if is_a
        then aux true  (succ n) cN  (succ nA) bl abl
        else aux true    1  (n::cN) (succ nA) bl abl
      )
      | B b -> (
        if is_a
        then aux false   1  (n::cN) nA (b::bl) abl
        else aux false (succ n) cN  nA (b::bl) abl
      )
    )
  in
  match abl with
  | [] -> (failwith "[GuaCaml.AB] error") (* invalid argument *)
  | ab::abl -> (
    let is_a, (il, nA, bl) = match ab with
      | A a -> (true , aux true  1 [] 1 []  abl)
      | B b -> (false, aux false 1 [] 0 [b] abl)
    in
    (is_a, il, nA, bl)
  )

let split_ablist_unitAB (abl:(unit, unit) ab list) : (bool * int list * int * int) =
  let rec aux is_a (n:int) (cN:int list) nA nB = function
    | [] -> (List.rev (n::cN), nA, nB)
    | ab::abl -> (
      match ab with
      | A () -> (
        if is_a
        then aux true  (succ n) cN  (succ nA) nB abl
        else aux true    1  (n::cN) (succ nA) nB abl
      )
      | B b -> (
        if is_a
        then aux false   1  (n::cN) nA (succ nB) abl
        else aux false (succ n) cN  nA (succ nB) abl
      )
    )
  in
  match abl with
  | [] -> (failwith "[GuaCaml.AB] error") (* invalid argument *)
  | ab::abl -> (
    let is_a, (il, nA, bl) = match ab with
      | A a -> (true , aux true  1 [] 1 0 abl)
      | B b -> (false, aux false 1 [] 0 1 abl)
    in
    (is_a, il, nA, bl)
  )

let combine_ablist ((is_a, il, al, bl):(bool * int list * 'a list * 'b list)) : ('a, 'b) ab list =
  let move_ab is_a n (c, al, bl) =
    if is_a
    then (
      let al', c' = MyList.move_map n (fun x -> A x) al c in
      (c', al', bl )
    )
    else (
      let bl', c' = MyList.move_map n (fun x -> B x) bl c in
      (c', al , bl')
    )
  in
  let rec loop is_a il c_al_bl =
    match il with
    | [] -> (
      let c, al, bl = c_al_bl in
      assert(al = []);
      assert(bl = []);
      List.rev c
    )
    | n::il -> (
      loop (not is_a) il (move_ab is_a n c_al_bl)
    )
  in
  loop is_a il ([], al, bl)

let combine_ablist_unitA ((is_a, il, nA, bl):(bool * int list * int * 'b list)) : (unit, 'b) ab list =
  let move_ab is_a n (c, nA, bl) =
    if is_a
    then (
      assert(n <= nA);
      let nA' = nA - n in
      let c' = MyList.ntimes ~carry:c (A()) n in
      (c', nA', bl )
    )
    else (
      let bl', c' = MyList.move_map n (fun x -> B x) bl c in
      (c', nA , bl')
    )
  in
  let rec loop is_a il c_nA_bl =
    match il with
    | [] -> (
      let c, nA, bl = c_nA_bl in
      assert(nA = 0 );
      assert(bl = []);
      List.rev c
    )
    | n::il -> (
      loop (not is_a) il (move_ab is_a n c_nA_bl)
    )
  in
  loop is_a il ([], nA, bl)

let combine_ablist_unitAB ((is_a, il, nA, nB):(bool * int list * int * int)) : (unit, unit) ab list =
  let move_ab is_a n (c, nA, nB) =
    if is_a
    then (
      assert(n <= nA);
      let nA' = nA - n in
      let c' = MyList.ntimes ~carry:c (A()) n in
      (c', nA', nB )
    )
    else (
      assert(n <= nB);
      let nB' = nB - n in
      let c' = MyList.ntimes ~carry:c (B()) n in
      (c', nA , nB')
    )
  in
  let rec loop is_a il c_nA_nB =
    match il with
    | [] -> (
      let c, nA, nB = c_nA_nB in
      assert(nA = 0);
      assert(nB = 0);
      List.rev c
    )
    | n::il -> (
      loop (not is_a) il (move_ab is_a n c_nA_nB)
    )
  in
  loop is_a il ([], nA, nB)

(* [alt_sum is_a il = (nA, nB)] *)
let alt_sum is_a (il:int list) : int * int =
  let rec loop is_a il nA nB =
    match il with
    | [] -> (nA, nB)
    | i::il -> if is_a
      then loop (not is_a) il (i+nA) nB
      else loop (not is_a) il nA (i+nB)
  in
  loop is_a il 0 0

module ToB =
struct
  open BTools.ToB

  let ab (aa:'a t) (bb:'b t) (xx:('a, 'b) ab) (s:stream) : stream = match xx with
    | A a -> false::(aa a s)
    | B b -> true ::(bb b s)

  let sized_abl (sal:'a list t) (sbl:'b list t) (abl:('a, 'b) ab list) (s:stream) : stream =
    let len = List.length abl in
    if len = 0
    then s
    else (
      let is_a, il, al, bl = split_ablist abl in
      s |> sbl bl |> sal al |> sum_list ~min:1 len il |> bool is_a
    )

  let sized_abl_unitA (sbl:'b list t) (abl:(unit, 'b) ab list) (s:stream) : stream =
    let len = List.length abl in
    if len = 0
    then s
    else (
      let is_a, il, _, bl = split_ablist_unitA abl in
      s |> sbl bl |> sum_list ~min:1 len il |> bool is_a
    )

  let sized_abl_unitAB (abl:(unit, unit) ab list) (s:stream) : stream =
    let len = List.length abl in
    if len = 0
    then s
    else (
      let is_a, il, _, _ = split_ablist_unitAB abl in
      let s = sum_list ~min:1 len il s in
      let s = bool is_a s in
      s
    )

  let abl sal sbl abl s : stream =
    let n = List.length abl in
    let s = sized_abl sal sbl abl s in
    let s = int n s in
    s
end

module OfB =
struct
  open BTools.OfB

  let ab aa bb = function
    | false::s ->
      let a, s = aa s in
      (A a, s)
    | true ::s ->
      let b, s = bb s in
      (B b, s)
    | _ -> (failwith "[GuaCaml.AB] error")

  let sized_abl_countAB (len:int) (sal:int -> 'a list t) (sbl:int -> 'b list t) (s:stream) : (('a, 'b) ab list * (int * int)) * stream =
    if len = 0
    then (([], (0, 0)), s)
    else (
      let is_a, s = bool s in
      let il, s = sum_list ~min:1 len s in
      let nA, nB = alt_sum is_a il in
      assert(nA + nB = len);
      let al, s = sal nA s in
      let bl, s = sbl nB s in
      let abl = combine_ablist (is_a,il, al, bl) in
      ((abl, (nA, nB)), s)
    )

  let sized_abl len sal sbl s : (_, _) ab list * stream =
    let (abl, _), s = sized_abl_countAB len sal sbl s in
    (abl, s)

  let sized_abl_countA len sal sbl s : ((_, _) ab list * int) * stream =
    let (abl, nAB), s = sized_abl_countAB len sal sbl s in
    ((abl, fst nAB), s)
  let sized_abl_countB len sal sbl s : ((_, _) ab list * int) * stream =
    let (abl, nAB), s = sized_abl_countAB len sal sbl s in
    ((abl, snd nAB), s)

  let sized_abl_unitA_countAB (len:int) (sbl:int -> 'b list t) (s:stream) : ((unit, 'b) ab list * (int * int)) * stream =
    if len = 0
    then (([], (0, 0)), s)
    else (
      let is_a, s = bool s in
      let il, s = sum_list ~min:1 len s in
      let nA, nB = alt_sum is_a il in
      assert(nA + nB = len);
      let bl, s = sbl nB s in
      let abl = combine_ablist_unitA (is_a,il, nA, bl) in
      ((abl, (nA, nB)), s)
    )

  let sized_abl_unitA len sbl s : (_, _) ab list * stream =
    let (abl, _), s = sized_abl_unitA_countAB len sbl s in
    (abl, s)

  let sized_abl_unitA_countA len sbl s : ((_, _) ab list * int ) * stream =
    let (abl, nAB), s = sized_abl_unitA_countAB len sbl s in
    ((abl, fst nAB), s)
  let sized_abl_unitA_countB len sbl s : ((_, _) ab list * int ) * stream =
    let (abl, nAB), s = sized_abl_unitA_countAB len sbl s in
    ((abl, snd nAB), s)

  let sized_abl_unitAB_countAB (len:int) (s:stream) : ((unit, unit) ab list * (int * int)) * stream =
    if len = 0
    then (([], (0, 0)), s)
    else (
      let is_a, s = bool s in
      let il, s = sum_list ~min:1 len s in
      let nA, nB = alt_sum is_a il in
      assert(nA + nB = len);
      let abl = combine_ablist_unitAB (is_a,il, nA, nB) in
      ((abl, (nA, nB)), s)
    )

  let sized_abl_unitAB len s : (_, _) ab list * stream =
    let (abl, _), s = sized_abl_unitAB_countAB len s in
    (abl, s)

  let sized_abl_unitAB_countA len s : ((_, _) ab list * int ) * stream =
    let (abl, nAB), s = sized_abl_unitAB_countAB len s in
    ((abl, fst nAB), s)
  let sized_abl_unitAB_countB len s : ((_, _) ab list * int ) * stream =
    let (abl, nAB), s = sized_abl_unitAB_countAB len s in
    ((abl, snd nAB), s)

  let abl (sal:int -> 'a list t) (sbl:int -> 'b list t) (s:stream) : ('a, 'b) ab list * stream =
    let n, s = int s in
    sized_abl n sal sbl s
end

module ToBStream =
struct
  open BTools.ToBStream

  let ab (aa:'a t) (bb:'b t) cha (xx:('a, 'b) ab) : unit =
    match xx with
    | A a -> bool cha false; aa cha a
    | B b -> bool cha true ; bb cha b

  let sized_abl (sal:'a list t) (sbl:'b list t) cha (abl:('a, 'b) ab list) : unit =
    let len = List.length abl in
    if len = 0 then ()
    else (
      let is_a, il, al, bl = split_ablist abl in
      assert(List.length il <= List.length abl);
      assert(List.length al + List.length bl = List.length abl);
      bool cha is_a;
      sum_list ~min:1 len cha il;
      sal cha al;
      sbl cha bl;
    )

  let abl sal sbl cha abl : unit =
    int cha (List.length abl);
    sized_abl sal sbl cha abl;
    ()
end

module OfBStream =
struct
  open BTools.OfBStream

  let ab aa bb cha =
    match bool cha with
    | false -> A(aa cha)
    | true  -> B(bb cha)

  let sized_abl (len:int) (sal:int -> 'a list t) (sbl:int -> 'b list t) cha : ('a, 'b) ab list  =
    if len = 0 then []
    else (
      let is_a = bool cha in
      let il = sum_list ~min:1 len cha in
      let nA, nB = alt_sum is_a il in
      assert(nA + nB = len);
      let al = sal nA cha in
      assert(List.length al = nA);
      let bl = sbl nB cha in
      assert(List.length bl = nB);
      let abl = combine_ablist (is_a,il, al, bl) in
      assert(List.length abl = len);
      abl
    )

  let abl (sal:int -> 'a list t) (sbl:int -> 'b list t) cha : ('a, 'b) ab list  =
    let n = int cha in
    sized_abl n sal sbl cha
end

let extract_A (r:(unit, ('a, 'b) ab) abl) : (unit, 'a) abl * (unit, 'b) abl =
  let rec aux sA sB = function
    | [] -> (List.rev sA, List.rev sB)
    | head::tail -> match head with
      | A unit -> aux (A () :: sA) (A () :: sB) tail
      | B(A a) -> aux (B a  :: sA) (        sB) tail
      | B(B b) -> aux (A () :: sA) (B b  :: sB) tail
  in aux [] [] r

(* compose_A listU listX *)

let compose_A (lC:(unit, 'a) abl) (lc:(unit, 'b) abl) : (unit, ('a, 'b) ab) abl =
  assert(listAB_countA lC = List.length lc);
  let rec aux carry = function
    | ([], []) -> List.rev carry
    | ([], _ ) -> (failwith "[GuaCaml.AB.compose_A] error")
    | (hC::tC, lc) -> match hC with
      | A () ->
      ( match lc with
        | [] -> (failwith "[GuaCaml.AB.compose_A] error")
        | hc::tc -> match hc with
          | A () -> aux (A()::carry) (tC, tc)
          | B bb  -> aux (B(B bb)::carry) (tC, tc)
      )
      | B ba -> aux (B(A ba)::carry) (tC, lc)
  in aux [] (lC, lc)

(* compose_B listX listU *)

let swap_B_AB row =
  List.map (function A x -> A x | B x -> B(swapAB x)) row

let compose_B (lC:(unit, 'a) abl) (lc:(unit, 'b) abl) : (unit, ('b, 'a) ab) abl =
  swap_B_AB (compose_A lC lc)

let merge_B_AB row =
  Tools.map (function A x -> A x | B x -> B(mergeAB x)) row

(* [compose_same lC lc = merge_B_AB (compose_A lC lc) *)
let compose_same (lC:(unit, 'a) abl) (lc:(unit, 'a) abl) : (unit, 'a) abl =
  let rec loop carry lC lc =
    match lC, lc with
    | [], [] -> List.rev carry
    | [], _  -> (failwith "[GuaCaml.AB] error")
    | (A())::lC, [] -> (failwith "[GuaCaml.AB] error")
    | (A())::lC, cc::lc -> loop ( cc  ::carry) lC lc
    | (B b)::lC,     lc -> loop ((B b)::carry) lC lc
  in loop [] lC lc

(* swap_ablA_ablB rA rB = extract_A (compose_B rA rU) *)
let swap_ablA_ablB (rA:(unit, 'a) abl) (rB:(unit, 'b) abl) : (unit, 'b) abl * (unit, 'a) abl =
  let rec swap cB cA rA rB =
    match rA, rB with
    | [], [] -> List.(rev cB, rev cA)
    | [], _  -> (failwith "[GuaCaml.AB] error")
    | (A())::rA, [] -> (failwith "[GuaCaml.AB] error")
    | (A())::rA, (A())::rB ->
      swap ((A())::cB) ((A())::cA) rA rB
    | (A())::rA, (B b)::rB ->
      swap ((B b)::cB)         cA  rA rB
    | (B a)::rA,        rB ->
      swap ((A())::cB) ((B a)::cA) rA rB
  in swap [] [] rA rB

let rec where_upward_A ?(carry=0) (rA:('a, 'b)abl) (i:int) : int * 'a =
  match rA with
  | [] -> (failwith "[GuaCaml.AB] error")
  | hd::tl ->
    match hd with
    | A a -> (
      if i = 0
      then (carry, a)
      else where_upward_A ~carry:(succ carry) tl (pred i)
    )
    | B _ -> where_upward_A ~carry:(succ carry) tl i

let rec where_downward_A ?(carry=0) (rA:('a, 'b)abl) (i:int) : int * 'a =
  match rA with
  | [] -> (failwith "[GuaCaml.AB] error")
  | hd::tl ->
    match hd with
    | A a -> (
      if i = 0
      then (carry, a)
      else where_downward_A ~carry:(succ carry) tl (pred i)
    )
    | B _ -> where_downward_A ~carry tl (pred i)
