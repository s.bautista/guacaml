(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * BTools_ToB [INTERNAL] : type-based conversion facility from basic
 *   types to [stream:=bool list]
 *)

type stream = BinUtils.stream
type 'a t = 'a BinUtils.dump

let unit : unit BinUtils.dump = fun () stream -> stream

let map f dump item stream = dump (f item) stream

let c2 dump0 dump1 elem stream = Poly.(match elem with
  | C2_0 t0 -> false::(dump0 t0 stream)
  | C2_1 t1 -> true ::(dump1 t1 stream))

let c3 dump0 dump1 dump2 elem stream = Poly.(match elem with
  | C3_0 t0 -> false::(dump0 t0 stream)
  | C3_1 t1 -> true ::false::(dump1 t1 stream)
  | C3_2 t2 -> true ::true ::(dump2 t2 stream))

let c4 dump0 dump1 dump2 dump3 elem stream = Poly.(match elem with
  | C4_0 t0 -> false::false::(dump0 t0 stream)
  | C4_1 t1 -> false::true ::(dump1 t1 stream)
  | C4_2 t2 -> true ::false::(dump2 t2 stream)
  | C4_3 t3 -> true ::true ::(dump3 t3 stream))

let option dump opx stream = match opx with
  | None -> false::stream
  | Some x -> true::(dump x stream)

let bool x stream = x :: stream

let sized_bool_list x stream = x @ stream
let sized_unit_list s stream = stream

let sized_barray x stream = sized_bool_list (BTools_BArray.to_bool_list x) stream

let sized_list dump liste stream =
  let rec aux stream = function
    | []      -> stream
    | head::tail  -> aux (dump head stream) tail
  in aux stream liste

let none_list dump liste stream =
  let rec aux stream = function
    | []      -> stream
    | head::tail  -> aux (dump (Some head) stream) tail

  in aux (dump None stream) liste

let unary n stream =
  let rec aux s = function
    | 0 -> true::s
    | n -> (false::(aux s (pred n)))
  in aux stream n

let int_legacy n stream =
  assert(n>=0);
  let rec aux c0 c1 = function
    | 0 -> c0@[true]@c1@stream
    | n -> aux (false::c0) ((n mod 2 = 1)::c1) (n/2)
  in aux [] [] n

(* assume [x] >= 1 *)
let rec int_vanilla_rec n s =
  assert(n>=1);
  if n = 1
  then (false::s)
  else (int_vanilla_rec (n lsr 1) (false::(n land 1 = 1)::s))

let int_vanilla n s =
  assert(n>=0);
  if n = 0 then (true::s)
  else (int_vanilla_rec n (true::s))

(* let int = int_legacy *)
let int = int_vanilla

let unary_le top x s =
  assert(0 <= x && x <= top);
  if x = top
  then (MyList.ntimes ~carry:s false top)
  else (unary x s)

let swrap text tos tob n0 n s =
  print_endline (text^" [0] n0:"^(tos n0));
  print_endline (text^" [0] n :"^(tos n));
  print_string (text^" [0] s :"); STools.SUtils.print_stream s; print_newline();
  let s = tob n0 n s in
  print_string (text^" [1] s :"); STools.SUtils.print_stream s; print_newline();
  s

(* false < true *)
let rec sized_bool_list_le top l s =
  match top, l with
  | [], [] -> s
  | [], _ | _, [] -> (failwith "[GuaCaml.BTools_ToB] error")
  | t0::top, l0::l -> (
    if t0
    then if l0
      then (true ::(sized_bool_list_le top l s))
      else (false::(sized_bool_list l s))
    else (assert(l0 = false); sized_bool_list_le top l s)
  )

let rec exactly_sized_int_rec n x s =
  if n <= 1
  then (assert(x=1); s)
  else (
    let s = exactly_sized_int_rec (pred n) (x lsr 1) s in
    ((x land 1) = 1)::s
  )

let exactly_sized_int n x s =
  if n <= 0
  then (assert(x=0); s)
  else (exactly_sized_int_rec n x s)

let rec sized_int_smart (i:int) (n:int) (s:stream) : stream =
  if i < 0
  then s
  else (
    let n0 = n land (1 lsl i) <> 0 in
    n0 :: (sized_int_smart (pred i) n s)
  )

let rec sized_int_le_smart (i:int) (top:int) (n:int) (s:stream) : stream =
  if i < 0
  then s
  else (
    let t0 = top land (1 lsl i) <> 0 in
    let n0 =  n  land (1 lsl i) <> 0 in
    if t0
    then if n0
      then (n0::(sized_int_le_smart (pred i) top n s))
      else (n0::(sized_int_smart    (pred i)     n s))
    else (
      assert(n0 = false);
      sized_int_le_smart (pred i) top n s
    )
  )

let int_le top x s =
  assert(0 <= x && x <= top);
  match top with
  | 0 -> s
  | 1 -> (x=1)::s
  | _ -> (
    let n0 = Tools.math_log2 top in
    let n = Tools.math_log2 x in
    let s = if n = n0
      then (sized_int_le_smart (n-2) top x s)
      else (exactly_sized_int n x s)
    in
    unary_le n0 n s
  )

let int' (min:int) (max:int) (x:int) (s:stream) : stream =
  assert(min <= x && x <= max);
  int_le (max-min) (x-min) s

let signed_int (x:int) (s:stream) : stream =
  let b = x < 0 in
  let x' = if x < 0 then (1-x) else x in
  int x' (bool b s)

let barray ba stream =
  sized_barray ba (int (BTools_BArray.length ba) stream)

let sized_int size n stream : bool list =
  assert(0 <= n && n <= (1 lsl size));
  let rec aux n stream = function
    | 0 -> assert(n=0); stream
    | i -> aux (n/2) ((n mod 2 = 1)::stream) (i-1)
  in
  (try (aux n stream size)
  with _ -> (
    print_int size;
    print_string " ";
    print_int n;
    print_newline();
    assert(false)
  ))

let pair dumpA dumpB (a, b) stream =
  dumpA a (dumpB b stream)
let ( * ) = pair
let trio dumpA dumpB dumpC (a, b, c) stream =
  dumpA a (dumpB b (dumpC c stream))
let quad dumpA dumpB dumpC dumpD (a, b, c, d) stream =
  dumpA a (dumpB b (dumpC c (dumpD d stream)))

let closure dump objet =
  dump objet [] |> BTools_BArray.of_bool_list

let list dump liste stream = int (List.length liste) (sized_list dump liste stream)

let array dump vect stream = list dump (Array.to_list vect) stream

let bool_option_list = none_list (fun elem stream -> match elem with
  | Some (Some b) -> false::b    ::stream
  | Some  None    -> true ::false::stream
  | None          -> true ::true ::stream)

let o3 = fst

(* assumes [sum = \sum_{i\in il} i] *)
let rec sum_list_rec ?(min=0) (len:int) (sum:int) (il:int list) (s:stream) : stream =
  if len <= 1
  then s
  else (
    let i, il = MyList.hdtl il in
    assert(i>=min);
    let max = sum - (Stdlib.( * ) min (len-1)) in
    int' min max i (sum_list_rec ~min (len-1) (sum-i) il s)
  )

let sum_list ?(min=0) (sum:int) (il:int list) (s:stream) : stream =
  if min > 0
  then (
    if sum = 0
    then (assert(il = []); s)
    else (
      let len = List.length il in
      assert(len > 0);
      let s = sum_list_rec ~min len sum il s in
      let s = int' 1 (sum/min) len s in
      s
    )
  )
  else (
    let len = List.length il in
    let s = sum_list_rec ~min len sum il s in
    let s = int len s in
    s
  )
