(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * DBBC : Divide, Branch, Bound and Conquer !
 *
 * === SHORT ===
 *
 *   DBBC is a generalization of Branch and Bound (BB) and Divide and Conquer (DC).
 *   The algorithm has a memoization mechanism, allowing to merge sub-computations efficiently.
 *   The present file intends at presenting the differents mechanism in an incremental fashion.
 *
 * === TODO ===
 *
 *   1) implement bound propagation
 *   2) implement garbage collector
 *   3) implement state serialization
 *   4) add scalar multiplication to proof primitives
 *)

open STools
open BTools

(* full proof *)
type ('a, 'd) proof =
  | End
  | Seq of 'a list * ('a, 'd) proof
  | Sum of 'd * ('a, 'd) proof list

let rec proof_normalize = function
  | End -> End
  | Seq ([], p) -> proof_normalize p
  | Seq (al, p) ->
  ( match proof_normalize p with
    | Seq (al', p') -> Seq(al@al', p')
    | p -> Seq(al, p)
  )
  | Sum (d, pl) ->
    Sum(d, List.map proof_normalize pl)

(* partial proof (cost annoted) *)
type ('a, 'd, 's, 'c) pproof =
  | PEnd
  | PNxt of 's
  | PSeq of 'a list * 'c * 's * ('a, 'd, 's, 'c) pproof
  | PSum of 'd * 'c * ('s * ('a, 'd, 's, 'c) pproof) list
  | PMin of 's * ('a, 'd, 's, 'c) pproof list

let rec pproof_normalize ( + ) = function
  | PEnd -> PEnd
  | PNxt s -> PNxt s
  | PSeq (al0, c0, s0, pp) ->
  (  match pproof_normalize ( + ) pp with
    | PSeq (al1, c1, s1, pp) ->
      PSeq (al0@al1, c0 + c1, s1, pp)
    | pp' -> PSeq(al0, c0, s0, pp')
  )
  | PSum (d, c, ppl) ->
    let ppl' = List.map
      (fun (s, pp) -> (s, pproof_normalize ( + ) pp))
      ppl
    in
    PSum(d, c, ppl')
  | PMin(s, ppl) ->
    match ppl with
    | [] -> assert false
    | [pp] -> pproof_normalize ( + ) pp
    | _ -> PMin (s, List.map (pproof_normalize ( + )) ppl)

type ('a, 'd) aproof =
  | AEnd
  | ANxt
  | ASeq of 'a list
  | ASum of 'd
  | AMin

let aproof_of_pproof = function
  | PEnd               -> AEnd
  | PNxt _             -> ANxt
  | PSeq (al, _, _, _) -> ASeq al
  | PSum (d , _, _)    -> ASum d
  | PMin (_ , _)       -> AMin

let rec proof_of_pproof :
    ('a, 'd, 's, 'c) pproof -> ('a, 'd) proof =
  function
  | PEnd -> End
  | PNxt s -> (failwith "[GuaCaml.DBBC] error")
  | PSeq (al, _, _, pp) ->
    Seq(al, proof_of_pproof pp)
  | PSum (d, _, ppl) ->
    Sum(d, List.map (fun (_, pp) -> proof_of_pproof pp) ppl)
  | PMin(_, _) -> (failwith "[GuaCaml.DBBC] error")

(* [LATER] implement PMin *)
let rec cost_of_pproof
  (zero : unit -> 'c)
  ((+):'c -> 'c -> 'c)
  (cost_of_pnxt:'s -> 'c) :
    ('a, 'd, 's, 'c) pproof -> 'c =
  function
  | PEnd -> zero()
  | PNxt s -> cost_of_pnxt s
  | PSeq (al, c, _, pp) -> c + (cost_of_pproof zero (+) cost_of_pnxt pp)
  | PSum (d, c, ppl) ->
    List.fold_left (fun c (_, pp) ->
      c + (cost_of_pproof zero (+) cost_of_pnxt pp)) c ppl
  | PMin(_, ppl) -> (failwith "[GuaCaml.DBBC] error")

module ToS =
struct
  include ToS

  let rec proof (sa:'a t) (sd:'d t) : ('a, 'd) proof t = function
    | End -> "End"
    | Seq (al, p) -> ("Seq "^(((list sa) * (proof sa sd)) (al, p)))
    | Sum (d, pl) -> ("Sum "^((sd * (list (proof sa sd))) (d, pl)))

  let rec pproof (sa:'a t) (sd:'d t) (ss:'s t) (sc:'c t) : ('a, 'd, 's, 'c) pproof t = function
    | PEnd -> "PEnd"
    | PNxt s -> "PNxt "^(ss s)
    | PSeq (al, c, s, pp) ->
      "PSeq ("^(list sa al)^", "^(sc c)^", "^(ss s)^", "^(pproof sa sd ss sc pp)^")"
    | PSum (d, c, sppl) ->
      "PSum "^((trio sd sc (list(ss*(pproof sa sd ss sc)))) (d, c, sppl))
    | PMin (s, ppl) ->
      "PMin "^(pair ss (list(pproof sa sd ss sc)) (s, ppl))

  let aproof (sa:'a t) (sd:'d t) : ('a, 'd) aproof t = function
    | AEnd -> "AEnd"
    | ANxt -> "ANxt"
    | ASeq al -> "ASeq "^(list sa al)
    | ASum d  -> "ASum "^(sd d)
    | AMin -> "AMin"
end

module type MSig =
sig
(* A.1] type of elements *)
  type s (* state : the type of intermediate states *)
    (* [FIXME] standard operation on state elements must be consistent
        e.g. Stdlib.compare, Hashtbl.hash *)
  type a (* non branching actions : the type of actions *)
  type d (* branching actions, aka divider actions *)
  type c (* cost : the type of cost *)

(* A.2] type of proofs *)
  type p  = (a, d)        proof
  type pp = (a, d, s, c) pproof
  type ap = (a, d)       aproof

(* B] type of operators *)
  val lower : s -> c
  val upper : s -> p * c
  val next  : s -> pp
  (* do not use empty transition from Min to Min *)
  val preproc : s -> pp
  (* this operation has a semantic similar to 'next' but is called only once at the beginning *)
  (* the default value for 'preproc' is 'let preproc s = PNxt s' *)

  val cost_add : c -> c -> c
    (* [cost_add c1 c2 = c3] st. [c3] = [c1] + [c2] *)
  val cost_sub : c -> c -> c
    (* [cost_sub c1 c2 = c3] st. [c3] = [c1] - [c2] *)
  val cost_cmp : c -> c -> int
    (* comparison operator on [c] *)
  val cost_zero : unit -> c
    (* returns a neutral cost element *)

(* C] action application operators *)
  val do_a : s -> a -> c * s
  val do_d : s -> d -> c * (s list)

(* D] [DEBUG] printing facility *)
  val ss : s -> string (* state *)
  val sa : a -> string (* action *)
  val sd : d -> string (* divider *)
  val sc : c -> string (* cost *)

(* E] [ESTIMATION] estimate the number of combination left to deal with *)
  val estim : s -> BNat.nat
end

module MakeProfile(H:MSig) : MSig
with
  type s = H.s and
  type a = H.a and
  type d = H.d and
  type c = H.c and
  type p = H.p and
  type pp = H.pp and
  type ap = H.ap =
struct
(* A.1] type of elements *)
  type s = H.s
  type a = H.a
  type d = H.d
  type c = H.c

(* A.2] type of proofs *)
  type p  = H.p
  type pp = H.pp
  type ap = H.ap

(* B] type of operators *)
  let lower (s:s) : c     = OProfile.profile OProfile.default "[DBBC.MakeProfile] H.lower" H.lower s
  let upper (s:s) : p * c = OProfile.profile OProfile.default "[DBBC.MakeProfile] H.upper" H.upper s
  let next  (s:s) : pp    = OProfile.profile OProfile.default "[DBBC.MakeProfile] H.next" H.next s
  let preproc (s:s) : pp  = OProfile.profile OProfile.default "[DBBC.MakeProfile] H.preproc" H.preproc s

  let cost_add (x:c) (y:c) : c   = OProfile.profile2 OProfile.default "[DBBC.MakeProfile] H.cost_add" H.cost_add x y
  let cost_sub (x:c) (y:c) : c   = OProfile.profile2 OProfile.default "[DBBC.MakeProfile] H.cost_sub" H.cost_sub x y
  let cost_cmp (x:c) (y:c) : int = OProfile.profile2 OProfile.default "[DBBC.MakeProfile] H.cost_cmp" H.cost_cmp x y
  let cost_zero ()         : c   = OProfile.uprofile OProfile.default "[DBBC.MakeProfile] H.cost_zero" H.cost_zero

(* C] action application operators *)
  let do_a (s:s) (a:a) : c * s = OProfile.profile2 OProfile.default "[DBBC.MakeProfile] H.do_a" H.do_a s a
  let do_d (s:s) (d:d) : c * (s list) = OProfile.profile2 OProfile.default "[DBBC.MakeProfile] H.do_d" H.do_d s d

(* D] [DEBUG] printing facility *)
  let ss (s:s) : string = OProfile.profile OProfile.default "[DBBC.MakeProfile] H.ss" H.ss s
  let sa (a:a) : string = OProfile.profile OProfile.default "[DBBC.MakeProfile] H.ss" H.sa a
  let sd (d:d) : string = OProfile.profile OProfile.default "[DBBC.MakeProfile] H.ss" H.sd d
  let sc (c:c) : string = OProfile.profile OProfile.default "[DBBC.MakeProfile] H.ss" H.sc c

(* E] [ESTIMATION] estimate the number of combination left to deal with *)
  let estim (s:s) : BNat.nat = OProfile.profile OProfile.default "[DBBC.MakeProfile] H.estim" H.estim s
end

module type Sig =
sig
  module H : MSig

  type t

  val newman : unit -> t

  val solve : t -> H.s -> H.c * H.p
  (* val dump_stats : t -> Tree.stree *)
end

module Make(H0:MSig) : Sig
  with module H = H0
  =
struct
  module H = H0

  type ident = int

  type alive =
    | NotVisited
    | Visited
    | Solved

  type tnxt = {
    tnxt_proof : H.p;
  }

  type tseq = {
    tseq_next  : ident;
    tseq_actions : H.a list;
    tseq_actions_cost : H.c;
  }

  module CostCmp : Map.OrderedType
    with type t = H.c =
  struct
    type t = H.c
    let compare = H.cost_cmp
  end

  module CostPQMax = PriorityQueue.MakeMax(CostCmp)
  module CostPQMin = PriorityQueue.MakeMin(CostCmp)

  type tmin = {
    mutable tmin_upper  : ident;
      (* child-node which has the lowest upper bound *)
            tmin_queue  : ident CostPQMin.t;
           (* parameter : node(ident).lower *)
  }

  type tsum = {
            tsum_divider : H.d;
            tsum_ccost   : H.c; (* constant cost for this divider *)
            tsum_queue   : (ident * int) CostPQMax.t;
            (* parameter : node(ident).diff *)
    mutable tsum_solved  : (ident * int) list;
  }

  (* Q? : possible state collision with [TMin], [TSeq], [TSum]
      A! : First encountered node takes priority other subsequent ones
   *)

  type sstate =
    | TEnd
    | TNxt of tnxt
    | TSeq of tseq
    | TSum of tsum
    | TMin of tmin

  type snode = {
            ident : ident; (* unique identifier *)
            state : H.s;
    mutable alive : alive; (* Visited | NotVisited | Solved *)
    mutable lower : H.c;
    mutable upper : H.c;
    mutable diff  : H.c;
    mutable sstate: sstate;

    (* E] Estimation *)
    mutable reached : bool ref;
      (* This field is used when traversing the search-tree to
         compute reachable nodes (and terminal-nodes) *)
  }

  type t = {
            memory : (ident, snode) Hashtbl.t;
  (* The [t.unique] table ensures that each intermediate state
       has at most one identifier.
     NB : Because of evaluation and solving an identifier may
       may represent several states, which therefore have the
       exact same set of optimal solution
   *)
            unique : (H.s * H.ap, ident) Hashtbl.t;
    mutable index : int;
  (* Store a mapping from [TNxt] to [BNat.nat]
     each number is the current upper bound on the number of sub-cases to deal with (for each TNxt)
     whem summing them up we obtain an upper-bound on the number of remaining sub-cases.
     Such upper bound takes into account the memoization strategy
   *)
            estim : (ident, (bool ref) * BNat.nat) Hashtbl.t;
  (* [bool ref] is used to determine which TNxt node are reachable *)
            oprof : OProfile.t;
  }

  let newman () = {
    memory = Hashtbl.create 10_000;
    unique = Hashtbl.create 10_000;
    index  = 0;
    estim  = Hashtbl.create 10_000;
    oprof  = OProfile.newman ~prefix:"[GuaCaml.DBBC]" ();
  }

  module ToS =
  struct
    include ToS

    let proof : H.p t = proof H.sa H.sd
    let pproof : H.pp t = pproof H.sa H.sd H.ss H.sc
    let aproof : H.ap t = aproof H.sa H.sd

    let ident (ident:ident) : string = "<"^(int ident)^">"

    let alive : alive t = function
      | NotVisited -> "NotVisited"
      | Visited    -> "Visited"
      | Solved     -> "Solved"

    let tnxt (tnxt:tnxt) : string =
      "tnxt:{proof="^(proof tnxt.tnxt_proof)^"}"

    let tseq (tseq:tseq) : string =
      "tseq:{next="^(ident tseq.tseq_next)^"; actions="^(list H.sa tseq.tseq_actions)^"; actions_cost="^(H.sc tseq.tseq_actions_cost)^"}"

    let tmin (tmin:tmin) : string =
      "tmin:{upper="^(ident tmin.tmin_upper)^"; queue="^(ignore tmin.tmin_queue)^"}"

    let tsum (tsum:tsum) : string =
      "tsum:{divider="^(H.sd tsum.tsum_divider)^"; ccost="^(H.sc tsum.tsum_ccost)^"; queue="^(ignore tsum.tsum_queue)^"; solved="^(list(ident*int) tsum.tsum_solved)^"}"

    let sstate : sstate t = function
      | TEnd -> "TEnd"
      | TNxt ss -> "TNxt "^(tnxt ss)
      | TSeq ss -> "TSeq "^(tseq ss)
      | TSum ss -> "TSum "^(tsum ss)
      | TMin ss -> "TMin "^(tmin ss)

    let snode (snode:snode) : string =
      "{ident="^(ident snode.ident)^
      "; state="^(H.ss snode.state)^
      "; alive="^(alive snode.alive)^
      "; lower="^(H.sc snode.lower)^
      "; upper="^(H.sc snode.upper)^
      "; diff="^(H.sc snode.diff)^
      "; sstate="^(sstate snode.sstate)^
      "}"

    (* [LATER]
    type t = {
              memory : (ident, snode) Hashtbl.t;
              unique : (H.s * H.ap, ident) Hashtbl.t;
      mutable index : int;

    }

    let newman () = {
      memory = Hashtbl.create 10_000;
      unique = Hashtbl.create 10_000;
      index  = 0;
    }
  *)
  end

  let zero3 () =
    H.(cost_zero(), cost_zero(), cost_zero())

  let add3 (l0, d0, u0) (l1, d1, u1) =
    H.(cost_add l0 l1,
       cost_add d0 d1,
       cost_add u0 u1)

  let sub3 (l0, d0, u0) (l1, d1, u1) =
    H.(cost_sub l0 l1,
       cost_sub d0 d1,
       cost_sub u0 u1)

  let do_list_a (al:H.a list) (s:H.s) : H.c * H.s =
    List.fold_left (fun (c, s) a ->
      let c', s' = H.do_a s a in
      (H.cost_add c c', s')
    ) (H.cost_zero(), s) al

  let tmin_add (t:t) (snode:snode) (tmin:tmin) (snode':snode) : unit =
    assert(snode.sstate = TMin tmin);
    assert(H.cost_cmp snode'.lower snode'.upper <= 0);
    (* 1) try to improve the upper bound [snode.upper] *)
    let cmpU = H.cost_cmp snode'.upper snode.upper in
    (if (cmpU < 0) || (snode'.alive = Solved && cmpU <= 0)
      (* i.e. snode'.upper < snode.upper *)
    then (
      (* we store the improved upper bound *)
      tmin.tmin_upper <- snode'.ident;
      snode.upper <- snode'.upper;
      (* removes branches with lower bound higher than current upper bound *)
      CostPQMin.shrink tmin.tmin_queue snode'.upper;
    ));
    (* 2) try to discard [snode'] if [snode'.lower] > [snode.upper] *)
    let cmpL = H.cost_cmp snode'.lower snode.upper in
    if cmpL > 0 || (cmpU > 0 && cmpL >= 0)
      (* i.e.
        if snode'.upper > snode.upper && snode'.lower >= snode.upper then discard
        if                               snode'.lower >  snode.upper then discard
        else keep
       *)
    then ()
    else (
      CostPQMin.add tmin.tmin_queue snode'.lower snode'.ident
    )

  let rec tmin_find_best (t:t) (snode:snode) (tmin:tmin) : H.c * ident * snode =
    assert(snode.sstate = TMin tmin);
    let lower', ident' = CostPQMin.pull_first tmin.tmin_queue in
    let snode' = Hashtbl.find t.memory ident' in
    let cmp = H.cost_cmp lower' snode'.lower in
    assert(cmp <= 0); (* lower bound can only increase *)
    (* by the way we try to update the upper bound *)
    if cmp = 0
    then (lower', ident', snode')
    else (
      tmin_add t snode tmin snode';
      tmin_find_best t snode tmin
    )

  let rec best_solution (t:t) (ident:ident) : H.c * H.p =
    let snode = Hashtbl.find t.memory ident in
    let c, p = match snode.sstate with
      | TEnd -> (H.cost_zero(), End)
      | TNxt tnxt -> (snode.upper, tnxt.tnxt_proof)
      | TSeq tseq -> (
        let c, p = best_solution t tseq.tseq_next in
        let c' = H.cost_add tseq.tseq_actions_cost c in
        let p' = Seq (tseq.tseq_actions, p) in
        (c', p')
      )
      | TSum tsum -> (
        let n =
          CostPQMax.length tsum.tsum_queue
          + List.length tsum.tsum_solved
        in
        let opa = Array.make n None in
        let cost = ref tsum.tsum_ccost in
        let recfun (ident, key) : unit =
          let c, p = best_solution t ident in
          cost := H.cost_add !cost c;
          opa.(key) <- Some p
        in
        CostPQMax.iter (fun _ -> recfun) tsum.tsum_queue;
        List.iter recfun tsum.tsum_solved;
        let sons = MyArray.unop opa |> Array.to_list in
        (!cost, Sum (tsum.tsum_divider, sons))
      )
      | TMin tmin -> (
        let c, p = best_solution t tmin.tmin_upper in
        assert(H.cost_cmp c snode.upper <= 0);
        (c, p)
      )
    in
    (if H.cost_cmp c snode.upper > 0
    (* i.e. the 'best solution' has a higher cost than expected (inconsistent database) *)
    then (
      print_endline "[GuaCaml.DBBC.best_solution] upper bound inconsistency with [best_solution]";
      print_endline "\t(best_solution) snode.ident="; print_int snode.ident;
      print_endline (H.ss snode.state);
      print_endline ("\tupper-bound:"^(H.sc snode.upper));
      print_endline ("\tbest-solution:"^(H.sc c));
      failwith "[GuaCaml.DBBC.best_solution] upper bound inconsistency with [best_solution]"
    ));
    (c, p)

  let rec add_pproof (t:t) (s:H.s) ?(sednxt=None) (pp:H.pp) : snode =
    let stop = OProfile.time_start t.oprof " [add_pproof] aproof_of_pproof" in
    let ap = aproof_of_pproof pp in
    stop();
    (* We check if this intermediate state has already
         been encountered *)
    ignore(OProfile.uprofile t.oprof "[add_pproof] hash" (fun()-> Hashtbl.hash (s, ap)));
    let stop = OProfile.time_start t.oprof " [add_pproof] find_opt" in
    let opident = Hashtbl.find_opt t.unique (s, ap) in
    stop();
    let stop = OProfile.time_start t.oprof " [add_pproof] main" in
    match opident with
    | Some ident -> (
      (* Yes, this intermediate state has already been seen *)
      let snode : snode = Hashtbl.find t.memory ident in
      stop();
      snode
    )
    | None -> (
      (* No, this intermediate is not recorded yet in the memory *)
      let ident : ident = match sednxt with
        | Some ident -> (
          assert(Hashtbl.mem t.memory ident);
          Hashtbl.remove t.memory ident;
          ident
        )
        | None -> (
          let ident = t.index in
          assert(not(Hashtbl.mem t.memory ident));
          t.index <- succ t.index;
          ident
        )
      in
      Hashtbl.add t.unique (s, ap) ident;
      stop();
      let snode : snode = match_pproof t ident s pp in
      Hashtbl.add t.memory ident snode;
      snode
    )
  and    match_pproof (t:t) (ident:ident) (state:H.s) (pp:H.pp) : snode =
    match pp with
    (* The programm should go only once in this branch (if there is a unique terminal state) *)
    | PEnd -> {
      ident; state;
      alive = Solved;
      lower = H.cost_zero();
      upper = H.cost_zero();
      diff  = H.cost_zero();
      sstate = TEnd;
      reached = ref true;
    }
    (* Leaf node of the partial proof, indicates where the computation has been stopped *)
    | PNxt state' -> (
      let stop = OProfile.time_start t.oprof " [match_proof] PNxt" in
      assert(state = state');
      let reached = ref true in
      Hashtbl.add t.estim ident (reached, H.estim state);
      let tnxt_proof, upper = H.upper state in
      let lower = H.lower state in
      let diff = H.cost_sub upper lower in
      stop();
      {
        ident; state;
        alive = NotVisited;
        lower; upper; diff;
        sstate = TNxt {tnxt_proof};
        reached;
      }
    )
    | PSeq (tseq_actions, tseq_actions_cost, state', nxt) ->
    (
      let snode' = add_pproof t state' nxt in
      let stop = OProfile.time_start t.oprof " [match_proof] PSeq" in
      (* Cheking State Consistency *)
      let cost'', state'' = do_list_a tseq_actions state in
      if not (state'' = state')
      then failwith "[GuaCaml.DBBC.match_pproof] PSeq:(state'' = state')";
      assert(H.cost_cmp cost'' tseq_actions_cost = 0);
      (* i.e., assert(cost'' = tseq_actions_cost) *)
      let lower = H.cost_add tseq_actions_cost snode'.lower in
      let upper = H.cost_add tseq_actions_cost snode'.upper in
      stop();
      {
        ident; state; alive = Visited;
        lower;
        upper;
        diff  = snode'.diff;
        sstate = TSeq {
          tseq_next = snode'.ident;
          tseq_actions; tseq_actions_cost;
        };
        reached = ref true;
      }
    )
    | PSum (tsum_divider, tsum_ccost, nxl) ->
    (
      let ssl = List.map (fun (s, p) -> add_pproof t s p) nxl in
      let stop = OProfile.time_start t.oprof " [match_proof] PSum" in
      let (lower, diff, upper) as total =
        ssl
        |> List.map
          (fun ss -> (ss.lower, ss.diff, ss.upper))
        |> List.fold_left add3 (zero3())
      in
      assert(H.cost_cmp (H.cost_add lower diff) upper = 0);
      let tsum_queue = CostPQMax.empty() in
      List.iteri
        (fun i ss -> CostPQMax.add tsum_queue ss.diff (ss.ident, i))
        ssl;
      stop();
      {
        ident; state; alive = Visited;
        lower; upper; diff;
        sstate = TSum {
          tsum_divider; tsum_ccost; tsum_queue;
          tsum_solved = [];
        };
        reached = ref true;
      }
    )
    | PMin (state', nxl) ->
    (
      (* Cheking State Consistency *)
      assert(state = state');
      let len_nxl = List.length nxl in
      assert(len_nxl >= 1);
      if len_nxl = 1
      then (
        match_pproof t ident state (List.hd nxl)
      )
      else (
        (* recursively add sub-pproofs *)
        let ssl = List.map (add_pproof t state) nxl in
        let stop = OProfile.time_start t.oprof " [match_proof] PMin" in
        (* find upper bound *)
        let (tmin_upper, upper) =
             ssl
          |> MyList.opmin ~cmp:(fun ssx ssy -> H.cost_cmp ssx.upper ssy.upper)
          |> Tools.unop
          |> (fun (_, ss) -> (ss.ident, ss.upper))
        in
        (* fill up [tmin.tmin_queue] with sub-search-tree *)
        let tmin_queue = CostPQMin.empty() in
        List.iter
          (fun ss ->
            (* discarding already irrelevant branches *)
            let cmp = H.cost_cmp ss.lower upper in
            if cmp > 0 || (cmp = 0 && ss.ident <> tmin_upper)
            (* i.e. [ss.lower] > [upper] || ([ss.lower] = [upper] && [ss.ident] <> [tmin_upper]) *)
            then ()
            else (CostPQMin.add tmin_queue ss.lower ss.ident)
          )
          ssl;
        let lower : H.c = CostPQMin.first tmin_queue |> fst in
        let diff : H.c = H.cost_sub upper lower in
        stop();
        {
          ident; state; alive = Visited;
          lower; upper; diff;
          sstate = TMin {
            tmin_upper; tmin_queue
          };
          reached = ref true;
        }
      )
    )

  (* Profile for 'add_pproof' method *)
  let add_pproof (t:t) (s:H.s) ?(sednxt=None) (pp:H.pp) : snode =
    let stop = OProfile.time_start t.oprof " add_pproof" in
    let snode = add_pproof t s ~sednxt pp in
    stop();
    snode

  let rec find_next (t:t) (ident:ident) : snode =
    let snode = Hashtbl.find t.memory ident in
    if snode.alive = Solved then snode
    else
    (
      let prev_upper = snode.upper in
      (* we keep the value for monotony checking at the end *)
      let snode' = match snode.sstate with
      | TEnd -> snode
      | TNxt tnxt -> (
        Hashtbl.remove t.estim ident;
        let cmp = H.cost_cmp snode.lower snode.upper in
        assert(cmp <= 0);
        if cmp = 0
        then (
          snode.alive <- Solved;
          snode
        ) else (
          let stop = OProfile.time_start t.oprof " H.next" in
          let pp = H.next snode.state in
          stop();
          let prev_upper = snode.upper in
          let snode' = add_pproof t snode.state ~sednxt:(Some ident) pp in
          (if not (H.cost_cmp snode'.upper prev_upper <= 0)
            then failwith "[GuaCaml.DBBC.find_next:TNxt] upper bound is not monotonous");
          (* i.e. the new 'best solution' has a higher cost than expected (upper-bound is not monotonous) *)
          snode'
        )
      )
      | TSeq tseq -> (
        let snode' = find_next t tseq.tseq_next in
        snode.sstate <- TSeq {tseq with tseq_next = snode'.ident};
        snode.lower <- H.cost_add snode'.lower tseq.tseq_actions_cost;
        snode.upper <- H.cost_add snode'.upper tseq.tseq_actions_cost;
        snode.diff  <- snode'.diff;
        if snode'.alive = Solved
        then snode.alive <- Solved;
        snode
      )
      | TSum tsum -> (
        let total3 = (snode.lower, snode.diff  , snode.upper ) in
        let (diff', (ident', key')) = CostPQMax.pull_first tsum.tsum_queue in
        let best = Hashtbl.find t.memory ident' in
        let best3  = (best.lower  , best.diff  , best.upper  ) in
        let other3 = sub3 total3 best3 in
        let snode' = find_next t ident' in
        if snode'.alive = Solved
        then (
          tsum.tsum_solved <-
            (ident', key') :: tsum.tsum_solved;
          if CostPQMax.length tsum.tsum_queue = 0
          then snode.alive <- Solved;
          snode
        )
        else (
          CostPQMax.add tsum.tsum_queue snode'.diff (snode'.ident, key');
          let best3' = (snode'.lower, snode'.diff, snode'.upper) in
          let (lower', diff', upper') = add3 other3 best3' in
          snode.lower <- lower';
          snode.upper <- upper';
          snode.diff  <- diff';
          snode
        )
      )
      | TMin tmin -> (
        assert(CostPQMin.length tmin.tmin_queue >= 1);
        (* [TODO] adapt it to TSum *)
        (* 1) find the best element *)
        let _, best_ident, _ = tmin_find_best t snode tmin in
        (* 2) recursive call to [find_next] *)
        let snode' = find_next t best_ident in
        (* 3) put its replacement back into the queue *)
        tmin_add t snode tmin snode';
        (* 4) Compute lower-bound by finding the new best element
              AND putting it back into the queue *)
        let lower'', ident'', snode'' = tmin_find_best t snode tmin in
        tmin_add t snode tmin snode'';
        (* 5) If there is a single element left into the queue
              Return this element (calling ancestor redirection)
              Replace current node with a stub (non-calling ancestors and future ancestors)
         *)
        if CostPQMin.length tmin.tmin_queue = 1
        then (
          let _, _, snode''' = tmin_find_best t snode tmin in
          assert(snode''' == snode'');
          (if snode'''.alive = Solved then snode.alive <- Solved);
          let test_lower = H.cost_cmp snode.lower snode'''.lower <= 0 in
          (* i.e., snode.lower <= snode'.lower (lower increases) *)
          let test_upper = H.cost_cmp snode.upper snode'''.upper >= 0 in
          (* i.e., snode.upper >= snode'.upper (upper decreases) *)
          if not test_lower || not test_upper
          then (
            print_endline ("[GuaCaml.DBBC.find_next] [TMin] test_lower:"^(ToS.bool test_lower));
            print_endline ("[GuaCaml.DBBC.find_next] [TMin] test_upper:"^(ToS.bool test_upper));
            print_endline ("[GuaCaml.DBBC.find_next] [TMin] state:"^(H.ss snode.state));
            failwith "[GuaCaml.DBBC.find_next] [TMin] non-monotonicity of cost estimation";
          );
          snode.lower <- snode'''.lower;
          snode.upper <- snode'''.upper;
          snode.diff  <- snode'''.diff;
          snode.sstate <- TSeq {
            tseq_next = snode'''.ident;
            tseq_actions = [];
            tseq_actions_cost = H.cost_zero();
          };
          snode'''
        )
        else snode
      )
    in
    assert(H.cost_cmp snode'.upper prev_upper <= 0);
    snode'
  )

  let total_estim (t:t) : int * BNat.nat =
    let nb = ref 0 in
    let total = ref (BNat.zero()) in
    Hashtbl.iter (fun ident (_, value) ->
      incr nb;
      total := BNat.add !total value
    ) t.estim;
    (!nb, !total)

  let rec update_reach (t:t) (ident:ident) : unit =
    let snode = Hashtbl.find t.memory ident in
    if !(snode.reached) then ()
    else (
      snode.reached := true;
      match snode.sstate with
      | TEnd -> ()
      | TNxt _ -> ()
      | TSeq tseq -> (
        update_reach t tseq.tseq_next
      )
      | TSum tsum -> (
        List.iter (fun (ident, _) -> update_reach t ident) tsum.tsum_solved;
        CostPQMax.iter (fun _ (ident, _) -> update_reach t ident) tsum.tsum_queue;
      )
      | TMin tmin -> (
        CostPQMin.iter (fun _ ident -> update_reach t ident) tmin.tmin_queue;
      )
    )

  let reach_estim (t:t) (nl:snode list) : int * int * BNat.nat =
    (* reset all [reached] field to [false] *)
    Hashtbl.iter (fun ident snode ->
      snode.reached := false
    ) t.memory;
    List.iter (fun (snode:snode) ->
      update_reach t snode.ident
    ) nl;
    let nbnxt = ref 0 in
    let total = ref (BNat.zero()) in
    Hashtbl.iter (fun ident (reached, value) ->
      if !reached then (
        incr nbnxt;
        total := BNat.add !total value
      )
    ) t.estim;
    let nbnode = ref 0 in
    Hashtbl.iter (fun ident snode ->
      if !(snode.reached) then incr nbnode
    ) t.memory;
    (!nbnxt, !nbnode, !total)

  let solve ?(verbose=true) ?(wait_time=10.) (t:t) (s:H.s) : H.c * H.p =
    let root = ref (add_pproof t s (H.preproc s)) in
    let niter = ref 0 in
    let last_time = ref (Tools.utime()) in
    print_endline "[GuaCaml.DBBC.solver] === INFO ===";
    print_endline "[GuaCaml.DBBC.solver] A : total | B : reachable";
    print_endline "[GuaCaml.DBBC.solver]     estim : estimate of the number of the remaining case to consider";
    print_endline "[GuaCaml.DBBC.solver] nb. snode : number of state node";
    print_endline "[GuaCaml.DBBC.solver] nb.  tnxt : number of state node which are in the 'Nxt' state";
    print_endline "[GuaCaml.DBBC.solver] === INFO ===";
    while !root.alive <> Solved
    do
      incr niter;
      root := find_next t !root.ident;
      let new_time = Tools.utime() in
      if !niter = 1 || (new_time >= !last_time +. wait_time)
      then (
        let time_stop = Tools.utime() in
        last_time := new_time;
        print_newline();
        print_endline      "[GuaCaml.DBBC.solver] === STATS ===";
        print_endline ToS.("[GuaCaml.DBBC.solver] niter:"^(pretty_int !niter));
        print_endline ToS.("[GuaCaml.DBBC.solver] upper:"^(H.sc !root.upper));
        print_endline ToS.("[GuaCaml.DBBC.solver] lower:"^(H.sc !root.lower));
        let nbnxt, estim = total_estim t in
        print_endline ToS.("[GuaCaml.DBBC.solver] [A] estim:"^(BNat.to_pretty_string estim));
        print_endline ToS.("[GuaCaml.DBBC.solver] [A] nb. snode:"^(pretty_int (Hashtbl.length t.memory)));
        print_endline ToS.("[GuaCaml.DBBC.solver] [A] nb.  tnxt:"^(pretty_int nbnxt));
        let nbnxt, nbnode, estim = reach_estim t [!root] in
        print_endline ToS.("[GuaCaml.DBBC.solver] [B] estim:"^(BNat.to_pretty_string estim));
        print_endline ToS.("[GuaCaml.DBBC.solver] [B] nb. snode:"^(pretty_int nbnode));
        print_endline ToS.("[GuaCaml.DBBC.solver] [B] nb.  tnxt:"^(pretty_int nbnxt));
        let time_delta = Tools.utime() -. time_stop in
        print_endline ToS.("[GuaCaml.DBBC.solver] showing statistiques:"^(float time_delta)^" s.");
        print_endline      "[GuaCaml.DBBC.solver] === STATS ===";
      )
    done;
    print_newline();
    print_endline "[GuaCaml.DBBC.solver] === PROFILE ===";
    OProfile.print t.oprof;
    print_endline "[GuaCaml.DBBC.solver] === PROFILE ===";
    print_newline();
    best_solution t !root.ident

  let solve t s = solve t s
end
