(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraphHFT : GGLA-based implementation of H-FT-Graph
 *
 * === NOTES ===
 *
 * H-Graphs are undirected graphs where true twins (i.e., vertices with
 * identical neighbors) are merged, such transformation is called
 * H-reduction
 *
 * === NOTES ===
 *
 * H-FT-Graph
 * House - Fixed Terminals - Graphs
 * This kind of graph is defined for WAP (Weighted Adjacency Propagation) Problem.
 * House = true-twins are merged
 * Fixed Terminals = some vertices cannot be supressed,
 *   thus cannot merge with regular vertices
 *)

open Extra
open STools
open BTools
module GGLA = GraphGenLA
open GGLA.Type

module Type =
struct
  type vtag = {
    vtag_names  : int list;
    vtag_weight : int;
    vtag_alive  : bool;
    (*
      true  => Normal
      false => NoKill
     *)
  }

  type etag = unit

  type hv = (vtag, unit) vertex
  type hg = (vtag, unit) graph

  type hgl = (vtag list, etag list) graph
end
open Type

module ToS =
struct
  open ToS
  open GGLA.ToS

  let vtag ?(short=true) (vt:vtag) =
    if short
    then (
      "{vtag}{ names="^(list int vt.vtag_names)^
      "; weight="^(int vt.vtag_weight)^
      "; alive="^(bool vt.vtag_alive)^" }"
    )
    else (
      "{ vtag_names="^(list int vt.vtag_names)^
      "; vtag_weight="^(int vt.vtag_weight)^
      "; vtag_alive="^(bool vt.vtag_alive)^" }"
    )

  let etag ?(short=true) () = unit ()

  let hv ?(short=true) hv = vertex (vtag ~short) (etag ~short) hv
  let hg ?(short=true) hg = graph  (vtag ~short) (etag ~short) hg

  let pprint_hg ?(short=true) hg =
    pprint_graph (vtag ~short) (etag ~short) hg

  let pprint_ophg ?(short=true) hg =
    pprint_opgraph (vtag ~short) (etag ~short) hg
end

module ToB =
struct
  open ToB
  open GGLA.ToB

  let vtag (vt:vtag) (s:stream) : stream =
    trio (list int) int bool (vt.vtag_names, vt.vtag_weight, vt.vtag_alive) s

  let etag = unit

  let hv (hv:hv) (s:stream) : stream = vertex vtag etag hv s
  let hg (hg:hg) (s:stream) : stream = graph  vtag etag hg s
end

module OfB =
struct
  open OfB
  open GGLA.OfB

  let vtag (s:stream) : vtag * stream =
    let (vtag_names, vtag_weight, vtag_alive), stream = trio (list int) int bool s in
    {vtag_names; vtag_weight; vtag_alive}, stream

  let etag = unit

  let hv (s:stream) : hv * stream = vertex vtag etag s
  let hg (s:stream) : hg * stream = graph  vtag etag s
end

let print_vertex ?(short=true) (v:hv) : unit =
  print_string (ToS.hv ~short v)
let print_graph ?(short=true) (g:hg) : unit =
  ToS.pprint_hg ~short g

let vertex_weight (v:hv) : int =
  v.tag.vtag_weight
let vertex_alive  (v:hv) : bool =
  v.tag.vtag_alive
let vertex_names  (v:hv) : int list =
  v.tag.vtag_names

(* [TODO] Check for H-reduction *)
let check ?(hred=false) (g:hg) =
  GGLA.Check.graph g &&
  GGLA.Utils.graph_is_undirected g &&
  GGLA.Utils.graph_is_reflexive ~refl:false g &&
  (if hred
   then GGLA.TrueTwinsFree.compute_naive g vertex_alive
   else true)

let vtag_add (v1:vtag) (v2:vtag) : vtag =
  assert(v1.vtag_alive = v2.vtag_alive);
  {
    vtag_names = SetList.union v1.vtag_names v2.vtag_names;
    vtag_weight = v1.vtag_weight + v2.vtag_weight;
    vtag_alive = v1.vtag_alive;
  }

(* Collapses a list-extend H-Graph to a regular H-Graph *)
let hg_of_hgl (gl:hgl) : hg =
  let map_vtag_list (vl:vtag list) : vtag =
    match vl with
    | [] -> (failwith "[GuaCaml.GraphHFT.hg_of_hgl] error")
    | v0 :: vl' ->
      List.fold_left vtag_add v0 vl'
  in
  let map_etag_list (el:etag list) : etag = ()
  in
  GGLA.Utils.graph_map_both
    map_vtag_list
    map_etag_list
    gl
  |> GGLA.Utils.graph_rm_reflexive
  |> Tools.check_print ~debug_only:true (check ~hred:true)
    (fun hg ->
      let open STools.ToS in
      print_endline  "[hg_of_hgl] [error] \"[hg] is not reduced\"";
      print_endline ("[hg_of_hgl] [error] hg:"^(ToS.hg hg));
      print_endline ("[hg_of_hgl] [error] check_graph:"^(bool (GGLA.Check.graph hg)));
      print_endline ("[hg_of_hgl] [error] is_undirected:"^(bool (GGLA.Utils.graph_is_undirected hg)));
      print_endline ("[hg_of_hgl] [error] (is_reflexive ~refl:false):"^(bool (GGLA.Utils.graph_is_reflexive ~refl:false hg)));
      print_endline  "[hg_of_hgl] [error] end";
    )

(* Alternative Versions of [GGLA.TrueTwin.compute_*] *)
let compute_truetwins_naive (hg:hg) =
  GGLA.TrueTwins.compute_naive hg vertex_alive

let compute_truetwins_naiveD (hg:hg) =
  GGLA.TrueTwins.compute_naiveD hg vertex_alive

let compute_truetwins_naive_pseudoquad (hg:hg) =
  GGLA.TrueTwins.compute_naive_pseudoquad hg vertex_alive

let compute_truetwins_pseudolinear (hg:hg) =
  GGLA.TrueTwins.compute_pseudolinear hg vertex_alive

let normalize (hg:hg) : hg =
  (*
    print_newline();
    print_graph hg;
    print_newline();
   *)
  (* remark: compute_cmp = compute_pseudolinear *)
  let qt = GGLA.TrueTwins.compute_cmp hg vertex_alive in
  hg_of_hgl qt.GGLA.Component.qgraph
  |> Tools.check_print ~debug_only:true (check ~hred:true)
    (fun hg' ->
      print_string "[GraphHFT.normalize] [error] begin"; print_newline();
      print_string "[GraphHFT.normalize] [error] (GGLA.TrueTwinsFree.compute_naive hg' vertex_alive = false)"; print_newline();
      print_string "[GraphHFT.normalize] [error] hg:"; print_newline();
      print_graph ~short:false hg; print_newline();
      print_string "[GraphHFT.normalize] [error] hg':"; print_newline();
      print_graph ~short:false hg'; print_newline();
      print_string "[GraphHFT.normalize] [error] end"; print_newline();
    )

let vertex_contraction (hg:hg) (iv:int) : hg =
  let v = hg.(iv) in
  (* adding a clique on the adjacency of the vertex [v] *)
  (* we may chose to not use the "~reflexive_false:true"
     option of graph_add_clique, as reflexives edges are
     removed again later (and may appear during merging) *)
  let hgK = GGLA.Utils.graph_add_clique ~replace:true ~reflexive_false:true hg v.edges in
  (* vertex [v] is removed *)
  let hg' = GGLA.Utils.graph_rm_vertex v.index hgK in
  (* the result is normalized *)
  (* [TODO] perform local normalization *)
  normalize hg'

(*
  ft : fixed terminals (default = [], no ft-vertex)
 *)
let of_GraphLA ?(ft=([]:int list)) (g:GraphLA.graph) : hg =
  let len = Array.length g.GraphLA.edges in
  let fta = MyArray.of_indexes len ft in
  (* alive.(i) = not fta.(i) *)
  let map_vertex (index:int) (el:int list) =
    assert(SetList.sorted_nat el);
    let el = SetList.minus el [index] in
    Some {
      index;
      tag = {
        vtag_names  = [index];
        vtag_weight = 1;
        vtag_alive  = not fta.(index);
      };
      edges = List.map (fun e -> (e,())) el;
    }
  in
  let ophg = Array.mapi map_vertex g.GraphLA.edges in
  let dead = MyArray.of_indexes len g.GraphLA.nodes in
  Array.iteri (fun i b -> if not b then ophg.(i) <- None) dead;
  let hg = GGLA.Utils.unop_opgraph ophg in
  if not (check ~hred:false hg)
  then (ToS.pprint_hg hg; (failwith "[GuaCaml.GraphHFT] error"));
  normalize hg

let neighbors_weight (g:hg) (iv:int) : int =
  let v = g.(iv) in
  List.fold_left
    (fun card (iu,()) -> card + vertex_weight g.(iu))
    (vertex_weight v)
     v.edges

let total_weight (hg:hg) : int =
  Array.fold_left (fun w hv -> w + vertex_weight hv) 0 hg

let wap_Sc (g:hg) (iv:int) : BNat.nat =
  BNat.pow2 (neighbors_weight g iv)

let call_to_wap_S = ref 0
let reset_call_to_wap_S () : int  =
  let value = !call_to_wap_S in
  call_to_wap_S:=0;
  value

let wap_S (g:hg) (iv:int) : BNat.nat * hg =
  incr call_to_wap_S;
  let sc = wap_Sc g iv in
  let sg = vertex_contraction g iv in
  (sc, sg)

let wap_S' (g:hg) (iv:int) : int * BNat.nat * hg =
  incr call_to_wap_S;
  let k = neighbors_weight g iv in
  let sc = BNat.pow2 k in
  let sg = vertex_contraction g iv in
  (k, sc, sg)

let wap_SS (g:hg) (ivl:int list) : BNat.nat * hg =
  List.fold_left (fun (sc, sg) iv ->
    let sc', sg' = wap_S sg iv in
    (BNat.(sc +/ sc'), sg')
  ) (BNat.zero(), g) ivl

let wap_SS_get_names (g:hg) (ivl:int list) : BNat.nat * hg * int list list =
  let sc, sg, vll =
    List.fold_left (fun (sc, sg, vll) iv ->
      let vl' = vertex_names sg.(iv) in
      let sc', sg' = wap_S sg iv in
      (BNat.(sc +/ sc'), sg', vl' :: vll)
    ) (BNat.zero(), g, []) ivl
  in (sc, sg, List.rev vll)

let graph_set_alive (vl:int list) ?(alive=false) (hg:hg) : hg =
  let vtag_alive = alive in
  GGLA.Utils.graph_map_vtag (fun tag -> {tag with vtag_alive}) hg

let call_to_wap_R = ref 0
let reset_call_to_wap_R () : int  =
  let value = !call_to_wap_R in
  call_to_wap_R:=0;
  value

let wap_R (hg:hg) (vl:int list) : hg list =
  incr call_to_wap_R;
  let len = Array.length hg in
  assert(List.for_all (fun iv -> 0 <= iv && iv < len) vl);
  (* we may chose to not use the "~reflexive_false:true"
     option of graph_add_clique, as reflexives edges are
     removed again later (and may appear during merging) *)
  let hgK = GGLA.Utils.graph_add_clique ~replace:true ~reflexive_false:true hg (vl ||> (fun iv -> (iv,()))) in
  (* change status to [alive = false] on [vl] vertices *)
  let hgK = graph_set_alive vl hgK in
  (* decomposition into connected components *)
  let hgKi, _, _ = GGLA.Connected.compute vertex_alive hgK in
  hgKi |> Array.to_list ||> normalize

let find_pendant_vertex (hg:hg) : int option =
  let rec pfv_rec hg i =
         if i >= Array.length hg
      then None
    else if List.length hg.(i).edges = 1
      then Some i
      else pfv_rec hg (succ i)
  in pfv_rec hg 0
