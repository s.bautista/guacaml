(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GGLA vertex separator linear
 *
 * === CONTRIBUTORS ===
 *
 * Joan Thibault
 *  - joan.thibault@irisa.fr
 * Clara Begue
 *  - clara.begue@ens-rennes.fr
 *)

open GraphGenLA.Type
open GraphGenLA.Utils

(*to finish*)

module VertexSeparatorLinear =
struct

  (*  @JoanThibault #alt [from_bool_array_to_list a]
        [MyArray.list_of_indexes_true a] *)
  (* Stack : O(1) *)
  let from_bool_array_to_list (a:bool array) : int list =
    let rec aux (carry:int list) (a:bool array) (i:int) =
      if (i = Array.length a) then List.rev carry
      else (if a.(i) then aux (i::carry) a (i+ 1)
            else aux carry a (i+1))
    in aux [] a 0

  (* DFS of graph *)
  let rec visit_from_linear (node:int) (graph:('v, 'e) graph)
                    (separator:bool array) (date_array: int array) (date:int ref) : int =
    let neib = neighbors_set graph [node] in
    date_array.(node) <- !date ;
    date := !date + 1 ;
    aux !date graph neib node separator date_array date ;
  and aux (mini:int) (graph:('v, 'e) graph) (neib:int list) (node:int)
                (separator:bool array) (date_array: int array) (date:int ref) : int =
        match neib with
          | [] -> mini
          | h::t ->
            (if (date_array.(h) = 0) then
              (if (node=1 && !date > 1) then (separator.(node) <- true) ;
                let mini_h = visit_from_linear h graph separator date_array date in
                let min_h = min mini_h !date in
                if (min_h = date_array.(node)) then (separator.(node) <- true);
                aux (min mini min_h) graph t node separator date_array date
              )
              else
                (* if data_array.(node) <= date then ... *)
                (let min_h = min date_array.(node) !date in
                  if (min_h = date_array.(node)) then (separator.(node) <- true);
                  aux (min mini min_h) graph t node separator date_array date
                )
              )

    (* give the list of node vertex-separator of [graph] *)
    let list_separator (graph:('v, 'e) graph) : int list =
      let separator = Array.make (Array.length graph) false in
      let date_array = Array.make (Array.length graph) 0 in
      let date = ref 0 in
      let _ = visit_from_linear 0 graph separator date_array date in
      from_bool_array_to_list separator;
end
