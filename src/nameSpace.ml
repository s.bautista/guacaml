(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * NameSpace : A simpl namespace manager
 *)

type 'tag t = {
        default : 'tag;
            sep : string;
  mutable wires : (string * 'tag) list;
          names : (string, unit) Hashtbl.t;
          table : (string, (string ref) * (int ref) * (bool ref)) Hashtbl.t;
}

let newman ?(sep="") ?(h_size = 10_000) (default:'tag) () =
  {
    default; sep;
    wires = [];
    names = Hashtbl.create h_size;
    table = Hashtbl.create h_size
  }

let get (t:_ t) (s:string) : string =
  let w, cnt, alive = Hashtbl.find t.table s in
  assert(!alive); (* has been deleted earlier *)
  !w

let rec find_name (t:_ t) (n:int) (s:string) : string * int =
  let w = s ^ t.sep ^ (string_of_int n) in
  if Hashtbl.mem t.names w
  then find_name t (succ n) (s:string)
  else (w, succ n)

(* if "w" does not already exist return "w"
   otherwise "w-[n]" where [n] is the smallest number which does
     not cause conflict with current variables *)
let push (t:_ t) ?tag (s:string) : string =
  let tag = match tag with Some tag -> tag | None -> t.default in
  match Hashtbl.find_opt t.table s with
  | None -> (
    Hashtbl.add t.table s (ref s, ref 0, ref true);
    t.wires <- (s , tag) :: t.wires;
    s
  )
  | Some (w, cnt, alive) -> (
    alive := true;
    let w', n' = find_name t !cnt s in
    Hashtbl.add t.names w' ();
    w   := w';
    cnt := n';
    t.wires <- (w', tag) :: t.wires;
    w'
  )

(* same as [push t ~tag s = s]
   except that it fails if [s] aready exists
 *)
let push' (t:_ t) ?tag (s:string) : string =
  let tag = match tag with Some tag -> tag | None -> t.default in
  match Hashtbl.find_opt t.table s with
  | None -> (
    Hashtbl.add t.table s (ref s, ref 0, ref true);
    t.wires <- (s , tag) :: t.wires;
    s
  )
  | Some _ -> (failwith "[GuaCaml.NameSpace] error")

let delete (t:_ t) (s:string) : unit =
  match Hashtbl.find_opt t.table s with
  | None -> (Hashtbl.add t.table s (ref s, ref 0, ref false))
  | Some (w, cnt, alive) -> (alive := false)

type var_type =
  | Input
  | Wire
  | Output

let split_var_type ?(rev=true) (l:('a * var_type) list) : 'a list * 'a list * 'a list =
  let rec loop cI cW cO = function
    | [] -> (cI, cW, cO)
    | (a, vt)::tail -> (
      match vt with
      | Input  -> loop (a::cI) cW cO tail
      | Wire   -> loop cI (a::cW) cO tail
      | Output -> loop cI cW (a::cO) tail
    )
  in
  if rev
  then (
    let cI, cW, cO = loop [] [] [] l in
    List.(rev cI, rev cW, rev cO)
  )
  else (loop [] [] [] l)
