(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * IntHeap : facility for serialization and deserialization of int list
 *)

open BTools

module BW :
sig
  open ToBStream

  val delta : ?diff:('a option -> 'a -> 'a) -> 'a t -> 'a list t
  (* [l] must be sorted with non-decreasing order *)
  val delta_int : ?bw_int:int t -> int list t
  (* [l] must be sorted with increasing order *)
  val delta'_int : ?bw_int:int t -> int list t
  val intlist_core : int list -> int t
  val intlist : ?delta:('a -> 'b -> 'b) -> int list -> Channel.t -> int t
  val intheap : int list -> Channel.t -> int t

  (* same as intheap but the int list given as input has no duplicate *)
  val intheap' : int list -> Channel.t -> int t
end

module BR :
sig
  open OfBStream

  val delta : 'a t -> ?plus:('a option -> 'a -> 'a) -> 'a list t
  val delta_int : ?br_int:int t -> int list t
  val delta'_int : ?br_int:int t -> int list t
  val intlist_core : int list -> int t
  val intlist : Channel.t -> int t
  val intheap : Channel.t -> int t
  val intheap' : Channel.t -> int t
end
