(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GGLA vertex separator
 *
 * === CONTRIBUTORS ===
 *
 * Joan Thibault
 *  - joan.thibault@irisa.fr
 * Clara Begue
 *  - clara.begue@ens-rennes.fr
 *)

open GraphGenLA.Type
open GraphGenLA.Utils
open GGLA_utils

module One_separator =
struct

  (*  [visit_from node graph to_visit] make a deep forst search into graph
      from node
        Time  : O(|V|+|E|)
        Stack : O(|V|)
        Heap  : O(|V|+|E|) (storing [neib])
   *)
  let visit_from (node:int) (graph:('v, 'e)graph) (to_visit:int array) : unit =
    let rec loop_node (node:int) (graph:('v, 'e)graph) (to_visit:int array) : unit =
      to_visit.(node) <- 1;
      List.iter (fun j ->
          if (to_visit.(j) = -1)
          then (loop_node node graph to_visit)
        )
        (neighbors_set graph [node])
    in loop_node node graph to_visit

  (*  [init_to_visit g = a] where:
        g : ('v, 'e) graph
        a : int array

      ensures :
        \forall i, match a.(i) with
          |  0 -> g.(i) does not exists
                        (@JoanThibault is disconnected ?)
                        (@JoanThibault or should not be traversed ?)
          | -1 -> g.(i) exists and have to be visited
          |  1 -> g.(i) exists and have been visited

      Time  : O(|V|)
      Stack : O(1)
      Heap  : O(|V|)
   *)
  let init_to_visit (graph:('v, 'e) graph) : int array =
    Array.map
      (fun v -> match v.edges with [] -> 0 | _ -> -1)
       graph

  (*  [separator_node] tests if node is a separator in graph

      Time  : O(|V|+|E|)
      Stack : O(1)
      Heap  : O(|V|+|E|)
   *)
  let separator_node (graph:('v, 'e) graph) (node:int) : bool =
    let neib = neighbors_set graph [node] in
    match neib with
      | [] -> false
      | h::_ -> (
        let to_visit = init_to_visit graph in
        to_visit.(node) <- 0;
        visit_from h graph to_visit;
        Array.for_all (fun x -> x <> -1) to_visit
      )

  (*  [one_separator] returns the list of vertex separator in [graph]

      Time  : O(|V|x(|V|+|E|))
      Stack : O(1)
      Heap  : O(|V|+|E|)
   *)
  let one_separator (graph:('v, 'e) graph) : int list =
    let rec aux (carry:int list) (graph:('v, 'e) graph) (i:int) =
      if (i = Array.length graph)
        then List.rev carry
      else if (separator_node graph i)
        then aux (i::carry) graph (i+1)
        else aux     carry  graph (i+1)
    in aux [] graph 0
end
