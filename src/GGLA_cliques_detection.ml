(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GGLA cliques detection
 *
 * === CONTRIBUTORS ===
 *
 * Joan Thibault
 *  - joan.thibault@irisa.fr
 * Clara Begue
 *  - clara.begue@ens-rennes.fr
 *)

open GraphLA

(* [WIP]
let rec cliques_k (graph:graph) (k:int) : int list list =
  if (k == 1) then List.map (fun x -> [x]) graph.nodes
  else
    let clique_j = cliques_k graph (k-1) in
 *)

