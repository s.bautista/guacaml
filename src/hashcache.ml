(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Hashcache : generic hash-based caching table
 *)

open STools

let default_size = 1_000_000

external seeded_hash_param :
  int -> int -> int -> 'a -> int = "caml_hash" [@@noalloc]

let hash x = seeded_hash_param 10 100 0 x
let hash_param n1 n2 x = seeded_hash_param n1 n2 0 x
let seeded_hash seed x = seeded_hash_param 10 100 seed x

(* To pick random seeds if requested *)

let randomized_default =
  let params =
    try Sys.getenv "OCAMLRUNPARAM" with Not_found ->
    try Sys.getenv "CAMLRUNPARAM" with Not_found -> "" in
  String.contains params 'R'

let randomized = ref randomized_default

let randomize () = randomized := true
let is_randomized () = !randomized

let prng = lazy (Random.State.make_self_init())

module Module =
struct

  type ('a, 'b) t = {
    data : ('a * 'b) option array;
    seed : int;
    mutable hitCnt : int;
    mutable addCnt : int;
  }

  (* Creating a fresh, empty table *)

  let rec power_2_above x n =
    if x >= n then x
    else if x * 2 > Sys.max_array_length then x
    else power_2_above (x * 2) n

  let create ?(random = !randomized) initial_size =
    let size = power_2_above 16 initial_size in
    let seed = if random then Random.State.bits (Lazy.force prng) else 0 in
    { data = Array.make size None; seed; hitCnt = 0; addCnt = 0 }

  let clear (t:(_, _)t) : unit =
    Array.fill t.data 0 (Array.length t.data) None

  let reset (t:(_, _) t) : unit =
    t.hitCnt <- 0;
    t.addCnt <- 0;
    clear t

  let copy cache = { cache with data = Array.copy cache.data }

  let length cache = Array.length cache.data

  let key_index cache key =
    (seeded_hash cache.seed key) land (Array.length cache.data - 1)

  let add cache key data =
    let index = key_index cache key in
    cache.data.(index) <- (Some(key, data))

  let find cache key =
    let index = key_index cache key in
    match cache.data.(index) with
    | Some(key', data) when key = key' -> Some data
    | _ -> None

  let memoize cache key compute =
    let index = key_index cache key in
    match cache.data.(index) with
    | Some(key', data) when key = key' -> data
    | _ ->
      let data = compute() in
      cache.data.(index) <- (Some(key, data));
      data

  let apply cache compute key =
    let index = key_index cache key in
    match cache.data.(index) with
    | Some(key', data) when key = key' ->
      cache.hitCnt <- cache.hitCnt + 1;
      data
    | _ ->
      cache.addCnt <- cache.addCnt + 1;
      let data = compute key in
      cache.data.(index) <- (Some(key, data));
      data

  let mem cache key =
    let index = key_index cache key in
    match cache.data.(index) with
    | Some(key', _) when key = key' -> true
    | _ -> false

  let iter (f : ('a * 'b) -> unit) (cache:('a, 'b) t) =
    Array.iter (function None -> () | Some key_data -> f key_data) cache.data

  let to_stree dump_key_data cache =
    let liste = ref [] in
    iter (fun key_data -> liste:=key_data::(!liste)) cache;
    ToSTree.((((int * int) * int) * int) * (list dump_key_data))
    ((((Array.length cache.data, cache.seed), cache.hitCnt), cache.addCnt), !liste)

  let of_stree load_key_data stree =
    let (((size, seed), hitCnt), addCnt), liste = OfSTree.((((int * int)* int )* int )* (list load_key_data)) stree in
    let data = Array.make size None in
    let key_index key = (seeded_hash seed key) land (size - 1) in
    List.iter (fun (key, elem) -> data.(key_index key) <- Some(key, elem)) liste;
    {data; seed; hitCnt; addCnt}

  let dump_stats cache = Tree.(ToSTree.(Node [
    Node [Leaf "size"  ; int(Array.length cache.data)];
    Node [Leaf "hitCnt"; int cache.hitCnt];
    Node [Leaf "addCnt"; int cache.addCnt];
  ]))

  let default_size = 1_000_000

  let make initial_size =
    let memo = create initial_size in
    let apply = apply memo in
    (memo, apply)
end

module Conv =
struct

  type ('a, 'aa, 'b, 'bb) t = {
    cache : ('aa, 'bb) Module.t;
    a2aa : 'a -> 'aa;
    aa2a : 'aa -> 'a;
    b2bb : 'b -> 'bb;
    bb2b : 'bb -> 'b;
  }

  let reset tt : unit =
    Module.reset tt.cache

  let clear tt : unit =
    Module.clear tt.cache

  let get_cache ttcache = ttcache.cache
  (* To pick random seeds if requested *)

  let iter f ttcache =
    Array.iter (function
      | None -> ()
      | Some (key, data) -> f(ttcache.aa2a key, ttcache.bb2b data)) ttcache.cache.Module.data

  let get_cache ttcache = ttcache.cache

  (* [FIXME] to deal with recoverable identifier *)
  let iter f ttcache =
    Array.iter (function
      | None -> ()
      | Some (key, data) -> f(ttcache.aa2a key, ttcache.bb2b data)) ttcache.cache.data

  (* [FIXME] to deal with recoverable identifier *)
  let apply_ifnotfound ttcache cache compute akey aakey index =
    cache.Module.addCnt <- cache.Module.addCnt + 1;
    let bdata =
      try compute akey
      with Not_found -> (
        print_string "[GuaCaml/Hashcache.Conv.apply] compute:failure:Not_found"; print_newline();
        raise Not_found
      )
    in
    let bbdata =
      try ttcache.b2bb bdata
      with Not_found -> (
        print_string "[GuaCaml/Hashcache.Conv.apply] b2bb:failure:Not_found"; print_newline();
        raise Not_found
      )
    in
    cache.data.(index) <- (Some(aakey, bbdata));
    bdata

  (* [FIXME] to deal with recoverable identifier *)
  let apply ttcache compute akey =
    let cache = ttcache.cache in
    let aakey =
      try ttcache.a2aa akey
      with Not_found -> (
        print_string "[GuaCaml/Hashcache:Conv:apply] a2aa:failure:Not_found"; print_newline();
        raise Not_found
      )
    in
    let index = Module.key_index cache aakey in
    match cache.data.(index) with
    | Some(aakey', bbdata) when aakey = aakey' -> (
      try
        let res = ttcache.bb2b bbdata in
        cache.Module.hitCnt <- cache.Module.hitCnt + 1;
        res
      with Not_found -> (
        print_string "[GuaCaml/Hashcache.Conv.apply] Not_found : begin"; print_newline();
        let res = apply_ifnotfound ttcache cache compute akey aakey index in
        print_string "[GuaCaml/Hashcache.Conv.apply] Not_found : done"; print_newline();
        res
      )
    )
    | _ -> apply_ifnotfound ttcache cache compute akey aakey index

  let make ?(random = !randomized) a3aa b3bb initial_size =
    let cache = Module.create ~random:random initial_size in
    let ttcache = { cache; a2aa = fst a3aa; aa2a = snd a3aa; b2bb = fst b3bb; bb2b = snd b3bb } in
    (ttcache, apply ttcache)

  let dump_stats ttcache = Module.dump_stats ttcache.cache
end

(* [WakeUpConv] module is similar to [Conv] except that :
    When decompressing and element (either [aa] or [bb]), the user should return an additional element.
    This element can used to carry an holder
    If the element cannot be recovered (e.g., a weak link is broken) one should raise the [Tools.BrokenWeakRef] exception
 *)
module WakeUpConv =
struct

  type ('a, 'aa, 'ha, 'b, 'bb, 'hb) t = {
    cache : ('aa, 'bb) Module.t;
    a2aa : 'a -> 'aa;
    aa2a : 'aa -> 'a * 'ha;
      (* May raise [Tools.BrokenWeakRef] *)
    b2bb : 'b -> 'bb;
    bb2b : 'bb -> 'b * 'hb;
      (* May raise [Tools.BrokenWeakRef] *)
  }

  let reset tt : unit =
    Module.reset tt.cache

  let get_cache ttcache = ttcache.cache

  (* [FIXME] to deal with recoverable identifier *)
  let iter f ttcache =
    Array.iter (function
      | None -> ()
      | Some (key, data) -> f(ttcache.aa2a key, ttcache.bb2b data)) ttcache.cache.data

  (* [FIXME] to deal with recoverable identifier *)
  let apply_ifnotfound ttcache cache compute akey aakey index =
    cache.Module.addCnt <- cache.Module.addCnt + 1;
    let bdata, hb =
      try compute akey
      with (Tools.BrokenWeakRef _) as err -> (
        print_string "[GuaCaml/Hashcache.WakeUpConv.apply] compute:failure:BrokenWeakRef"; print_newline();
        raise err
      )
    in
    let bbdata =
      try ttcache.b2bb bdata
      with (Tools.BrokenWeakRef _) as err -> (
        print_string "[GuaCaml/Hashcache.WakeUpConv.apply] b2bb:failure:BrokenWeakRef"; print_newline();
        raise err
      )
    in
    cache.data.(index) <- (Some(aakey, bbdata));
    (bdata, hb)

  (* [FIXME] to deal with recoverable identifier *)
  let apply (ttcache:('a, 'aa, 'ha, 'b, 'bb, 'hb)t) (compute:'a -> 'b * 'hb) (akey:'a) =
    let cache = ttcache.cache in
    let aakey =
      try ttcache.a2aa akey
      with (Tools.BrokenWeakRef _) as err -> (
        print_string "[GuaCaml/Hashcache.WakeUpConv.apply] a2aa:failure:BrokenWeakRef"; print_newline();
        raise err
      )
    in
    let index = Module.key_index cache aakey in
    match cache.data.(index) with
    | Some(aakey', bbdata) when aakey = aakey' -> (
      try
        let bdata, hb = ttcache.bb2b bbdata in
        cache.Module.hitCnt <- cache.Module.hitCnt + 1;
        (bdata, hb)
      with Tools.BrokenWeakRef _ -> (
        print_string "[GuaCaml/Hashcache.WakeUpConv.apply] BrokenWeakRef : begin"; print_newline();
        let res = apply_ifnotfound ttcache cache compute akey aakey index in
        print_string "[GuaCaml/Hashcache.WakeUpConv.apply] BrokenWeakRef : done"; print_newline();
        res
      )
    )
    | _ -> apply_ifnotfound ttcache cache compute akey aakey index

  let make ?(random = !randomized) a3aa b3bb initial_size =
    let cache = Module.create ~random:random initial_size in
    let ttcache = { cache; a2aa = fst a3aa; aa2a = snd a3aa; b2bb = fst b3bb; bb2b = snd b3bb } in
    (ttcache, apply ttcache)

  let dump_stats ttcache = Module.dump_stats ttcache.cache
end
