(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * BRLE : Boolean RLE
 *)

open Extra

type t = (bool * (int list)) option
type brle = t

module ToS =
struct
  open STools.ToS
  let t = option (bool * (list int))
  let brle = t
end

let empty : t = None
let single ((n, b):(int * bool)) : t =
  assert(n>=0);
  if n > 0 then (Some(b, [n])) else None

let length : t -> int = function
  | None -> 0
  | Some(_, l) -> MyList.sum l

let normalized : t -> bool = function
  | None -> true
  | Some (_, l) -> l <> [] && (List.for_all (fun i -> i > 0) l)

let rev : t -> t = function
  | None -> None
  | Some(b, l) -> Some(b = ((List.length l) mod 2 = 1), List.rev l)

let cons ((n, b):int * bool) (t:t) : t =
  assert(n >= 0);
  if n = 0 then t else (
    match t with
    | None -> Some(b, [n])
    | Some(b', t') -> (
      match t' with
      | [] -> (
        print_endline "[DAGaml.RLE_bool:cons] (non-consistent call)";
        Some(b, [n])
      )
      | n'::t'' ->
        if b' = b then Some(b, (n+n')::t'') else Some(b, n::t')
    )
  )

let uncons : t -> ((int * bool) * t) option =
  function
  | None -> None
  | Some (_, []) -> assert false
  | Some (b, n::l) -> (
    let t' = if l = [] then None else Some(not b, l) in
    Some((n, b), t')
  )

let push ?(n=1) (b:bool) (t:t) : t = cons (n, b) t

let bw_not : t -> t = function
  | None -> None
  | Some(b, l) -> Some(not b, l)

let bw_cnot (b:bool) : t -> t = function
  | None -> None
  | Some(b', l') -> Some(b <> b', l')

let rec rev_bw_and_rec (carry:t) (b1:bool) (l1:int list) (b2:bool) (l2:int list) : t =
  match l1, l2 with
  | [], [] -> carry
  | [], _ | _, [] -> assert false
  | n1::l1', n2::l2' ->
         if n1 = n2
    then
      (rev_bw_and_rec (cons (n1, b1 && b2) carry) (not b1) l1' (not b2) l2')
    else if n1 < n2
    then
      (rev_bw_and_rec (cons (n1, b1 && b2) carry) (not b1) l1' b2 ((n2-n1)::l2'))
    else (* n2 < n1 *)
      (rev_bw_and_rec (cons (n2, b1 && b2) carry) b1 ((n1-n2)::l1') (not b2) l2')

let rec rev_bw_xor_rec (carry:t) (b1:bool) (l1:int list) (b2:bool) (l2:int list) : t =
  match l1, l2 with
  | [], [] -> carry
  | [], _ | _, [] -> assert false
  | n1::l1', n2::l2' ->
         if n1 = n2
    then
      (rev_bw_and_rec (cons (n1, b1 <> b2) carry) (not b1) l1' (not b2) l2')
    else if n1 < n2
    then
      (rev_bw_and_rec (cons (n1, b1 <> b2) carry) (not b1) l1' b2 ((n2-n1)::l2'))
    else (* n2 < n1 *)
      (rev_bw_and_rec (cons (n2, b1 <> b2) carry) b1 ((n1-n2)::l1') (not b2) l2')

let bw_and (t1:t) (t2:t) : t =
  try
    match t1, t2 with
    | None, None -> None
    | None, _ | _, None -> assert false
    | Some(b1, l1), Some(b2, l2) -> rev(rev_bw_and_rec None b1 l1 b2 l2)
  with err -> (
    print_endline ("[GuaCaml.BRLE.bw_and] t1:"^(ToS.t t1));
    print_endline ("[GuaCaml.BRLE.bw_and] t2:"^(ToS.t t2));
    raise err
  )

let bw_or (t1:t) (t2:t) : t =
  bw_not(bw_and(bw_not t1)(bw_not t2))

let bw_xor (t1:t) (t2:t) : t =
  match t1, t2 with
  | None, None -> None
  | None, _ | _, None -> assert false
  | Some(b1, l1), Some(b2, l2) -> rev(rev_bw_xor_rec None b1 l1 b2 l2)

let rec rev_normalize_rec (carry:t) (b:bool) : int list -> t = function
  | [] -> carry
  | n::t' -> rev_normalize_rec (push ~n b carry) (not b) t'

let normalize : t -> t = function
  | None -> None
  | Some(b, l) -> rev(rev_normalize_rec None b l)

let of_rle : bool RLE.t -> t =
  function
  | [] -> None
  | ((nx, x)::_) as l -> Some(x, l ||> fst)

let rec rev_to_rle_rec (carry:bool RLE.t) (b:bool) : int list -> bool RLE.t =
  function
  | [] -> carry
  | n::l -> rev_to_rle_rec (RLE.cons (n, b) carry) (not b) l

let to_rle : t -> bool RLE.t =
  function
  | None -> []
  | Some(b, l) -> RLE.rev (rev_to_rle_rec [] b l)

let reduce (bl:bool list) : t =
  of_rle(RLE.reduce bl)

let rec rev_expand_append (carry:bool list) (b:bool) : int list -> bool list =
  function
  | [] -> carry
  | n::l -> rev_expand_append (MyList.ntimes ~carry b n) (not b) l

let rev_expand (t:t) : bool list =
  match t with
  | None -> []
  | Some(b, l) -> rev_expand_append [] b l

let expand (t:t) : bool list = rev_expand (rev t)

(* [LATER] explicit case disjunction, to avoid calling 'cons' *)
let rec rev_of_int_list_rec (carry:t) (length:int) (prev:int) : int list -> t =
  function
  | [] -> (
    assert(length-prev-1 >= 0);
    cons (length-prev-1, false) carry
  )
  | l0::l -> (
    assert(l0-prev-1 >= 0);
    let carry = cons (1, true) (cons (l0-prev-1, false) carry) in
    rev_of_int_list_rec carry length l0 l
  )

let of_int_list (length:int) (il:int list) : t =
  (* print_endline ("\n[GuaCaml.BRLE] length, il = "^(STools.ToS.(int * (list int)) (length, il))); *)
  let il = SetList.sort il in
  rev(rev_of_int_list_rec None length (-1) il)

(* [IMPROVE] improve performances by using straight induction, rather than unary conversion *)
let to_int_list (t:t) : int list =
  MyList.indexes_true (expand t)

let pull ?(n=1) (t:t) : bool * t =
  assert(n > 0);
  match t with
  | None -> assert false
  | Some(_, []) -> assert false
  | Some(b, nb::l) -> (
    assert(n <= nb);
    (b, if n = nb
      then (if l = [] then None else Some(not b, l))
      else (Some(b, (nb-n)::l))
    )
  )

let hdtl (t:t) : bool * t = pull ~n:1 t

(* [move head n tail = (head', tail')]
 *   picks the [n] first elements of [head] and put them backward on [tail]
 * - move_rec assumes that the head element of [head] differs from the head
     element of [tail]
 *)
let move (head:t) (n:int) (tail:t) : t * t =
  let x, y = RLE.move (to_rle head) n (to_rle tail) in
  (of_rle x, of_rle y)

let rev_append (l1:t) (l2:t) : t =
  of_rle (RLE.rev_append (to_rle l1) (to_rle l2))

let append (t1:t) (t2:t) : t =
  rev_append (rev t1) t2

let hdtl_nth (n:int) (t:t) : t * t =
  let head, tail = move t n None in
  (rev tail, head)

let rec count_rec (n:int) (b:bool) (l:int list) : int =
  match l with
  | [] -> n
  | head::tail -> (
    let n' = if b then n else (n+head) in
    count_rec n' (not b) tail
  )

let count (b:bool) = function
  | None -> 0
  | Some(b', l') -> (count_rec 0 (b <> b') l')

let count_true  l = count true  l
let count_false l = count false l

let forall (v:bool) : t -> bool =
  function
  | None -> v
  | Some(b, l) -> b = v && (match l with [ _ ] -> true | _ -> false)

module ToB =
struct
  open BTools.ToB
  let t = option (bool * (list int))
  let brle = t
end

module OfB =
struct
  open BTools.OfB
  let t = option (bool * (list int))
  let brle = t
end

module ToBStream =
struct
  open BTools.ToBStream
  let t = option (bool * (list int))
  let brle = t
end

module OfBStream =
struct
  open BTools.OfBStream
  let t = option (bool * (list int))
  let brle = t
end
