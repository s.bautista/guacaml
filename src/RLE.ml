(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * RLE : Run-Length Encoding
 *)

type 'a t = (int * 'a) list
type 'a rle = 'a t

module ToS =
struct
  open STools.ToS
  let rle a = list (int * a)
end

let rec normalized_rec (x:'a) : 'a t -> bool = function
  | [] -> true
  | (ny, y)::t -> ny >= 1 && x <> y && normalized_rec y t

let normalized : 'a t -> bool = function
  | [] -> true
  | (nx, x)::t -> nx >= 1 && normalized_rec x t

let rev (t:'a t) : 'a t = List.rev t

let rec rev_normalize_rec (carry:'a t) (nx:int) (x:'a) : 'a t -> 'a t =
  function
  | [] -> (nx, x)::carry
  | (ny, y)::t -> (
    if x = y
    then rev_normalize_rec carry (nx+ny) x t
    else rev_normalize_rec ((nx, x)::carry) ny y t
  )

let rev_normalize : 'a t -> 'a t =
  function
  | [] -> []
  | (nx, x)::t -> rev_normalize_rec [] nx x t

let normalize (t:'a t) : 'a t =
  rev(rev_normalize t)

let rec rev_reduce_rec (carry:'a t) (n:int) (x:'a) : 'a list -> 'a t =
  function
  | [] -> (n, x)::carry
  | h::t -> (
    if h = x
    then rev_reduce_rec carry (succ n) x t
    else rev_reduce_rec ((n, x)::carry) 1 h t
  )

let rev_reduce : 'a list -> 'a t =
  function
  | [] -> []
  | h::t -> rev_reduce_rec [] 1 h t

let reduce (l:'a list) : 'a t =
  rev(rev_reduce l)

let rec rev_expand_append (head:'a t) (carry:'a list) : 'a list =
  match head with
  | [] -> carry
  | (nu, u)::head' ->
    rev_expand_append head' (MyList.ntimes ~carry u nu)

let rev_expand (t:'a t) : 'a list =
  rev_expand_append t []

let expand (t:'a t) : 'a list =
  rev_expand_append (rev t) []

let rec rev_combine_rec (carry:('a * 'b)t) (la:'a t) (lb:'b t) : ('a * 'b) t =
  match la, lb with
  | [], [] -> carry
  | _, [] | [], _ -> assert false
  | (na, a)::la, (nb, b)::lb -> (
         if na = nb
    then rev_combine_rec ((na, (a, b))::carry) la lb
    else if na < nb
    then rev_combine_rec ((na, (a, b))::carry) la ((nb-na, b)::lb)
    else rev_combine_rec ((nb, (a, b))::carry) ((na-nb, a)::la) lb
  )

let rev_combine la lb = rev_combine_rec [] la lb

let combine (la:'a t) (lb:'b t) : ('a * 'b) t =
  rev_combine_rec [] (rev la) (rev lb)

let cons ((nu, u):(int * 'a)) (t:'a t) : 'a t =
  assert(nu >= 0);
  if nu = 0
  then t
  else match t with
    | (nv, v)::tail when u = v -> (nu+nv, v)::tail
    | t -> (nu, u)::t

let push ?(n=1) u t = cons (n, u) t

let pull ?(n=1) (t:'a t) : 'a * 'a t =
  assert(n > 0);
  match t with
  | [] -> assert false
  | (na, a)::t -> (
    assert(n <= na);
    (a, if na = n then t else ((na-n, a)::t))
  )

let hdtl : 'a t -> 'a * 'a t = function
  | [] -> (failwith "[GuaCaml.RLE] error")
  | (nx, x)::t ->
    if nx = 1
    then (x, t)
    else (x, (pred nx, x)::t)

(* [move head n tail = (head', tail')]
 *   picks the [n] first elements of [head] and put them backward on [tail]
 * - move_rec assumes that the head element of [head] differs from the head
     element of [tail]
 *)
let rec move_rec (head:'a t) (n:int) (tail:'a t) : 'a t * 'a t =
  match head with
  | [] -> assert(n = 0); (head, tail)
  | (nu, u)::head' -> (
         if nu > n
    then (((nu-n, u)::head'), ((n, u)::tail))
    else if nu = n
    then (head', (n, u)::tail)
    else (move_rec head' (n-nu) ((nu, u)::tail))
  )

let move (head:'a t) (n:int) (tail:'a t) : 'a t * 'a t =
  match head with
  | [] -> assert(n = 0); (head, tail)
  | (nu, u)::head' -> (
         if nu > n
    then (((nu-n, u)::head'), (cons (n, u) tail))
    else if nu = n
    then (head', cons (n, u) tail)
    else (move_rec head' (n-nu) (cons (nu, u) tail))
  )

(* [move_map map head n tail = (head', tail')]
 *   picks the [n] first elements of [head] and put them backward on [tail]
 *   after applying [map: int -> 'a -> 'b] the integer is given for informational
 *   purpose, and can be used via side effect in some cases
 *)
let rec move_map (f:int -> 'a -> 'b) (head:'a t) (n:int) (tail:'b t) : 'a t * 'b t =
  match head with
  | [] -> assert(n = 0); (head, tail)
  | (nu, u)::head' -> (
         if nu < n
    then (((nu-n, u)::head), ((n, f n u)::tail))
    else if nu = n
    then (head, (n, f n u)::tail)
    else (move_map f head' (n-nu) ((nu, f nu u)::tail))
  )

let rec rev_map_append (f:int -> 'a -> 'b) (head:'a t) (tail:'b t) : 'b t =
  match head with
  | [] -> tail
  | (nu, u)::head' -> (
    let tail' = cons (nu, f nu u) tail in
    rev_map_append f head' tail'
  )

let rev_map (f:int -> 'a -> 'b) (t:'a t) : 'b t =
  rev_map_append f t []

let map (f:int -> 'a -> 'b) (t:'a t) : 'b t =
  rev(rev_map f t)

let rev_append (l1:'a t) (l2:'a t) : 'a t =
  match l1 with
  | [] -> l2
  | (nu, u)::l1' ->
    List.rev_append l1' (cons (nu, u) l2)

let hdtl_nth (n:int) (t:'a t) : 'a t * 'a t =
  let head, tail = move t n [] in
  (rev tail, head)

let length (t:'a t) : int =
  let rec length_rec carry = function
    | [] -> carry
    | (n, _)::tail -> length_rec (carry+n) tail
  in length_rec 0 t

let split (p:('a * 'b)t) : 'a t * 'b t =
  let rp = rev p in
  let pa = rev_map (fun _ -> fst) rp in
  let pb = rev_map (fun _ -> snd) rp in
  (pa, pb)

let rec rev_unop_rec (carry:'a t) (l:'a option t) : 'a t =
  match l with
  | [] -> carry
  | (_, None)::l -> rev_unop_rec carry l
  | (nu, Some u)::l -> rev_unop_rec (cons (nu, u) carry) l

let rev_unop (l:'a option t) : 'a t = rev_unop_rec [] l

let unop (l:'a option t) : 'a t = rev(rev_unop_rec [] l)

module ToB =
struct
  open BTools.ToB
  let rle a = list (int * a)
end

module OfB =
struct
  open BTools.OfB
  let rle a = list (int * a)
end

module ToBStream =
struct
  open BTools.ToBStream
  let rle a = list (int * a)
  let sized_rle a = list (int * a)

  let rle_unit = rle unit
  let rle_bool = rle bool

  (* [LATER] improve *)
  let sized_rle_unit = sized_rle unit
  (* [LATER] improve *)
  let sized_rle_bool = sized_rle bool
end

module OfBStream =
struct
  open BTools.OfBStream
  let rle a = list (int * a)
end
