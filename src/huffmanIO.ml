(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * HuffmanIO : generic huffman encoding
 *)

open STools

module AdaptIdent =
struct
  open BTools
  type ident = int
  module BW =
  struct
    open ToBStream

    let mini_rle ?(size=None) top cha (l:int list) : unit =
      let rec aux0 c0 n0 = function
        | [] -> (n0, [])
        | h::t -> if h = c0
          then aux0 c0 (succ n0) t
          else (n0, (h::t))
      in
      let rec aux1 cN cC = function
        | [] -> (List.rev cN, List.rev cC)
        | h::t ->
          if h <= top
            then (
            let n, t = aux0 h 0 t in
            aux1 (n::cN) (h::cC) t
          ) else (
            aux1     cN  (h::cC) t
          )
      in
      let cN, cC = aux1 [] [] l in
      Channel.mpos ~s:(">CC> "^ToS.(list int cC)) cha;
      Channel.mpos ~s:(">CN> "^ToS.(list int cN)) cha;
      list int cha cC;
      sized_list int cha cN

    let bw_core cha (wl:(ident*int)list) : unit =
      let ids, wgs = List.split wl in
      let dids = MyList.delta'_int ids in
      if Tools.(mode = Profil) then
        Channel.mpos ~s:(">DID> "^ToS.(list int dids)) cha;
      mini_rle 8 cha dids;
      let wgs = List.map
        (fun x -> assert(x>0); pred x) wgs in
      mini_rle 8 cha wgs;
      if Tools.(mode = Profil) then
        Channel.mpos ~s:(">WGS> "^ToS.(list int wgs )) cha;
      ()

    let filter wl half zeros top curr =
      let mywl = !wl in
      let len = Array.length mywl in
      let rec aux carry i =
        if i < len then (
          let id, nb = Array.unsafe_get mywl i in
          if id <= top then
            if !nb > 0 then (
              let y = !nb in
              if id = curr then (decr nb);
              if !nb = 0 then (incr zeros);
              aux ((id = curr, y)::carry) (succ i)
            ) else (
              aux                  carry  (succ i)
            )
          else carry

        ) else carry
      in
      let return = aux [] 0 in
      if !zeros >= !half then (
        let newlen = Array.length mywl - !zeros in
        let newwl = Array.sub mywl 0 newlen in
        let j = ref 0 in
        for i = 0 to Array.length mywl - 1
        do
          let (id, nb) as x = Array.unsafe_get mywl i in
          if !nb > 0 then (
            Array.set newwl !j x;
            incr j;
          )
        done;
        assert(!j = newlen);
        wl := newwl;
        half := !half / 2;
        zeros := 0;
      );
      return

    let bw (cha:Channel.t) (wl:(ident*int)list) :
        (ident -> int bw) =
      let wl = List.sort Stdlib.compare wl in
      bw_core cha wl;
      let wl = ref (Array.map (fun (id, nb) -> assert(nb>=1); (id, ref nb)) (Array.of_list wl)) in
      let half = ref ((Array.length !wl)/2) in
      let zeros = ref 0 in
      let encode top cha curr =
        let sub = filter wl half zeros top curr in
        assert(List.exists fst sub);
        let code = HuffmanCoding.single_huffman sub in
        if Tools.(mode = Debug) then Channel.mpos ~s:"AHC" cha;
        sized_bool_list cha code;
        if Tools.(mode = Debug) then Channel.mpos ~s:"AHC!" cha;
      in
      encode

  end
  module BR =
  struct
    open OfBStream

    let mini_rld top cha : int list =
      let lC = list int cha in
      let s = MyList.count (fun h -> h <= top) lC in
      let lN = sized_list int s cha in
      let rec aux0 carry lC lN = match lC with
        | [] -> (assert(lN=[]); List.rev carry)
        | h::lC ->
          if h <= top
            then (match lN with
            | [] -> (failwith "[GuaCaml.HuffmanIO] error")
            | n::lN -> (
              let carry = MyList.ntimes ~carry h (succ n) in
              aux0 carry lC lN)
          ) else (aux0 (h::carry) lC lN)
      in
      aux0 [] lC lN

    let br_core cha : (ident*int) list =
      let dids = mini_rld 8 cha in
      let wgs  = mini_rld 8 cha in
      let ids = MyList.undelta'_int dids in
      let wgs = Tools.map succ wgs in
      List.combine ids wgs

    let filter wl half zeros top =
      let mywl = !wl in
      let len = Array.length mywl in
      let rec aux carry i =
        if i < len then (
          let id, nb = Array.unsafe_get mywl i in
          if id <= top then
            if !nb > 0 then (
              aux ((id, !nb)::carry) (succ i)
            ) else (
              aux             carry  (succ i)
            )
          else carry

        ) else carry
      in
      let return = aux [] 0 in
      if !zeros >= !half then (
        let newlen = Array.length mywl - !zeros in
        let newwl = Array.sub mywl 0 newlen in
        let j = ref 0 in
        for i = 0 to Array.length mywl - 1
        do
          let (id, nb) as x = Array.unsafe_get mywl i in
          if !nb > 0 then (
            Array.set newwl !j x;
            incr j;
          )
        done;
        assert(!j = newlen);
        wl := newwl;
        half := !half / 2;
        zeros := 0;
      );
      return

    let br (cha:Channel.t) : (ident -> int br) =
      let wl = br_core cha in
      let wl = ref (Array.map (fun (id, nb) -> assert(nb>0); (id, ref nb)) (Array.of_list wl)) in
      let half = ref ((Array.length !wl)/2) in
      let zeros = ref 0 in
      let decode top cha =
        let sub = filter wl half zeros top in
        match HuffmanCoding.huffmanize sub with
        | None -> (failwith "[GuaCaml.HuffmanIO] error")
        | Some btree -> (
          Channel.mpos ~s:"AHC" cha;
          let curr = btree_code btree cha in
          Channel.mpos ~s:"AHC!" cha;
          let curr_nb = MyArray.assoc curr !wl in
          decr curr_nb;
          curr
        )
      in
      decode
  end
end
