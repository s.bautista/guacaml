(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * UTree : Unique Trees
 *)

open STools
open BTools

(* Hashed-Cons Tables with Smart Identifier
    designed for 64-bits processor
    key : 63 bits = (hkey:32 bits) @ (uid:31 bits)
    each layer = 8 bits
    card layer = 4

  version 1 : >time and >memory than H2Table =(
 *)

external seeded_hash_param :
  int -> int -> int -> 'a -> int = "caml_hash" [@@noalloc]

let hash x = seeded_hash_param 10 100 0 x
let hash_param n1 n2 x = seeded_hash_param n1 n2 0 x
let seeded_hash seed x = seeded_hash_param 10 100 seed x

(* We do dynamic hashing, and resize the table and rehash the elements
   when buckets become too long. *)

type key = int

(* bucket list are sorted according to [hkey] *)
type bucket_tree =
  | Bucket of { mutable index : int;
                mutable array : barray array }
  | Empty
  | Node of bucket_tree array (* each node contains exactly 256 cells *)

type t = {
  mutable size : int;         (* number of entries *)
  mutable data : bucket_tree; (* the buckets *)
  mutable seed : int;         (* for randomization *)
}

(* To pick random seeds if requested *)

let randomized_default =
  let params =
    try Sys.getenv "OCAMLRUNPARAM" with Not_found ->
    try Sys.getenv "CAMLRUNPARAM" with Not_found -> "" in
  String.contains params 'R'

let randomized = ref randomized_default

let randomize () = randomized := true
let is_randomized () = !randomized

let prng = lazy (Random.State.make_self_init())

(* Creating a fresh, empty table *)

let create ?(random = !randomized) () =
  let seed = if random then Random.State.bits (Lazy.force prng) else 0 in
  { size = 0; data = Empty; seed }

let clear h =
  h.size <- 0; h.data <- Empty; ()

let rec copy_bucket_tree = function
  | Bucket {index; array} -> Bucket{index; array = Array.copy array}
  | Empty -> Empty
  | Node array -> Node(Array.map copy_bucket_tree array)

let copy h = { h with data = copy_bucket_tree h.data }

let length h = h.size

let hkey_mask = (1 lsl 32) -1
let index_mask = (1 lsl 31) -1
let split8_mask = (1 lsl 8) -1
let split8 h : int * int = (h land split8_mask, h lsr 8)

let hkey_of_item h item : int =
  (seeded_hash_param 10 100 h.seed item) land hkey_mask

let split_key key  : int * int =
  let hkey = key land hkey_mask
  and index = (key lsr 32) land index_mask in
  assert(hkey <= hkey_mask && index <= index_mask);
  (hkey, index)

let combine_key hkey index : int =
  assert(hkey <= hkey_mask && index <= index_mask);
  (hkey lor (index lsl 32))

let rec new_bucket_tree ?(d=4) hk ba =
  assert(d>=0);
  if d = 0
  then Bucket {index = 1; array = [|ba|]}
  else (
    let head, tail = split8 hk in
    let array = Array.make 256 Empty in
    array.(head) <- new_bucket_tree ~d:(pred d) tail ba;
    Node array
  )

let array_index (a:'a array) (x:'a) : int option =
  let rec loop i =
    if i >= 0
    then if Array.unsafe_get a i = x
      then Some i
      else loop (pred i)
    else None
  in loop (pred (Array.length a))

(* if required push an option which fail if [ba] is not in the table *)
let rec push_bucket_tree ?(d=4) hk ba = function
  | Bucket bucket -> (
    assert(d=0);
    let len = Array.length bucket.array in
    assert(len <= bucket.index);
    match array_index bucket.array ba with
    | Some index_ba -> (false, index_ba)
    | None -> (
      if len >= bucket.index
      then (
        let array' = Array.make (len lsl 1) bucket.array.(0) in
        Array.blit bucket.array 0 array' 0 len;
        bucket.array <- array';
      );
      let index_ba = bucket.index in
      bucket.index <- succ bucket.index;
      bucket.array.(index_ba) <- ba;
      (true, index_ba)
    )
  )
  | Empty -> (failwith "[GuaCaml.UTree] error")
  | Node array -> (
    let head, tail = split8 hk in
    if array.(head) = Empty
    then (array.(head) <- new_bucket_tree ~d:(pred d) tail ba; (true, 0))
    else (push_bucket_tree ~d:(pred d) tail ba array.(head))
  )

let internal_push h ba =
  let hk = hkey_of_item h ba in
  if h.data = Empty
  then (
    h.size <- 1;
    h.data <- new_bucket_tree hk ba;
    (false, combine_key hk 0)
  )
  else (
    let is_new, id = push_bucket_tree hk ba h.data in
    if is_new then h.size <- succ h.size;
    (is_new, combine_key hk id)
  )

let push h ba : int =
  internal_push h ba |> snd

let rec pull_bucket_tree ?(d=4) hk id = function
  | Bucket bucket -> (
    assert(d=0 && id < bucket.index);
    bucket.array.(id)
  )
  | Empty -> (failwith "[GuaCaml.UTree] error")
  | Node array -> (
    let head, tail = split8 hk in
    pull_bucket_tree ~d:(pred d) tail id array.(head)
  )

let pull h key =
  let hk, id = split_key key in
  pull_bucket_tree hk id h.data

let rec mapreduce_bucket_tree map red carry = function
  | Bucket {index; array} -> Array.fold_left (fun c x -> red (map x) c) carry array
  | Empty -> carry
  | Node array -> Array.fold_left (mapreduce_bucket_tree map red) carry array

let mapreduce h init map red = mapreduce_bucket_tree map red init h.data

module ToS =
struct
  open ToS

  let key = int
end

let string_of_key = ToS.key

module ToSTree =
struct
  open ToSTree

  let key = int

  let rec bucket_tree ?(d=4) = function
  | Bucket bucket -> (
    assert(d=0);
    array BArray.to_stree (Array.sub bucket.array 0 bucket.index);
  )
  | Empty  -> array (bucket_tree ~d:(pred d)) [||]
  | Node a -> array (bucket_tree ~d:(pred d)) a

  let t h : Tree.stree = Tree.Node [int h.size; bucket_tree h.data; int h.seed]
end

let key_to_stree : key Tree.to_stree = ToSTree.key
let to_stree (h:t) : Tree.stree = ToSTree.t h

module OfSTree =
struct
  open OfSTree

  let key = int

  let rec bucket_tree ?(d=4) s =
    if d = 0 then (
      let a = array BArray.of_stree s in
      let index = Array.length a in
      if index = 0 then Empty else (
      let size = Tools.power_2_above 1 index in
      let a' = Array.make size a.(0) in
      Array.blit a 0 a' 0 index;
      Bucket {index; array = a'}
    ))
    else (
      let a = array (bucket_tree ~d:(pred d)) s in
      let n = Array.length a in
      assert(n = 0 || n = 256);
      if n = 0 then Empty else Node a
    )

  let t s =
    match s with
    | Tree.Node[size; data; seed] -> (
      let size = int size
      and data = bucket_tree data
      and seed = int seed in
      {size; data; seed}
    )
    | _ -> (failwith "[GuaCaml.UTree] error")
end

let key_of_stree : key Tree.of_stree = OfSTree.key
let of_stree s : t = OfSTree.t s

module ToBStream =
struct
  open ToBStream
  let key cha key =
    let hk, id = split_key key in
    sized_int 32 cha hk;
    int cha id

  let rec bucket_tree ?(destruct=false) ?(d=4) cha = function
  | Bucket bucket -> (
    assert(d=0);
    array barray cha (Array.sub bucket.array 0 bucket.index);
    (if destruct then bucket.array <- [||]);
  )
  | Empty  -> array (bucket_tree ~d:(pred d)) cha [||]
  | Node a -> (
    array (bucket_tree ~d:(pred d)) cha a;
    (if destruct then Array.fill a 0 256 Empty);
  )

  let t ?(destruct=false) cha h =
    int cha h.size;
    bucket_tree ~destruct cha h.data;
    sized_int 63 cha h.seed;
    ()
end

module OfBStream =
struct
  open OfBStream
  let key cha =
    let hk = sized_int 32 cha in
    let id = int cha in
    combine_key hk id

  let rec bucket_tree ?(d=4) cha =
    if d = 0 then (
      let a = array barray cha in
      let index = Array.length a in
      if index = 0 then Empty else (
      let size = Tools.power_2_above 1 index in
      let a' = Array.make size a.(0) in
      Array.blit a 0 a' 0 index;
      Bucket {index; array = a'}
    ))
    else (
      let a = array (bucket_tree ~d:(pred d)) cha in
      let n = Array.length a in
      assert(n = 0 || n = 256);
      if n = 0 then Empty else Node a
    )

  let t cha =
    let size = int cha in
    let data = bucket_tree cha in
    let seed = sized_int 63 cha in
    {size; data; seed}
end

let bw_key = ToBStream.key
let br_key = OfBStream.key

let bw ?(destruct=false) cha h = ToBStream.t ~destruct cha h
let br cha = OfBStream.t cha

module ToB =
struct
  open ToB
  let key key stream =
    pair (sized_int 32) int (split_key key) stream
end

let tob_key : key tob = ToB.key

module OfB =
struct
  open OfB
  let key stream =
    let (hk, id), stream = pair (sized_int 32) int stream in
    (combine_key hk id, stream)
end

let ofb_key : key ofb = OfB.key

let iob_key = (tob_key, ofb_key)
