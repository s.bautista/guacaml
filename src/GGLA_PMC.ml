open Extra
module GGLA = GraphGenLA
open GGLA.Type
open GraphHFT
open GraphHFT.Type

(* [??] similar to GraphGenLA.Connected *)

type seen_tag =
  | InK        (* Tags a node to be ignored by the search of connected components *)
  | NotVisited (* Tags a node that has not been visited yet *)
  | Visited    (* Tags a node that has been visited *)

(* [PRIVATE]
   [explore hg seen i = (component, neighbors)] *)
(* Depth first search to identify the connected component of i
        [explore(self, seen, i, component, neighbors) = component, neighbours]
        returns the connected component of i and its neighbors in K, as encoded in seen
  ensures :
  - [SetList.sorted component]
  - [SetList.sorted neighbors]
 *)
let explore (hg:hg) (seen:seen_tag array) (i:int) : int list * int list =
  let component = ref [] in
  let neighbors = ref [] in
  let rec explore_rec (i:int) : unit =
      (* Visits node i, stacks node i into the current connected component *)
    seen.(i) <- Visited;
    stack_push component i;
    (* Visits the neighbors of i *)
    List.iter
      (fun (node, ()) ->
        match seen.(node) with
        (* Ignores nodes that are already visited *)
        | Visited    -> ()
        (* Does not visit K nodes but stores them as neighbors *)
        | InK        -> stack_push neighbors node
        (* Recursively visits other unseen nodes *)
        | NotVisited -> explore_rec node
      ) hg.(i).edges;
  in
  explore_rec i;
  (* Returns the connected component of i and the set of neighbors in K *)
  (SetList.sort !component, SetList.sort !neighbors)

(* [connected_components_exclusion hg idents = [(component, excluded, is_full)]]
    (the notion of non-suppressible node is ignore for computation)
    [TODO] [??] deal with ft-vertices
        (* [??] G = (V u L, E, w) an H-FT-Graph
        we say that K \subset V is an MKVM of G iff
        K u L is an MKVM of G' = (V u L, E u L^2, w) an H-Graph *)
    (we assume [SetList.sorted idents])
    returns a list of triplet, each triplet correspond to one connected component of G\K :
    1. component : list of vertices C
    2. excluded : set S of vertices of K which are adjacent to C
    3. is_full : 'True' iff the component is full (i.e., S = K) *)
let connected_components_exclusion
    (hg:hg)
    (idents:int list) : (int list * int list * bool) list =
  assert(SetList.sorted idents);
  let len = Array.length hg in
  (* Inits nodes' status *)
  let seen = Array.make len NotVisited in
  List.iter (fun i -> seen.(i) <- InK) idents;
  let all_comp = ref [] in
  (* Visits every unseen node in the graph *)
  Array.iteri
    (fun i v ->
      match seen.(i) with
      | Visited | InK -> ()
      | NotVisited -> (
        let component, neighbors = explore hg seen i in
        let is_full = neighbors = idents in
        stack_push all_comp (component, neighbors, is_full)
      )
    ) hg;
  let all_comp = List.rev !all_comp in
  OUnit.print_error_endline (fun()->"[connected_components_exclusion] all_comp:"^(STools.ToS.(list(trio (list int) (list int) bool)) all_comp));
  all_comp

(* [is_pmc hg idents = bool]
    returns true iff idents(=K) is a pmc in the graph hg(=G) *)
let is_pmc (hg:hg) (idents:int list) : bool =
  let components = connected_components_exclusion hg idents in
  (* 1. G\K has no full component associated to K *)
  (List.for_all (fun (_, _, is_full) -> not is_full) components) &&
  (* 2. K is cliquish *)
  (let g' : hg = List.fold_left
      (fun g' (_, neighbors, _) ->
        GGLA.Utils.graph_add_clique
          ~inplace:true
          ~replace:true
          ~reflexive_false:true
           g'
        (neighbors ||> (fun j -> (j, ()))))
      (Array.copy hg)
      components
    in
    GGLA.Utils.graph_is_clique g' idents)
