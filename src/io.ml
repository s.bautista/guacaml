(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Io : type-based file-based IO facility
 *)

module ToF =
struct
  type stream = out_channel
  type 'a t = stream -> 'a -> unit

  let bool (cha:stream) (b:bool) : unit =
    output_byte cha (if b then 1 else 0)

  let char (cha:stream) (c:char) : unit =
    output_char cha c

  let int (cha:stream) (n:int) : unit =
    output_binary_int cha n;
    output_binary_int cha (n lsr 32)

  let unit (cha:stream) () : unit = ()

  let option (tof:'a t) (cha:stream) = function
    | None -> output_byte cha 0
    | Some a -> (output_byte cha 1; tof cha a)

  let pair (tofa:'a t) (tofb:'b t) (cha:stream) (a, b) = tofa cha a; tofb cha b
  let ( * ) = pair
  let trio (tofa:'a t) (tofb:'b t) (tofc:'c t) (cha:stream) (a, b, c) = tofa cha a; tofb cha b; tofc cha c

  let rec list (tof:'a t) (cha:stream) = function
    | [] -> output_byte cha 0
    | hd::tl -> output_byte cha 1; tof cha hd; list tof cha tl

  let sized_array (tof:'a t) (cha:stream) (a:'a array) : unit =
    Array.iter (fun x -> tof cha x) a

  let array (tof:'a t) (cha:stream) (a:'a array) : unit =
    int cha (Array.length a);
    sized_array tof cha a

  let sized_bytes (cha:stream) (b:bytes) : unit =
    output_bytes cha b

  let bytes (cha:stream) (b:bytes) : unit =
    int cha (Bytes.length b);
    sized_bytes cha b

  let string (cha:stream) (s:string) : unit =
    int cha (String.length s);
    output_string cha s
end

module OfF =
struct
  type stream = in_channel
  type 'a t = stream -> 'a

  let bool (cha:stream) : bool =
    match input_byte cha with
    | 0 -> false
    | 1 -> true
    | _ -> (failwith "[GuaCaml.Io] error")

  let char (cha:stream) : char =
    input_char cha

  let int (cha:stream) : int =
    let hd = input_binary_int cha in
    let tl = input_binary_int cha in
    (hd lor (tl lsl 32))

  let unit (cha:stream) : unit = ()

  let option (off:'a t) (cha:stream) : 'a option =
    match input_byte cha with
    | 0 -> None
    | 1 -> Some(off cha)
    | _ -> (failwith "[GuaCaml.Io] error")

  let pair (offa:'a t) (offb:'b t) (cha:stream) =
    let a = offa cha in
    let b = offb cha in
    (a, b)

  let ( * ) = pair

  let trio (offa:'a t) (offb:'b t) (offc:'c t) (cha:stream) =
    let a = offa cha in
    let b = offb cha in
    let c = offc cha in
    (a, b, c)

  let list (off:'a t) (cha:stream) : 'a list =
    let rec loop carry off cha =
      match input_byte cha with
      | 0 -> List.rev carry
      | 1 -> (
        let a = off cha in
        loop (a::carry) off cha
      )
      | _ -> (failwith "[GuaCaml.Io] error")
    in
    loop [] off cha

  let sized_array (off:'a t) (len:int) (cha:stream) : 'a array =
    Array.init len (fun _ -> off cha)

  let array (off:'a t) (cha:stream) : 'a array =
    let n = int cha in
    sized_array off n cha

  let sized_bytes (len:int) (cha:stream) : bytes =
    let b = Bytes.create len in
    really_input cha b 0 len;
    b

  let bytes (cha:stream) : bytes =
    let n = int cha in
    sized_bytes n cha

  let string (cha:stream) : string =
    let n = int cha in
    really_input_string cha n

end
