(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Io : type-based file-based IO facility
 *)

module ToF :
sig
  type stream = out_channel
  type 'a t = stream -> 'a -> unit

  val unit : unit t
(** [unit] writter **)
  val bool : bool t
(** [bool] writter **)
  val char : char t
(** [cha] writter **)
  val int  : int t
(** [int] writter for positive integers *)
  val string : string t
(** [string] writter *)
  val sized_bytes : bytes t
  val bytes : bytes t
(** [Bytes] writter **)
  val option : 'a t -> 'a option t
(** [option bw'a] return a ['a option] writter using the ['a] writter [bw'a]
 **)
  val pair : 'a t -> 'b t -> ('a * 'b) t
(** [pair bw'a bw'b] returns ['a * 'b] writter using the ['a] writter [bw'a] and the ['b] writter [bw'b]
 **)
  val ( * ) : 'a t -> 'b t -> ('a * 'b) t
(** ( * ) alias for pair **)
  val trio : 'a t -> 'b t -> 'c t -> ('a * 'b * 'c) t
(** [trio bw'a bw'b bw'c] returns a ['a * 'b * 'c] writter using the ['a] writter [bw'a], the ['b] writter [bw'b] and the the ['c] writter [bw'c]
 **)
  val list : 'a t -> 'a list t
(** [list bw'a] returns a ['a list] writter using the ['a] writter [bw'a]
 **)
  val sized_array : 'a t -> 'a array t
(** [array bw'a] returns a ['a array] writter using the ['a] writter [bw'a]
 **)
  val array : 'a t -> 'a array t
(** [array bw'a] returns a ['a array] writter using the ['a] writter [bw'a]
 **)
end

module OfF :
sig
  type stream = in_channel
  type 'a t = stream -> 'a

  val bool : bool t
(** [bool] writter **)
  val char : char t
(** [cha] writter **)
  val int  : int t
(** [int] writter for positive integers *)
  val string : string t
(** [string] writter *)
  val sized_bytes : int -> bytes t
  val bytes : bytes t
(** [Bytes] writter **)
  val option : 'a t -> 'a option t
(** [option bw'a] return a ['a option] writter using the ['a] writter [bw'a]
 **)
  val unit : unit t
(** [unit] writter
 **)
  val pair : 'a t -> 'b t -> ('a * 'b) t
(** [pair bw'a bw'b] returns ['a * 'b] writter using the ['a] writter [bw'a] and the ['b] writter [bw'b]
 **)
  val ( * ) : 'a t -> 'b t -> ('a * 'b) t
(** ( * ) alias for pair **)
  val trio : 'a t -> 'b t -> 'c t -> ('a * 'b * 'c) t
(** [trio bw'a bw'b bw'c] returns a ['a * 'b * 'c] writter using the ['a] writter [bw'a], the ['b] writter [bw'b] and the the ['c] writter [bw'c]
 **)
  val list : 'a t -> 'a list t
(** [list bw'a] returns a ['a list] writter using the ['a] writter [bw'a]
 **)
  val sized_array : 'a t -> int -> 'a array t
(** [list bw'a] returns a ['a list] writter using the ['a] writter [bw'a]
 **)
  val array : 'a t -> 'a array t
(** [list bw'a] returns a ['a list] writter using the ['a] writter [bw'a]
 **)
end
