(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Extra : Simple Extension to OCaml's Standard Short Notations
 *)

let flip f x y = f y x
let ( <| ) x y = x y
let ( >> ) f g x = g ( f x )
let ( << ) g f x = g ( f x )
let ( ||> ) l f = List.rev_map f l |> List.rev
let ( <|| ) f l = List.rev_map f l |> List.rev
let ( |+ ) l f x = List.fold_left f x l
let ( ||>> ) f g x = List.rev_map g (f x) |> List.rev
let  (+=) x y = x:=!x+y
let  (-=) x y = x:=!x-y
let (+.=) x y = x:=!x+.y
let (-.=) x y = x:=!x-.y
let (!++) x = let x' = !x in x := succ x'; x'
let (!--) x = let x' = !x in x := pred x'; x'
(* [LATER] *)
let array_push a i x = a.(i) <- x :: a.(i)
let stack_push l x = l := x :: !l
let stack_pull (l:'a list ref) : 'a =
  match !l with
  | [] -> (failwith "[GuaCaml.Extra] error")
  | hd :: tl  -> (l := tl; hd)
(* [LATER]
let stack_push_list (stack: 'a list ref) (liste: 'a list) =
  List.iter (fun v -> stack_push stack(Hashtbl.find vars v)) liste
 *)
