(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * H2Table : efficient implementation of unique tables for hash-consing
 *)

type 'a t
val default_size : int
(**
create hsize index
@ hsize : the size of the hash-table
@ index : index of the first element
**)
val default_size : int
val create : int -> int -> 'a t
val memA : 'a t -> 'a -> bool
val memI : 'a t -> int -> bool
val push : 'a t -> 'a -> int
(* same as [push] but raise an error if the object does not exists *)
val push' : 'a t -> 'a -> int
val pull : 'a t -> int -> 'a
val length : 'a t -> int
val iter : 'a t -> ('a -> int -> unit) -> unit
val map  : 'a t -> (int -> 'a -> 'b  ) -> 'b list
val mapreduce : 'a t -> 'c -> (int -> 'a -> 'b) -> ('b -> 'c -> 'c) -> 'c

val to_stree :        'a Tree.to_stree -> 'a t Tree.to_stree
val of_stree : int -> 'a Tree.of_stree -> 'a t Tree.of_stree

open BTools

val bw : ?destruct:bool -> 'a bw -> 'a t bw
val br : ?hsize:int     -> 'a br -> 'a t br

(* [keep_clean t il] where [SetList.sorted ~strict:true il = true]
   undefined behaviour if [il] are not valid identifiers *)
val keep_clean : 'a t -> int list -> unit

module ToF :
sig
  val h2table : 'a Io.ToF.t -> 'a t Io.ToF.t
end

module OfF :
sig
  val h2table : ?hsize:int -> 'a Io.OfF.t -> 'a t Io.OfF.t
end
