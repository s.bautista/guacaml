(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * UTree : Unique Trees
 *)

open STools
open BTools

(* Hashed-Cons Tables with Smart Identifier
    designed for 64-bits processor
    key : 63 bits = (hkey:32 bits) @ (uid:31 bits)
    each layer = 8 bits
    card layer = 4

  version 1 : >time and >memory than H2Table =(
*)

val seeded_hash_param : int -> int -> int -> 'a -> int

type key

type t

val randomize : unit -> unit
val is_randomized : unit -> bool

val create : ?random:bool -> unit -> t
val clear : t -> unit

val copy : t -> t
val length : t -> int

val internal_push : t -> barray -> bool * int
val push : t -> barray -> key

val pull : t -> key -> barray

val string_of_key : key -> string

val key_to_stree : key Tree.to_stree
val to_stree : t Tree.to_stree

val key_of_stree : key Tree.of_stree
val of_stree : t Tree.of_stree

val bw_key : key bw
val bw : ?destruct:bool -> t bw

val br_key : key br
val br : t br

val tob_key : key tob
val ofb_key : key ofb
val iob_key : key iob

val mapreduce : t -> 'c -> (barray -> 'b) -> ('b -> 'c -> 'c) -> 'c
