(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * PriorityQueue : implementation of priority queues based on standard
 *   [Map] module
 *
 * === CREDIT ===
 *
 * signature imported from : https://github.com/harnold/ocaml-base.git]
 * the signature is meant to be integrated into GuaCaml
 * the implementation is based on the the Map module of the standard library
 *
 * === TODO ===
 *
 *   1) add a Stdlib-friendly version of the interface with type ('k, 'v) t
 *   2) dd a module-less version of the interface, very handy in some cases
 *)

(* [definition of Map.OrderedType]
module type OrderedType =
  sig
    type t
    val compare : t -> t -> int
    (* a total order over keys, such as Stdlib.compare *)

  end
 *)

(* TODO : add a Stdlib-friendly version of the interface with type ('k, 'v) t *)
(* TODO : add a module-less version of the interface, very handy in some cases *)

module type S =
sig
  type key

  type 'v t (* 'v being the type of values *)

  val empty : unit -> 'v t
  (** [empty () ] creates a new empty priority queue *)

  val length: 'v t -> int
  (** Returns the number of elements in a queue. *)

  val is_empty: 'v t -> bool
  (** Tests whether a queue is empty. *)

  val add: 'v t -> key -> 'v -> unit
  (** Adds an elements to the queue. *)

(*
  val mem: 'v t -> 'v -> bool
  (** Tests whether a queue contains a given element. *)

  val get_key: 'v t -> 'v -> key
  (** [get_key q v] returns the smallest key associated with the value. *)
 *)

  val first: 'v t -> key * 'v
  (** [first q] returns an element with higher priority contained in the queue
      [q].

      @raise Not_found if [q] is empty. *)

  val remove_first: 'v t -> unit
  (** [remove_first q] removes the element returned by [first q] from the queue
      [q].

      @raise Not_found if [q] is empty. *)

  val pull_first : 'v t -> key * 'v
  (** [pull_first q] returns an element with higher priority contained in the
      queue [q] and removes it from [q].

      let pull_first q =
          let obj = first q in
          remove_first q;
          obj

      @raise Not_found if [q] is empty. *)

  val pull_first_opt : 'v t -> (key * 'v) option
  (** [pull_first q] returns an element with higher priority contained in the
      queue [q] and removes it from [q].

      let pull_first_opt q =
        try
          Some(pull_first q)
        with
          | Not_found -> None
    *)

  val pull_firsts : 'v t -> key * ('v list)
  (** [pull_firsts q] returns all elements with higher priority contained in the
      queue [q] and removes them from [q]

      [List.length (snd pull_firsts) > 0]

      @raise Not_found if [q] is empty. *)

(*
  val remove: 'v t -> 'v -> unit
  (** [remove q x] removes the element [x] from the queue [q].  If [q] does not
      contain the element [x], the function does nothing. *)
 *)

  val clear: 'v t -> unit
  (** Removes all elements from a queue. *)

  val shrink : 'v t -> key -> unit
  (** Removes all elements with a priority lower (inclusive) than key. *)

  val to_stree :
    (key -> Tree.stree) ->
    ('v  -> Tree.stree) ->
    'v t -> Tree.stree
  val of_stree :
    (Tree.stree -> key) ->
    (Tree.stree -> 'v ) ->
    Tree.stree -> 'v t

  val iter: (key -> 'a -> unit) -> 'a t -> unit
  (** [iter f m] applies [f] to all bindings in map [m].
       [f] receives the key as first argument, and the associated value
       as second argument.  The bindings are passed to [f] in increasing
       order with respect to the ordering over the type of the keys.
   **)

(*
  val reorder_up: 'a t -> 'a -> unit
  (** [reorder_up q x] notifies the queue [q] that the priority of the element
      [x] has increased.  If [q] does not contain [x], the function does
      nothing. *)

  val reorder_down: 'a t -> 'a -> unit
  (** [reorder_down q x] notifies the queue [q] that the priority of the element
      [x] has decreased.  If [q] does not contain [x], the function does
      nothing. *)
*)

(* TODO : add serialization/conversion interface *)
(* TODO : add archive collection *)

end

module MakeMax(Ord:Map.OrderedType) : S
  with type key = Ord.t
(* version where piority is defined according to [OT.compare], i.e. :
   - values with higher keys have higher priority
   - *first* return value(s) with maximal key
 *)

module MakeMin(Ord:Map.OrderedType) : S
  with type key = Ord.t
(* version where piority is defined according to [- OT.compare], i.e. :
   - values with lower keys have higher priority
   - *first* return value(s) with minimal key
 *)

(* TODO : add a version with user-transparent compression of elements of values *)
(* TODO : add a version with weak-refered values *)
