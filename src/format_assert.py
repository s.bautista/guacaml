import sys
import os

file_in = sys.argv[1]
file_out = sys.argv[2]

FILE = open(file_in)
TEXT = list(FILE.readlines())
FILE.close()

file_dir, file_name = os.path.split(file_in)
file_core, file_ext = tuple(file_name.split('.'))

def ModuleCase(s):
    assert(len(s) > 0)
    return s[0].upper() + s[1:]

module_name = ModuleCase(file_core)

MLPACK = "GuaCaml"

if file_ext != "ml" :
    print("not an .ml file : "+file_name)
    exit()
if '_build' in file_dir:
    print("file in _build/ : "+file_dir)
    exit()
if '.git' in file_dir :
    print("file in .git/ : "+file_dir)
    exit()

print("file_in   :"+file_in)
print("file_dir  :"+file_dir)
print("file_name :"+file_name)
print("file_core :"+file_core)
print("file_ext  :"+file_ext)
print("module    :"+module_name)


def format_line(i, line):
    if 'assert' in line :
        if 'assert false' :
            return line.replace('assert false', '(failwith "['+MLPACK+'.'+module_name+'] error")')
        else:
            print('['+str(i)+'] '+line, end='')
            return line
    else:
        return line

TEXT2 = [ format_line(i, line) for i, line in enumerate(TEXT)]

FILE = open(file_out, 'w')
FILE.write(''.join(TEXT2))
FILE.close()
