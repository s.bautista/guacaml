(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraphAA : adjacency-array based representation of directed graphs
 *)

type graph = {
(* List of valid nodes *)
  nodes : int list;
(* adjacence matrix *)
  edges : bool Matrix.matrix;
}

let la_of_aa aa =
  let nodes = aa.nodes in
  let funmap a = a |> Array.to_list |> MyList.indexes (fun x -> x) in
  let edges = Array.map funmap aa.edges.Matrix.a in
  GraphLA.{nodes; edges}

let aa_of_la la =
  let nodes = la.GraphLA.nodes in
  let size = Array.length la.GraphLA.edges in
  let edges = Matrix.make (size, size) false in
  Array.iteri (fun i liste ->
    List.iter (fun j -> Matrix.set edges (i, j) true) liste
  ) la.GraphLA.edges;
  {nodes; edges}

let make n b = {nodes = MyList.init n (fun i -> i); edges = Matrix.make (n, n) b}

let set graph edge = Matrix.set graph.edges edge true
let get graph edge = Matrix.get graph.edges edge

let closure graph = {
  nodes = graph.nodes;
  edges = Matrix.expmat  ( || ) ( && ) false true graph.edges (List.length graph.nodes);
}

type 'a o3graph = {
  dump : 'a -> int;
  load : int -> 'a;
  graph : graph;
}

let o3make dump load n b = {dump; load; graph = make n b}

let o3set graph (x, y) = set graph.graph (graph.dump x, graph.dump y)
let o3get graph (x, y) = get graph.graph (graph.dump x, graph.dump y)

let o3get_bidir graph (x, y) = (o3get graph (x, y)) && (o3get graph (y, x))

let o3closure graph = {
  dump = graph.dump;
  load = graph.load;
  graph = closure graph.graph;
}
