(* Work In Progress *)
(** Branch And Bound
  * - vanilla version
  * - without memoization
  **)

(* full proof *)
type ('a, 'd) proof = 'a list

(* partial proof (cost annoted) *)

type ('a, 's, 'c) pproof =
  | PEnd
  | PNxt of 's
  | PSeq of 'a list * 'c * 's * ('a, 'd, 's, 'c) pproof
  | PMin of 's * ('a, 'd, 's, 'c) pproof list

let rec pproof_normalize add = function
  | PEnd -> PEnd
  | PNxt s -> PNxt s
  | PSeq ([], _, _, p) -> pproof_normalize add p
  | PSeq (al, c, s, p) ->
  ( match pproof_normalize add p with
    | PSeq (al', c', s', p') -> PSeq(al@al', add c c', s', p')
    | p -> PSeq(al, c, s, p)
  )
  | PMin (s, []) -> assert false
  | PMin (s, [pp]) -> pproof_normalize add pp
  | PMin (s, ppl) ->
    PMin (s, List.map (pproof_normalize add) ppl)

type ('a, 'd) aproof =
  | AEnd
  | ANxt
  | ASeq of 'a list
  | AMin

let aproof_of_pproof = function
  | PEnd               -> AEnd
  | PNxt _             -> ANxt
  | PSeq (al, _, _, _) -> ASeq al
  | PSum (d , _, _)    -> ASum d
  | PMin (_ , _)       -> AMin

module type MSig =
sig
(* A.1] type of elements *)
  type s (* state : the type of intermediate states *)
    (* [FIXME] standard operation on state elements must be consistent
        e.g. Stdlib.compare, Hashtbl.hash *)
  type a (* non branching actions : the type of actions *)
  type d (* branching actions, aka divider actions *)
  type c (* cost : the type of cost *)

(* A.2] type of proofs *)
  type p  = (a, d)        proof
  type pp = (a, d, s, c) pproof
  type ap = (a, d)       aproof

(* B] type of operators *)
  val lower : s -> c
  val upper : s -> p * c
  val next  : s -> pp
  (* do not use empty transition from Min to Min *)

  val cost_add : c -> c -> c
    (* [cost_add c1 c2 = c3] st. [c3] = [c1] + [c2] *)
  val cost_sub : c -> c -> c
    (* [cost_sub c1 c2 = c3] st. [c3] = [c1] - [c2] *)
  val cost_cmp : c -> c -> int
    (* comparison operator on [c] *)
  val cost_zero : unit -> c
    (* returns a neutral cost element *)

(* C] action application operators *)
  val do_a : s -> a -> c * s
  val do_d : s -> d -> c * (s list)

end

module type Sig =
sig
  module H : MSig

  type t

  val newman : unit -> t

  val solve : t -> H.s -> H.c * H.p
  (* val dump_stats : t -> Tree.stree *)
end

module Make(H0:MSig) : Sig
  with module H = H0
  =
struct
  module H = H0

  type ident = int

  type alive =
    | NotVisited
    | Visited
    | Solved

  type tnxt = {
    tnxt_proof : H.p;
  }

  type tseq = {
    tseq_next  : ident;
    tseq_actions : H.a list;
    tseq_actions_cost : H.c;
  }

  module CostCmp : Map.OrderedType
    with type t = H.c =
  struct
    type t = H.c
    let compare = H.cost_cmp
  end

  module CostPQMax = PriorityQueue.MakeMax(CostCmp)
  module CostPQMin = PriorityQueue.MakeMin(CostCmp)

  type tmin = {
    mutable tmin_upper  : ident;
      (* child-node which has the lowest upper bound *)
            tmin_queue  : ident CostPQMin.t;
              (* parameter : node(ident).lower *)
  }

  type tsum = {
            tsum_divider : H.d;
            tsum_ccost   : H.c; (* constant cost for this divider *)
            tsum_queue   : (ident * int) CostPQMax.t;
              (* parameter : node(ident).diff *)
    mutable tsum_solved  : (ident * int) list;
  }

  (* Q? : possible state collision with [TMin], [TSeq], [TSum]
      A! : First encountered node takes priority other subsequent ones
   *)

  type sstate =
    | TEnd
    | TNxt of tnxt
    | TSeq of tseq
    | TSum of tsum
    | TMin of tmin

  type snode = {
            ident : ident; (* unique identifier *)
            state : H.s;
    mutable alive : alive; (* Visited | NotVisited | Solved *)
    mutable lower : H.c;
    mutable upper : H.c;
    mutable diff  : H.c;
    mutable sstate: sstate;
  }

  type t = {
            memory : (ident, snode) Hashtbl.t;
  (* The [t.unique] table ensures that each intermediate state
       has at most one identifier.
     NB : Because of evaluation and solving an identifier may
       may represent several states, which therefore have the
       exact same set of optiman solution
   *)
            unique : (H.s * H.ap, ident) Hashtbl.t;
    mutable index : int;
  }

  let newman () = {
    memory = Hashtbl.create 10_000;
    unique = Hashtbl.create 10_000;
    index  = 0;
  }

  let zero3 () =
    H.(cost_zero(), cost_zero(), cost_zero())

  let add3 (l0, d0, u0) (l1, d1, u1) =
    H.(cost_add l0 l1,
       cost_add d0 d1,
       cost_add u0 u1)

  let sub3 (l0, d0, u0) (l1, d1, u1) =
    H.(cost_sub l0 l1,
       cost_sub d0 d1,
       cost_sub u0 u1)

  (* [WIP] *)
  let rec add_pproof (t:t) (s:H.s) ?sednxt (pp:H.pp) : snode =
    (* print_string "add_pproof t s ?sednxt pp"; print_newline(); *)
    let ap = aproof_of_pproof pp in
    (* We check if this intermediate state has already
         been encountered *)
    match Hashtbl.find_opt t.unique (s, ap) with
    | Some ident -> (
      (* print_string "\told state"; print_newline(); *)
      (* Yes, this intermediate state has already been seen *)
      Hashtbl.find t.memory ident
    )
    | None -> (
      (* No, this intermediate is not recorded yet in the memory *)
      (* print_string "\tnew state"; print_newline(); *)
      let ident : ident = match sednxt with
        | Some ident -> ident
        | None -> (
          let ident = t.index in
          t.index <- succ t.index;
          Hashtbl.remove t.memory ident;
          (* print_string "t.index : "; print_int t.index; print_newline(); *)
          ident
        )
      in
      Hashtbl.add t.unique (s, ap) ident;
      let snode : snode = match_pproof t ident s pp in
      Hashtbl.add t.memory ident snode;
      snode
    )
  and    match_pproof (t:t) (ident:ident) (state:H.s) (pp:H.pp) : snode =
    (* print_string "math_pproof t (ident:"; print_int ident; print_string ") state pp"; print_newline(); *)
    match pp with
    (* The programm should go only once in this branch *)
    | PEnd -> {
      ident; state;
      alive = Solved;
      lower = H.cost_zero();
      upper = H.cost_zero();
      diff  = H.cost_zero();
      sstate = TEnd;
    }
    (* Leaf node of the partial proof, indicates where the computation has been stopped *)
    | PNxt state' -> (
      assert(state = state');
      let tnxt_proof, upper = H.upper state in
      let lower = H.lower state in
      {
        ident; state;
        alive = NotVisited;
        lower; upper;
        diff  = H.cost_sub upper lower;
        sstate = TNxt {tnxt_proof};
      }
    )
    | PSeq (tseq_actions, tseq_actions_cost, state', nxt) ->
    (
      let snode' = add_pproof t state' nxt in
      {
        ident; state; alive = Visited;
        lower = H.cost_add tseq_actions_cost snode'.lower;
        upper = H.cost_add tseq_actions_cost snode'.upper;
        diff  = snode'.diff;
        sstate = TSeq {
          tseq_next = snode'.ident;
          tseq_actions; tseq_actions_cost;
        }
      }
    )
    | PSum (tsum_divider, tsum_ccost, nxl) ->
    (
      let ssl = List.map (fun (s, p) -> add_pproof t s p) nxl in
      let (lower, upper, diff) as total =
        ssl
        |> List.map
          (fun ss -> (ss.lower, ss.diff, ss.upper))
        |> List.fold_left add3 (zero3())
      in
      let tsum_queue = CostPQMax.empty() in
      List.iteri
        (fun i ss -> CostPQMax.add tsum_queue ss.diff (ss.ident, i))
        ssl;
      {
        ident; state; alive = Visited;
        lower; upper; diff;
        sstate = TSum {
          tsum_divider; tsum_ccost; tsum_queue;
          tsum_solved = [];
        }
      }
    )
    | PMin (_, nxl) ->
    (
      let len_nxl = List.length nxl in
      assert(len_nxl >= 1);
      if len_nxl = 1
      then (
        match_pproof t ident state (List.hd nxl)
      )
      else (
        let ssl = List.map (add_pproof t state) nxl in
        let tmin_queue = CostPQMin.empty() in
        List.iter
          (fun ss -> CostPQMin.add tmin_queue ss.lower ss.ident)
          ssl;
        let (tmin_upper, upper) =
             ssl
          |> MyList.opmin ~cmp:(fun ssx ssy -> Stdlib.compare ssx.upper ssy.upper)
          |> Tools.unop
          |> (fun (_, ss) -> (ss.ident, ss.upper))
        in
        let lower : H.c = CostPQMin.first tmin_queue |> fst in
        let diff : H.c = H.cost_sub upper lower in
        {
          ident; state; alive = Visited;
          lower; upper; diff;
          sstate = TMin {
            tmin_upper; tmin_queue
          }
        }
      )
    )

  let rec find_next (t:t) (ident:ident) : snode =
    (* print_string "find_next t (ident:"; print_int ident; print_string ")"; print_newline(); *)
    let snode = Hashtbl.find t.memory ident in
    if snode.alive = Solved then snode
    (*  print_string "[Solved]\n"; snode *)
    else match snode.sstate with
    | TEnd -> assert false (* TEnd.alive = Solved *)
    | TNxt tnxt -> (
      (* print_string "[TNxt]\n"; *)
      if H.cost_cmp snode.lower snode.upper = 0
      then (
        snode.alive <- Solved;
        snode
      ) else (
        let pp = H.next snode.state in
        add_pproof t snode.state ~sednxt:ident pp
      )
    )
    | TSeq tseq -> (
      (* print_string "[TSeq]\n"; *)
      let snode' = find_next t tseq.tseq_next in
      snode.lower <- H.cost_add snode'.lower tseq.tseq_actions_cost;
      snode.upper <- H.cost_add snode'.upper tseq.tseq_actions_cost;
      snode.diff  <- snode'.diff;
      assert(snode'.alive <> NotVisited);
      snode.alive <- snode'.alive;
      snode
    )
    | TSum tsum -> (
      (* print_string "[TSum]\n"; *)
      let total3 = (snode.lower, snode.diff  , snode.upper ) in
      (* if tsum.tsum_queue is empty then snode.alive = Solved *)
      let (diff', (ident', key')) = CostPQMax.pull_first tsum.tsum_queue in
      let best = Hashtbl.find t.memory ident' in
      let best3  = (best.lower  , best.diff  , best.upper  ) in
      let other3 = sub3 total3 best3 in
      let snode' = find_next t ident' in
      if snode'.alive = Solved
      then (
        tsum.tsum_solved <-
          (ident', key') :: tsum.tsum_solved;
        if CostPQMax.length tsum.tsum_queue = 0
        then snode.alive <- Solved;
        snode
      )
      else (
        CostPQMax.add tsum.tsum_queue snode'.diff (snode'.ident, key');
        let best3' = (snode'.lower, snode'.diff, snode'.upper) in
        let (lower', diff', upper') = add3 other3 best3' in
        snode.lower <- lower';
        snode.upper <- upper';
        snode.diff  <- diff';
        snode
      )
    )
    | TMin tmin -> (
      let len_q = CostPQMin.length tmin.tmin_queue in
      (* print_string ("[TMin] : {len : "^(string_of_int len_q)^"}\n"); *)
      assert(len_q >= 1);
      let lower', ident' = CostPQMin.pull_first tmin.tmin_queue in
      let snode' = find_next t ident' in
      if snode'.alive = Solved && H.cost_cmp snode'.lower lower' = 0
      then (
        snode.sstate <- TSeq {
          tseq_next = ident';
          tseq_actions = [];
          tseq_actions_cost = H.cost_zero();
        };
        find_next t ident'
      )
      else (
        (* At least one element *)
        if H.cost_cmp snode'.upper snode.upper < 0
        then (
        (* snode'.upper < snode.upper : i.e., upper bound improved *)
          tmin.tmin_upper <- snode'.ident;
          snode.upper <- snode'.upper;
          CostPQMin.shrink tmin.tmin_queue snode'.upper;
        );
        CostPQMin.add tmin.tmin_queue snode'.lower snode'.ident;
        let lower', _ = CostPQMin.first tmin.tmin_queue in
        snode.lower <- lower';
        snode.diff <- H.cost_sub snode.upper snode.lower;
        snode
      )
    )

  let rec best_solution (t:t) (ident:ident) : H.c * H.p =
    let snode = Hashtbl.find t.memory ident in
    match snode.sstate with
    | TEnd -> (H.cost_zero(), End)
    | TNxt tnxt -> (snode.lower, tnxt.tnxt_proof)
    | TSeq tseq ->
      let c, p = best_solution t tseq.tseq_next in
      let c' = H.cost_add tseq.tseq_actions_cost c in
      let p' = Seq (tseq.tseq_actions, p) in
      (c', p')
    | TSum tsum -> (
      let n =
        CostPQMax.length tsum.tsum_queue
        + List.length tsum.tsum_solved
      in
      let opa = Array.make n None in
      let cost = ref tsum.tsum_ccost in
      let recfun (ident, key) : unit =
        let c, p = best_solution t ident in
        cost := H.cost_add !cost c;
        opa.(key) <- Some p
      in
      CostPQMax.iter (fun _ -> recfun) tsum.tsum_queue;
      List.iter recfun tsum.tsum_solved;
      let sons = MyArray.unop opa |> Array.to_list in
      (!cost, Sum (tsum.tsum_divider, sons))
    )
    | TMin tmin ->
      (best_solution t tmin.tmin_upper)

  let solve (t:t) (s:H.s) : H.c * H.p =
    let root = ref (add_pproof t s (PNxt s)) in
    while !root.alive <> Solved
    do
      (* print_string "[solve] !root.ident : "; print_int !root.ident; print_newline(); *)
      root := find_next t !root.ident;
    done;
    best_solution t !root.ident
end
