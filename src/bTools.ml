(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * BTools : Toolbox for bit encoded bool array
 * (current implementation uses bytes as the underlying representation)
 *)

module BChar = BTools_BChar

module BArray =
struct
  include BTools_BArray
  module Nat =
  struct
    type nat = t
    include BTools_BArray_Nat
  end
end

type barray = BArray.t

module BNat = BArray.Nat

module OfB =
struct
  include BTools_OfB
end

module ToB =
struct
  include BTools_ToB
end

module IoB =
struct
  type stream = BinUtils.stream
  type 'a t = 'a BinUtils.o3s
  type 'a b = 'a BinUtils.o3b
  include BTools_IoB
end

module OfBStream =
struct
  include BTools_OfBStream
end

type 'a br = 'a OfBStream.t

module ToBStream =
struct
  include BTools_ToBStream
end

type 'a bw = 'a ToBStream.t

let barray_of_br (br:'a br) (bin:BArray.t) : 'a =
  let cha = OfBStream.Channel.open_barray bin in
  let a = br cha in
  OfBStream.Channel.close_barray cha;
  a

let barray_of_bw (bw:'a bw) (a:'a) : BArray.t =
  let cha = ToBStream.Channel.open_barray () in
  bw cha a;
  ToBStream.Channel.close_barray cha

type 'a b3 = 'a bw * 'a br

let barray_of_b3 (bw, br) : (_, BArray.t) O3.o3 =
  (barray_of_bw bw, barray_of_br br)

type 'a tob = 'a ToB.t
type 'a ofb = 'a OfB.t
type 'a iob = 'a IoB.t

let bw_of_tob tob cha x =
  ToBStream.bool_list cha (tob x [])

let br_of_ofb ofb cha =
  let x, tail = ofb (OfBStream.bool_list cha) in
  assert(tail = []);
  x

let b3_of_iob (tob, ofb) = (bw_of_tob tob, br_of_ofb ofb)

module IoBStream =
struct
end
