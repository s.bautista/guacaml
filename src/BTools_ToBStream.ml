(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * BTools_ToBStream [INTERNAL] : type-based conversion facility from
 *   basic types to [stream]
 *)

open STools

module Channel =
struct

  type data = {
    mutable free : bool;
    mutable pos  : int;
    mutable meta : (string -> unit) option;
  }

  let data () = {
    free = true;
    pos  = 0;
    meta = None
  }

  (* let file_channel_buffer_size = 1_024 *)
  let file_channel_buffer_size = 8

  type file_channel = {
    mutable pos : int;
    mutable buf : bool array;
            cha : Stdlib.out_channel;
  }

  type stats_channel = {
    mutable state : string list;
            table : (string list, int ref) Hashtbl.t
  }

  module BA = BTools_BArray

  type barray_channel = {
    mutable pos : int;
    mutable buf : char;
    mutable cha : Bytes.t;
    mutable tot_len : int;
    mutable tot_cha : Bytes.t list;
  }

  type channel_type =
    | Null
    | File   of file_channel
    | BArray of barray_channel
    | Tee    of channel * channel
    | Fun    of (bool -> unit)
    | Stats  of stats_channel
  and channel = data * channel_type

  type t = channel

  let get_pos ((data, _):channel) = data.pos

  let get_free ((data, _):channel) : bool = data.free

  let set_meta ((data, _):channel) anno : unit =
    data.meta <- Some anno

  let prn_meta ((data, _):channel) str : unit =
    match data.meta with
    | None -> ()
    | Some meta -> meta str

  let ppos ?(s="") cha =
    print_string s;
    print_string " [";
    print_int (get_pos cha);
    print_string "] ";
    print_newline()

  let mpos ?(s="") cha =
    let pos = string_of_int(get_pos cha) in
    prn_meta cha (s^" ["^pos^"] ")

  let open_null () = (data(), Null)
  let close_null (data, oc) =
    assert(data.free);
    match oc with
    | Null -> data.free <- false
    | _ -> (failwith "[GuaCaml.BTools_ToBStream] error")

  let open_file target : channel = (data(), File {
    pos = 0;
    buf = Array.make file_channel_buffer_size false;
    cha = Stdlib.open_out_bin target;
  })

  let open_fun f : channel = (data(), Fun f)

  let open_flush ?(cha=stdout) () = open_fun
    (fun b ->
      output_string cha (SUtils.string_of_bool b);
      Stdlib.flush cha
    )

  let lock ((data, _):channel) : unit =
    assert(data.free);
    data.free <- false

  let unlock ((data, _):channel) : unit =
    assert(not data.free);
    data.free <- true

  let close_fun ((data, cha) as chat) : unit =
    lock chat;
    match cha with
    | Fun _ -> ()
    | _ -> (failwith "[GuaCaml.BTools_ToBStream] error")

  let close_flush oc = close_fun oc

  let flush ((data, oc):channel) =
    let rec aux0 (oc:channel_type) = match oc with
      | Null -> ()
      | File foc -> (
        if foc.pos <> 0 then (
          Array.fill foc.buf foc.pos (file_channel_buffer_size-foc.pos) false;
          let b : bytes = BTools_BArray.((of_bool_array foc.buf).bytes) in
          let n = (foc.pos+7)/8 in
          Stdlib.output foc.cha b 0 n;
          foc.pos <- 0;
      ))
      | BArray bar -> (
        if bar.pos mod 8 <> 0 then (
          BA.bytes_unsafe_set bar.cha (bar.pos/8) bar.buf;
          bar.pos <- bar.pos + (8 - bar.pos);
      ))
      | Tee (chA, chB) -> aux1 chA; aux1 chB
      | Fun _ -> ()
      | Stats _ -> ()
    and     aux1 ((data, oc):channel) =
      assert(not data.free); aux0 oc
    in
    assert(data.free); aux0 oc

  let open_tee ((dataA, _) as chA) ((dataB, _) as chB) : channel =
    assert(dataA.free && dataB.free);
    dataA.free <- false;
    dataB.free <- false;
    (data(), Tee (chA, chB))

  let close_tee ((data, tee):channel) : channel * channel =
    assert(data.free);
    match tee with
    | Tee (chA, chB) ->
      data.free <- false;
      (fst chA).free <- true;
      (fst chB).free <- true;
      (chA, chB)
    | _ -> (failwith "[GuaCaml.BTools_ToBStream] error")

  let close_file ((data, cha') as cha : channel) : unit =
    assert(data.free);
    flush cha;
    match cha' with
    | File fcha -> (
      data.free <- false;
      Stdlib.close_out fcha.cha;
      ()
    )
    | _ -> ()

  let open_barray () : channel =
    let cha : barray_channel = {
      pos = 0;
      buf = '\000';
      cha = Bytes.create 1_024;
        (* [TODO] optimize value *)
      tot_len = 0;
      tot_cha = [];
    } in
    (data(), BArray cha)

  let close_barray ((data, oc):channel) : BA.t =
    assert(data.free);
    (* do not use flush, as it will modify BArray.length *)
    match oc with
    | BArray bar -> (
      data.free <- false;
      (* we flush the char-buffer into the barray-buffer *)
      if bar.pos mod 8 <> 0 then (
        BA.bytes_unsafe_set bar.cha (bar.pos/8) bar.buf
      );
      (* we create the output barray *)
      let len, out = BA.bytes_create "ToBStream.close_barray" (bar.tot_len + bar.pos) in
      (* we blit each component to its right place *)
      let rec aux start = function
        | [] -> start
        | h::t -> (
          Bytes.unsafe_blit h 0 out.BA.bytes start (Bytes.length h);
          aux (start + Bytes.length h) t
        )
      in
      (* we explore component in the reverse-reverse-order of their writting *)
      let start = aux 0 (List.rev bar.tot_cha) in
      (* we blit the remaing bits from the barray-buffer *)
      Bytes.unsafe_blit bar.cha 0 out.BA.bytes start (len - start);
      out
    )
    | _ -> (failwith "[GuaCaml.BTools_ToBStream] error")

  let open_stats () : channel =
    (data(), (Stats {state = []; table = Hashtbl.create 100}))

  let close_stats ((data, cha):channel) : (string list, int) Hashtbl.t =
    assert(data.free);
    match cha with
    | Stats stat -> (
      data.free <- false;
      assert(stat.state = []);
      let h = Hashtbl.(create (length stat.table)) in
      Hashtbl.(iter (fun k v -> add h k !v) stat.table);
      h
    )
    | _ -> (failwith "[GuaCaml.BTools_ToBStream] error")

  let stats_rem ((data, cha):channel) (tag:string) : unit =
    let rec aux0 = function
      | Null
      | File _
      | BArray _
      | Fun _            -> ()
      | Tee (cha1, cha2) -> (aux1 cha1; aux1 cha2)
      | Stats stats      -> (
        match stats.state with
        | [] -> (failwith "[GuaCaml.BTools_ToBStream] error")
        | head::state -> (
          assert(head = tag);
          stats.state <- state
        )
      )
    and    aux1 (data, cha) =
      assert(not data.free); aux0 cha
    in
    assert(data.free);
    aux0 cha;
    ()

  let stats_add ((data, cha):channel) (tag:string) : (unit -> unit) =
    let rec aux0 = function
      | Null
      | File _
      | BArray _
      | Fun _            -> ()
      | Tee (cha1, cha2) -> (aux1 cha1; aux1 cha2)
      | Stats stats      -> (
        stats.state <- tag::stats.state
      )
    and    aux1 (data, cha) =
      assert(not data.free); aux0 cha
    in
    assert(data.free);
    aux0 cha;
    (fun () -> stats_rem (data, cha) tag)
end

open Channel

type 'a t = Channel.t -> 'a -> unit

let pos cha =
  print_string " ["; print_int (get_pos cha); print_string "] "

let bool_file (foc:file_channel) b : unit =
  foc.buf.(foc.pos) <- b;
  foc.pos <- succ foc.pos;
  if foc.pos = file_channel_buffer_size
  then (
    let b : bytes = BTools_BArray.((of_bool_array foc.buf).bytes) in
    Stdlib.output_bytes foc.cha b;
    foc.pos <- 0;
  )

let bool_barray (bar:barray_channel) b : unit =
  let len = Bytes.length bar.cha lsl 3 in
  (* assert(bar.pos <= len); *)
  (* if current barray-buffer is full, store and create new one *)
  if bar.pos  = len (* bar.pos = len *)
  then (
    bar.pos <- 0;
    (* we add current buffer to the carry *)
    bar.tot_cha <- bar.cha :: bar.tot_cha;
    (* we update carry's length *)
    bar.tot_len <- bar.tot_len + len;
    bar.cha <- Bytes.create (min 8_192 (Bytes.length bar.cha lsl 1))
  );
  (* update char-buffer *)
  bar.buf <- BTools_BChar.unsafe_set bar.buf (bar.pos land 7) b;
  (* [NO BETTER] if b then (bar.buf <- Char.unsafe_chr ((Char.code bar.buf) lor (1 lsl (bar.pos land 7)))); *)
  bar.pos <- succ bar.pos;
  (* char-buffer is full, store and re-initialize *)
  if bar.pos land 7 = 0 (* bar.pos mod 8 = 0 *)
  then (
    BA.bytes_unsafe_set bar.cha (pred(bar.pos lsr 3)) bar.buf;
    bar.buf <- '\000'
  )

let bool (data, oc) b : unit =
  let rec aux0 (oc:channel_type) b : unit =
    match oc with
    | Null -> ()
    | File foc -> (bool_file foc b)
    | BArray bar -> (bool_barray bar b)
    | Tee (chA, chB) -> (aux1 chA b; aux1 chB b)
    | Fun f -> f b
    | Stats stat -> (
      match Hashtbl.find_opt stat.table stat.state with
      | Some x -> incr x
      | None -> (
        Hashtbl.add stat.table stat.state (ref 1)
      )
    )
  and     aux1 ((data, oc):channel) b : unit =
    assert(not data.free);
    data.pos <- succ data.pos;
    aux0 oc b
  in
  if not data.free then
    (invalid_arg "BinWrite.ToBStream.bool : error : locked output channel");
  data.pos <- succ data.pos;
  aux0 oc b

let sized_bool_list oc bl : unit =
  List.iter (fun b -> bool oc b) bl;
  ()
let sized_unit_list oc bl : unit = ()

let bool_option_list oc bol : unit =
  List.iter (function
    | Some b -> (bool oc false; bool oc b    )
    | None   -> (bool oc true ; bool oc false)) bol;
  bool oc true; bool oc true;
  ()

let rec unsafe_unary cha x = if x <= 0
  then (bool cha true )
  else (bool cha false; unsafe_unary cha (pred x))

let unary cha x =
  assert(x>=0);
  unsafe_unary cha x

(* write the [s] first bits of [x] on [oc] *)
let sized_int s oc x =
  let rec aux i x = if i < s
    then (bool oc (x mod 2 = 1); aux (i+1) (x/2))
    else ()
  in aux 0 x

(* assume [x >= 1] *)
(* if [x = 0] does nothing *)
let rec int_aux cha x =
  if x <= 1
    then ()
    else (
      bool cha (x mod 2 = 1);
      int_aux cha (x/2)
    )

let int_legacy oc x =
  assert(x>=0);
  if x = 0
  then (unary oc 0)
  else (
    let n = Tools.math_log2 x in
    unary oc n;
    int_aux oc x
  )

(* assume [x] >= 1 *)
let rec int_vanilla_rec cha x =
  if x = 1
  then (bool cha true)
  else (
    bool cha false;
    bool cha (x mod 2 <> 0);
    int_vanilla_rec cha (x / 2)
  )

let int_vanilla cha x =
  if x <= 0
  then bool cha true
  else (
    bool cha false;
    int_vanilla_rec cha x
  )

(*
let rec int_vanilla_barray (n:int) (bar:barray_channel) x =
  if x <= 0
  then (bool_barray bar true; (succ n))
  else (bool_barray bar false; bool_barray bar (x mod 2 <> 0); int_vanilla_barray (n+2) bar (x / 2))

let int ((data, oc) as cha) x =
  assert(x >= 0);
  match oc with
  | BArray bar -> (
    if not data.free then
      (invalid_arg "[ToBStream.int_barray] error : locked output channel");
    data.pos <- data.pos + (int_vanilla_barray 0 bar x)
  )
  | _ -> int_vanilla cha x
 *)

let int = int_vanilla

(* DEBUG *)
let debug_int oc x =
  mpos ~s:"int" oc;
  int oc x;
  mpos ~s:("int! : "^(string_of_int x)) oc;
  ()

let int = if Tools.(mode = Debug) then debug_int else int_vanilla

let rec ntimes n bw cha x =
  if n > 0 then (
    bw cha x;
    ntimes (pred n) bw cha x
  )

let unary_le top cha x =
  assert(0 <= x && x <= top);
  if x = top
  then (ntimes top bool cha false)
  else (unsafe_unary cha x)

(* more compact representation of big integers [WIP]
let int oc x =
  let rec aux0 i x = if x = 0
    then i
    else aux0 (i+1) (x/2)
  in
  let rec aux1 x = if x <= 1
    then ()
    else (bool oc (x mod 2 = 1); aux1 (x/2))
  in
  assert(x>=0);
  if x = 0
  then (bool oc true)
  else (
    int oc (aux0 0 x);
    aux1 x
  )
*)

let option (bw:'a t) oc : 'a option -> unit = function
  | None -> bool oc false
  | Some a -> (bool oc true; bw oc a)

let int_option cha opx : unit =
  int cha (Tools.int_of_int_option opx)

let int_bool_result cha r =
  int cha (Tools.int_of_int_bool_result r)

let sized_list (bw:'a t) oc (l:'a list) : unit =
  List.iter (bw oc) l

let rec sized_bool_list_le top cha l =
  match top, l with
  | [], [] -> ()
  | [], _ | _, [] -> (failwith "[GuaCaml.BTools_ToBStream] error")
  | t0::top, l0::l -> (
    if t0
    then (
      bool cha l0;
      if l0
      then sized_bool_list_le top cha l
      else sized_bool_list cha l
    )
    else (assert(l0 = false); sized_bool_list_le top cha l)
  )

let int_le top cha x =
  assert(0 <= x && x <= top);
  match top with
  | 0 -> ()
  | 1 -> (bool cha (x=1))
  | _ -> (
    let n0 = Tools.math_log2 top in
    let b0 = Tools.blist_of_int top |> List.rev in
    assert(List.hd b0 = true);
    let b0 = List.tl b0 in
    let n = Tools.math_log2 x in
    unary_le n0 cha n;
    if n = n0
    then (
      let b = Tools.blist_of_int x |> List.rev in
      assert(List.hd b = true);
      let b = List.tl b in
      sized_bool_list_le b0 cha b;
    )
    else (
      int_aux cha x
    )
  )

let int' (min:int) (max:int) cha (x:int) : unit =
  assert(min <= x && x <= max);
  int_le (max-min) cha (x-min)

let list (bw:'a t) oc (l:'a list) : unit =
  int oc (List.length l); List.iter (bw oc) l

let bool_list oc (l:bool list) : unit = list bool oc l

let none_list (bw:'a option t) oc (l:'a list) : unit =
  let rec aux = function
    | [] -> bw oc None
    | head::tail -> bw oc (Some head); aux tail
  in aux l

let array (bw:'a t) oc (a:'a array) : unit =
  int oc (Array.length a); Array.iter (bw oc) a

let pair (bwA:'a t) (bwB:'b t) oc (a, b) =
  bwA oc a; bwB oc b

let ( * ) = pair

let trio (bwA:'a t) (bwB:'b t) (bwC:'c t) oc (a, b, c) =
  bwA oc a; bwB oc b; bwC oc c

let quad (bwA:'a t) (bwB:'b t) (bwC:'c t) (bwD:'d t) oc (a, b, c, d) =
  bwA oc a; bwB oc b; bwC oc c; bwD oc d

let unit oc () = ()

let char oc c =
  for i = 0 to 7
  do
    bool oc (BTools_BChar.unsafe_get c i)
  done

let bytes oc b =
  int oc (Bytes.length b);
  for i = 0 to (Bytes.length b)-1
  do
    char oc (Bytes.unsafe_get b i)
  done

let string oc s =
  bytes oc (Bytes.unsafe_of_string s)

let float oc f =
  string oc (Float.to_string f)

let barray oc b =
  let n = BA.length b in
  int oc n;
  BA.iter (bool oc) b

let btree (bw:'a t) oc t =
  let rec aux : 'a Tree.btree -> unit = function
    | Tree.BLeaf a ->
      (bool oc true; bw oc a)
    | Tree.BNode (t0, t1) ->
      (bool oc false; aux t0; aux t1)
  in aux t

let btree_code ?(pf=None)
  (t:'a Tree.btree) : 'a t =
  let h = BTreeUtils.reverse t in
  (match pf with
    | None -> ()
    | Some pf -> Hashtbl.iter pf h
  );
  (fun oc (a:'a) ->
    sized_bool_list oc (Hashtbl.find h a);
  )

let huffman_btree ?(pf=None)
  (bw:'a t) oc (t:'a Tree.btree) =
  let cnth : ('a, int) Hashtbl.t = Tools.cnt_init ((BTreeUtils.size t)+1) in
  BTreeUtils.iter (fun _ x -> Tools.cnt_click cnth x) t;
  let wl = Tools.cnt_end cnth in
  match HuffmanCoding.huffmanize wl with
  | None -> (failwith "[GuaCaml.BTools_ToBStream] error") (* there is always at least one leaf *)
  | Some ht -> (
    OfSTree.pprint
      [BTreeUtils.to_stree (fun _ -> Tree.Leaf "") ht];
    let hc = btree_code ~pf ht in
    (* Write : Huffman's codebook *)
    btree bw oc ht;
    (* Write : encode ['a btree] *)
    btree hc oc t
  )

(* if [List.length wl = 0]
     then returns (fun _ _ -> (failwith "[GuaCaml.BTools_ToBStream] error"))
    Write Huffman Code's Book on the channel [cha]
 *)
let huffman ?(sofa=None) (bw:'a t) cha (wl:('a * int) list) : 'a t =
  let pa, pf = match sofa with
    | None -> (None, None)
    | Some ats -> (
      let pa x = Tree.Leaf (ats x) in
      let pf a bl =
        print_string (ats a); print_string " -> ";
        print_string
        BA.(to_bool_string (of_bool_list bl));
        print_string ";"; print_newline()
      in
      (Some pa, Some pf)
    )
  in
  match HuffmanCoding.huffmanize ~pa wl with
    | None -> (
      bool cha  true;
      (fun _ _ -> (failwith "[GuaCaml.BTools_ToBStream] error"))
    )
    | Some ht -> (
      bool cha false;
      (* Write : Huffman's codebook for nodes *)
      btree bw cha ht;
      btree_code ~pf ht
    )

let huffman2 ?(sofa=None) ?(post=None) (bw:'a t) cha (wl:('a * int) list) : 'a t =
  let pa, pf = match sofa with
    | None -> (None, None)
    | Some sofa -> (
      let pa x = ToSTree.(option (leaf sofa)) x in
      let pf (a:'a) (bl:bool list) =
        print_string (sofa a); print_string " -> ";
        print_string
        BA.(to_bool_string (of_bool_list bl));
        print_string ";"; print_newline()
      in
      (Some pa, Some pf)
    )
  in
  let ht, sumNone, listNone =
    HuffmanCoding.huffmanize2 ~pa wl
  in
  let codeNone = ref None in
  let hh = BTreeUtils.reverse ht in
  let hh' = Hashtbl.(create (length hh)) in
  Hashtbl.iter (fun k code -> match k with
    | None -> codeNone := Some code
    | Some k -> Hashtbl.add hh' k code) hh;
  (match pf with
    | None -> ()
    | Some pf -> Hashtbl.iter pf hh'
  );
  let codeNone = match !codeNone with
    | None -> (failwith "[GuaCaml.BTools_ToBStream] error")
      (* [None] has no associated code *)
    | Some codeNone -> codeNone
  in
  btree (option bw) cha ht;
  let encodeNone : 'a t = match post with
    | None -> bw
    | Some post -> post listNone
  in
  let encode cha a =
    try
      let code = Hashtbl.find hh' a in
      prn_meta cha (">HC-S> "^(string_of_int(List.length code)));
      sized_bool_list cha code
    with Not_found ->
      prn_meta cha (">HC-N> "^(string_of_int(List.length codeNone)));
      sized_bool_list cha codeNone;
      encodeNone cha a
  in
  encode

let dag_core
  (find : 'ident -> int option)
  (add : 'ident -> int -> unit)
  (get_ident : 'link -> 'ident)
  (recfun : channel ->
    ('link -> int -> int) ->
     'link -> int -> int
  )
  (bw_ident : int -> int t)
  (cha : channel) : 'link -> int -> int =
  let rec rec_link (link:'link) (nnode:int) : int =
    match find (get_ident link) with
    | Some ident -> (
      bool cha true;
      bw_ident nnode cha ident;
      nnode
    )
    | None -> (
      add (get_ident link) nnode;
      bool cha false;
      recfun cha rec_link link (succ nnode)
    )
  in rec_link

let ctx_dag_core
  (find : 'ctx -> 'ident -> int option)
  (add : 'ctx -> 'ident -> int -> unit)
  (get_ident : 'link -> 'ident)
  (recfun : channel ->
    ('ctx -> 'link -> int -> int) ->
     'ctx -> 'link -> int -> int
  )
  (bw_ident : 'ctx -> int -> int t)
  (cha : channel) : 'ctx -> 'link -> int -> int =
  let rec rec_link (ctx:'ctx) (link:'link) (nnode:int) : int =
    assert(get_free cha);
    match find ctx (get_ident link) with
    | Some ident -> (
      bool cha true;
      bw_ident ctx nnode cha ident;
      nnode
    )
    | None -> (
      add ctx (get_ident link) nnode;
      bool cha false;
      recfun cha rec_link ctx link (succ nnode)
    )
  in rec_link

let hctx_dag_core
  (find : 'ctx -> 'ident -> int option)
  (add : 'ctx -> 'ident -> int -> unit)
  (get_ident : 'link -> 'ident)
  (recfun : channel ->
    ('ctx -> 'link -> unit) ->
     'ctx -> 'link -> unit
  )
  (bw_ident : 'ctx -> int -> int t)
  (curr_nnode : 'ctx -> int)
  (incr_nnode : 'ctx -> int)
  (cha : channel) : 'ctx -> 'link -> unit =
  let rec rec_link (ctx:'ctx) (link:'link) : unit =
    assert(get_free cha);
    match find ctx (get_ident link) with
    | Some ident -> (
      bool cha true;
      let nnode = curr_nnode ctx in
      bw_ident ctx nnode cha ident;
    )
    | None -> (
      let nnode = incr_nnode ctx in
      add ctx (get_ident link) nnode;
      bool cha false;
      recfun cha rec_link ctx link
    )
  in rec_link

let hctx2_dag_core
  (find : 'ctx -> 'ident -> int option)
  (add : 'ctx -> 'ident -> int -> unit)
  (get_ident : 'link -> 'ident)
  (recfun : channel ->
    ('ctx -> 'link -> unit) ->
     'ctx -> 'link -> unit
  )
  (*
    Some n -> pointer to the n-th node
    None   -> pointed node
   *)
  (bw_ident : 'ctx -> int -> int option t)
  (curr_nnode : 'ctx -> int)
  (incr_nnode : 'ctx -> int)
  (cha : channel) : 'ctx -> 'link -> unit =
  let rec rec_link (ctx:'ctx) (link:'link) : unit =
    assert(get_free cha);
    match find ctx (get_ident link) with
    | Some ident -> (
      let nnode = curr_nnode ctx in
      bw_ident ctx nnode cha (Some ident);
    )
    | None -> (
      let nnode = incr_nnode ctx in
      add ctx (get_ident link) nnode;
      bw_ident ctx nnode cha None;
      recfun cha rec_link ctx link
    )
  in rec_link

let hctx3_dag_core
  (find : 'ctx -> 'ident -> (int, bool) result)
  (add : 'ctx -> 'ident -> int -> unit)
  (get_ident : 'link -> 'ident)
  (recfun : channel ->
    ('ctx -> 'link -> unit) ->
     'ctx -> 'link -> unit
  )
  (*
    Some n -> pointer to the n-th node
    None   -> pointed node
   *)
  (bw_ident : 'ctx -> int -> (int, bool) result t)
  (curr_nnode : 'ctx -> int)
  (incr_nnode : 'ctx -> int)
  (cha : channel) : 'ctx -> 'link -> unit =
  let rec rec_link (ctx:'ctx) (link:'link) : unit =
    assert(get_free cha);
    let info = find ctx (get_ident link) in
    let nnode = curr_nnode ctx in
    bw_ident ctx nnode cha info;
    match info with
    | Ok _ -> ()
    | Error toadd -> (
      if toadd then (
        ignore(incr_nnode ctx);
        add ctx (get_ident link) nnode
      );
      recfun cha rec_link ctx link
    )
  in rec_link

let ofbstream (wcha:channel) (rcha:BTools_OfBStream.Channel.t) : unit =
  try
    while true
    do
      let b = BTools_OfBStream.bool rcha in
      bool wcha b;
    done;
  with
    | BTools_OfBStream.Channel.End_of_OfBStream _ -> ()

(* assumes [sum = \sum_{i\in il} i] *)
let rec sum_list ?(min=0) (sum:int) cha (il:int list) : unit =
  match il with
  | [] -> assert(sum = 0)
  | i::il -> (
    assert(i>=min);
    int' min sum cha i;
    sum_list ~min (sum-i) cha il
  )
