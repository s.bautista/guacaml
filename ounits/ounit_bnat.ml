open OUnit
open BTools
open BArray
open Nat

let test_of_bicimal_gen bici bici_string () : unit =
  let bnat = of_bicimal bici in
  let bnat_string = to_string bnat in
  error_assert
    "bnat_string = bici_string"
    ~func:(fun()-> Some(
      let open STools.ToS in
      "bici:"^(list int bici)^"\n"^
      "bici_string:"^(string bici_string)^"\n"^
      "bnat_string:"^(string bnat_string)
    ))
    (bnat_string = bici_string);
  let bici_bnat = to_bicimal bnat in
  error_assert
    "bici = bici_bnat"
    (bici = bici_bnat);
  ()

let of_bicimal_tests : (int list * string) array = [|
  ([1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 14; 15; 18], "315390");
  ([2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 14; 15; 19], "577532");
  ([12; 14; 15; 17], "184320");
  ([12; 14; 15; 17; 32], "4295151616");
  ([1; 2; 3; 5; 6; 8; 11; 12; 13; 17; 18; 20; 21; 25; 26; 29; 31; 33; 35; 37; 39; 44; 47; 51; 56; 58; 64; 72; 76], "80299039651690288920942");
|]

let test_of_bicimal_indexed i () =
  let bici, bici_string = of_bicimal_tests.(i) in
  test_of_bicimal_gen bici bici_string ()

let _ =
  Array.iteri (fun i (bici, bici_string) ->
    let tag = OUnit.ToS.("["^(int i)^"]") in
    push_test
        ("test_of_bicimal_"^tag^"_"^bici_string)
        (test_of_bicimal_gen bici bici_string)
    ) of_bicimal_tests

let _ = run_tests()
