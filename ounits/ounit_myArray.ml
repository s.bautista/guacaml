open OUnit
open MyArray

(* UNIT TEST : [map_of_list : ('a -> 'b) -> 'a list -> 'b array] *)

let test_map_of_list_00 () =
  let l = [] in
  let f = succ in
  let r = map_of_list f l in
  error_assert
    "r = [||]"
    (r = [||]);
  ()

let _ = push_test "test_map_of_list_00" test_map_of_list_00

let test_map_of_list_01 () =
  let l = [0] in
  let f = succ in
  let r = map_of_list f l in
  error_assert
    "r = [|1|]"
    (r = [|1|]);
  ()

let _ = push_test "test_map_of_list_01" test_map_of_list_01

let test_map_of_list_02 () =
  let l = [0; 1] in
  let f = succ in
  let r = map_of_list f l in
  error_assert
    "r = [|1; 2|]"
    (r = [|1; 2|]);
  ()

let _ = push_test "test_map_of_list_02" test_map_of_list_02

let test_map_of_list_03 () =
  let l = [0; 1; 2] in
  let f = succ in
  let r = map_of_list f l in
  error_assert
    "r = [|1; 2; 3|]"
    (r = [|1; 2; 3|]);
  ()

let _ = push_test "test_map_of_list_03" test_map_of_list_03

(* UNIT TEST : [map_of_rev_list : ('a -> 'b) -> 'a list -> 'b array] *)

let test_map_of_rev_list_00 () =
  let l = [] in
  let f = succ in
  let r = map_of_rev_list f l in
  error_assert
    "r = [||]"
    (r = [||]);
  ()

let _ = push_test "test_map_of_rev_list_00" test_map_of_rev_list_00

let test_map_of_rev_list_01 () =
  let l = [0] in
  let f = succ in
  let r = map_of_rev_list f l in
  error_assert
    "r = [|1|]"
    (r = [|1|]);
  ()

let _ = push_test "test_map_of_rev_list_01" test_map_of_rev_list_01

let test_map_of_rev_list_02 () =
  let l = [0; 1] in
  let f = succ in
  let r = map_of_rev_list f l in
  error_assert
    "r = [|2; 1|]"
    (r = [|2; 1|]);
  ()

let _ = push_test "test_map_of_rev_list_02" test_map_of_rev_list_02

let test_map_of_rev_list_03 () =
  let l = [0; 1; 2] in
  let f = succ in
  let r = map_of_rev_list f l in
  error_assert
    "r = [|3; 2; 1|]"
    (r = [|3; 2; 1|]);
  ()

let _ = push_test "test_map_of_rev_list_03" test_map_of_rev_list_03

(* UNIT TEST : [rev : 'a array -> 'a array] *)

let test_rev_00 () =
  let a = [| |] in
  let rev_a = rev a in
  error_assert "a = [| |]" (a = [| |]);
  error_assert "rev_a = [| |]" (rev_a = [| |]);
  ()

let _ = push_test "test_rev_00" test_rev_00

let test_rev_04 () =
  let a = [|0; 1; 2; 3; 4|] in
  let rev_a = rev a in
  error_assert "rev_a != a" (rev_a != a);
  error_assert
    "a = [|0; 1; 2; 3; 4|]"
    (a = [|0; 1; 2; 3; 4|]);
  error_assert
    "rev_a = [|4; 3; 2; 1; 0|]"
    (rev_a = [|4; 3; 2; 1; 0|]);
  ()

let _ = push_test "test_rev_04" test_rev_04

(* test generator *)
let test_rev_range_gen n () =
  let a1 = Array.init n (fun i -> i) in
  let rev1_a1 = rev a1 in
  error_assert "rev1_a1 != a1" (rev1_a1 != a1);
  let a2 = Array.init n (fun i -> i) in
  error_assert "a1 = a2" (a1 = a2);
  let rev2_a1 =
    a1
    |> Array.to_list
    |> List.rev
    |> Array.of_list
  in
  error_assert "rev1_a1 = rev2_a1" (rev1_a1 = rev2_a1);
  ()

let _  =
  let n = 100 in
  for k = 1 to n (* avoid k = 0, where physical inequality is
                    not ensured *)
  do
    push_test
      ("test_rev_range_1000_["^(string_of_int k)^"]")
      (test_rev_range_gen k)
  done;
  ()

(* UNIT TEST : [is_permut_check_bound : int array -> bool] *)
let test_is_permut_00 () =
  let a = [|0; 1; 2; 3; 4|] in
  let b = is_permut a in
  error_assert
    "a = [|0; 1; 2; 3; 4|]"
    (a = [|0; 1; 2; 3; 4|]);
  error_assert
    "b = true"
    (b = true);
  ()

let _ = push_test
  "test_is_permut_00"
   test_is_permut_00

let test_is_permut_01 () =
  let a = [|0; 1; 2; 3; 5|] in
  let b = is_permut a in
  error_assert
    "a = [|0; 1; 2; 3; 5|]"
    (a = [|0; 1; 2; 3; 5|]);
  error_assert
    "b = false"
    (b = false);
  ()

let _ = push_test
  "test_is_permut_01"
   test_is_permut_01

let test_is_permut_02 () =
  let a = [| |] in
  let b = is_permut a in
  error_assert
    "a = [| |]"
    (a = [| |]);
  error_assert
    "b = true"
    (b = true);
  ()

let _ = push_test
  "test_is_permut_02"
   test_is_permut_02

(* UNIT TEST : [unop_rename: 'a option array -> ('a array) * (int array)] *)

let test_unop_rename_00 () =
  let opa = [|Some 'a'; None; Some 'b'; None; None; Some 'c'|] in
  let res_unop, res_rename = unop_rename opa in
  let sol_unop   = [|'a'; 'b'; 'c'|] in
  let sol_rename = [|0; -1; 1; -1; -1; 2|] in
  error_assert
    "opa = [|Some 'a'; None; Some 'b'; None; None; Some 'c'|]"
    (opa = [|Some 'a'; None; Some 'b'; None; None; Some 'c'|]);
  error_assert
    "res_unop = sol_unop"
    (res_unop = sol_unop);
  error_assert
    "res_rename = sol_rename"
    (res_rename = sol_rename);
  ()

let _ = push_test
  "test_unop_rename_00"
   test_unop_rename_00

let _ = run_tests()
