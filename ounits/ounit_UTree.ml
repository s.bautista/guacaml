open BTools
open STools

open UTree
open OUnit

let _ =
  let ba = BArray.of_string "11110100" in
  let h = UTree.create ~random:false () in
  let k1 = UTree.push h ba in
  let k2 = UTree.push h ba in
  assert(k1 = k2);
  ()

