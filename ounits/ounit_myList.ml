open OUnit
open MyList

(* UNIT TEST :
  [collapse_first : ('a * 'b) list -> ('a * ('b list)) list]
 *)
let test_collapse_first_00 () =
  let l = [] in
  let r = collapse_first l in
  error_assert "r = []" (r = []);
  ()

let _ = push_test
  "test_collapse_first_00"
   test_collapse_first_00

let test_collapse_first_01 () =
  let l = [(1, 2); (1, 3); (2, 4); (1, 5); (1, 2)] in
  let r = collapse_first l in
  error_assert
    "r = [(1, [2; 3]); (2, [4]); (1, [5; 2])]"
    (r = [(1, [2; 3]); (2, [4]); (1, [5; 2])]);
  ()

let _ = push_test
  "test_collapse_first_01"
   test_collapse_first_01

let indexes_true_tests_len12 = [|
  ([true; false; false; true; false; false; false; false; false; false; false; false], [0; 3]);
  ([true; false; false; false; true; false; false; false; false; false; false; false], [0; 4]);
  ([false; false; false; true; true; false; false; false; false; false; false; false], [3; 4]);
  ([false; true; false; true; true; false; false; false; false; false; false; false], [1; 3; 4]);
  ([false; true; false; true; false; false; false; false; false; false; false; false], [1; 3]);
  ([false; false; true; true; true; false; false; false; false; false; false; false], [2; 3; 4]);
  ([false; false; true; false; true; true; false; false; false; false; false; false], [2; 4; 5]);
  ([false; false; false; false; false; true; false; false; true; true; false; false], [5; 8; 9]);
  ([false; false; false; false; false; false; false; false; true; true; true; false], [8; 9; 10]);
  ([false; false; false; false; false; false; false; false; true; false; true; true], [8; 10; 11]);
  ([false; false; false; false; false; false; false; true; true; false; false; true], [7; 8; 11]);
  ([false; false; false; false; false; true; true; false; false; false; false; false], [5; 6]);
  ([false; false; false; false; false; false; true; true; false; false; false; false], [6; 7]);
  ([true; false; false; true; true; false; false; false; false; false; false; false], [0; 3; 4]);
  ([false; false; false; false; false; false; false; false; false; false; false; false], []);
|]

let test_indexes_gen bl il () =
  let il' = indexes (fun x -> x) bl in
  error_assert
    "il' = il"
    (il' = il);
  ()

let test_indexes_true_gen bl il () =
  let il' = indexes_true bl in
  error_assert
    "il' = il"
    (il' = il);
  ()

let test_of_sorted_indexes_gen bl il () =
  let len = List.length bl in
  let bl' = of_sorted_indexes len il in
  error_assert
    "bl' = bl"
    (bl' = bl);
  ()

let test_of_indexes_true_gen bl il () =
  let len = List.length bl in
  let bl' = of_indexes ~sorted:true len il in
  error_assert
    "bl' = bl"
    (bl' = bl);
  ()

let test_of_indexes_false_gen bl il () =
  let len = List.length bl in
  let bl' = of_indexes ~sorted:false len il in
  error_assert
    "bl' = bl"
    (bl' = bl);
  ()

let test_poll_indexes = [|
  (test_indexes_gen,
  "test_indexes_gen");
  (test_indexes_true_gen,
  "test_indexes_true_gen");
  (test_of_sorted_indexes_gen,
  "test_of_sorted_indexes_gen");
  (test_of_indexes_true_gen,
  "test_of_indexes_true_gen");
  (test_of_indexes_false_gen,
  "test_of_indexes_false_gen");
|]

let _ =
  Array.iteri (fun i (bl, il) ->
    Array.iteri (fun j (compute, compute_name) ->
      let tag = OUnit.ToS.("["^(int i)^","^(int j)^"]") in
      push_test
        ("test_"^tag^"_"^compute_name)
        (compute bl il)
      ) test_poll_indexes
    ) indexes_true_tests_len12

(* UNIT TEST [of_sorted_indexes : int -> int list -> bool list] *)
(* UNIT TEST [of_indexes : ?sorted:bool -> int -> int list -> bool list] *)

(* UNIT TEST [lexsort : ... ] *)

let test_lexsort_00 () =
  let input = [([1; 2; 3; 4; 5; 10; 11; 12; 14; 15; 16; 17], 2)] in
  let expected_output = [[2]] in
  let output = lexsort Stdlib.compare input in
  error_assert
    "output = expected_output"
    (output = expected_output)

let _ = push_test
  "test_lexsort_00"
   test_lexsort_00

let _ = run_tests()
