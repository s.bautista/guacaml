open BTools
open STools

open BRLE
open OUnit

let data_of_int_list = [ (* list of (length, il) *)
  (0, []);
  (1, []);
  (5, []);
  (5, [0; 2; 4]);
  (5, [0; 1; 3; 4]);
(*  (281, [0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 21; 22; 23; 24; 25; 26; 27; 28; 29; 30; 31; 32; 33; 34; 35; 36; 37; 38; 39; 41; 42; 43; 44; 45; 46; 47; 48; 49; 50; 51; 52; 53; 54; 55; 56; 57; 58; 60; 61; 62; 63; 64; 65; 66; 67; 68; 69; 70; 71; 72; 73; 74; 75; 76; 77; 78; 79; 80; 81; 82; 83; 84; 85; 86; 87; 88; 89; 90; 91; 92; 93; 94; 95; 96; 97; 98; 99; 100; 102; 103; 104; 105; 106; 107; 108; 109; 110; 111; 112; 113; 114; 115; 116; 117; 118; 119; 120; 121; 122; 123; 124; 125; 126; 127; 129; 130; 131; 132; 133; 134; 135; 136; 137; 138; 139; 140; 141; 142; 143; 144; 145; 146; 147; 149; 150; 151; 152; 153; 154; 155; 156; 157; 158; 159; 160; 161; 162; 163; 164; 165; 166; 167; 168; 169; 170; 171; 172; 173; 174; 175; 176; 177; 178; 179; 180; 181; 182; 183; 184; 185; 186; 187; 188; 190; 191; 192; 193; 194; 195; 196; 197; 198; 199; 200; 201; 202; 203; 204; 205; 206; 207; 208; 209; 210; 211; 212; 213; 214; 215; 217; 218; 219; 220; 221; 222; 223; 224; 225; 226; 227; 228; 229; 230; 231; 232; 233; 234; 235; 237; 238; 239; 240; 241; 242; 243; 244; 245; 246; 247; 248; 249; 250; 251; 252; 253; 254; 255; 256; 257; 258; 259; 260; 261; 262; 263; 264; 265; 266; 267; 268; 269; 270; 271; 272; 273; 274; 276; 277; 278; 279; 280]); *)
]

let test_of_bool_list_00_gen (len:int) (il:int list) () : unit =
    let bl = MyList.of_indexes len il in
    let x = reduce bl in
    let bl' = expand x in
    error_assert "bl' = bl" (bl' = bl);
    ()

let _ =
  List.iteri (fun i (len, il) ->
    push_test
      ("test_of_bool_list_00["^(ToS.int i)^"]")
      ( test_of_bool_list_00_gen len il   )
  ) data_of_int_list

let test_of_int_list_00_gen (len:int) (il:int list) () : unit =
    let x = of_int_list len il in
    let il' = to_int_list x in
    error_assert "il' = il" (il' = il);
    ()

let _ =
  List.iteri (fun i (len, il) ->
    push_test
      ("test_of_int_list_00["^(ToS.int i)^"]")
      ( test_of_int_list_00_gen len il   )
  ) data_of_int_list

let data_count_both = [
  (None, 0, 0);
  (Some(false, [45]), 45, 0);
  (Some(true , [1; 350]), 350, 1);
]

let test_count_both_00_gen (t:t) (n0:int) (n1:int) () : unit =
  error_assert "count false t = n0" (count false t = n0);
  error_assert "count true  t = n0" (count true  t = n1);
  ()

let _ =
  List.iteri (fun i (t, n0, n1) ->
    push_test
      ("test_count_both_00["^(ToS.int i)^"]")
      ( test_count_both_00_gen t n0 n1  )
  ) data_count_both

let data_hdtl_nth = [
  (8, Some (false, [8; 1; 184]), Some (false, [8]), Some (true, [1; 184]));
]

let test_hdtl_nth_00_gen n t hd_ref tl_ref () : unit =
  let hd, tl = hdtl_nth n t in
  error_assert "hd = hd_ref" (hd = hd_ref);
  error_assert "tl = tl_ref" (tl = tl_ref);
  ()

let _ =
  List.iteri (fun i (n, t, hd_ref, tl_ref) ->
    push_test
      ("test_hdtl_nth_00["^(ToS.int i)^"]")
      ( test_hdtl_nth_00_gen n t hd_ref tl_ref)
  ) data_hdtl_nth

let _ = run_tests()
