(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *)

open OUnit

let test_prettyfy_00 () =
  let inp = "1234567890" in
  let out = "1_234_567_890" in
  let out' = Tools.prettyfy_nat inp in
  if out' <> out then (
    print_endline ("inp :"^(ToS.string inp));
    print_endline ("out :"^(ToS.string out));
    print_endline ("out':"^(ToS.string out'));
  );
  error_assert "out' = out" (out' = out);
  ()

let _ = push_test
  "test_prettyfy_00"
   test_prettyfy_00

let _ = run_tests()
