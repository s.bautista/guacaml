# GuaCaml : Generic Unspecific Algorithmic in OCaml

GuaCaml is a library extending the standard library of OCaml by various methods that are natively implemented in other languages.
In particular, it includes several facilities allowing for the conversion of arbitrary types into specific types used when exchanging, displaying of storing information.
All modules are implemented using pure OCaml, thus do not require knowledge of third party programming languages such as C.
The implementation aims at finding a compromise between the development effort, the genericity and performances, either computation time or memory consumption.

# Contribution

All contributions are welcome, that is adding functionalities, documentation, tutorial, fixing typos, etc.
Unless specified otherwise all contributions are made under the terms of the INDIVIDUAL CONTRIBUTOR LICENSE AGREEMENT (that you can find on the eponym file) (ICLA).
Just so you know, the ICLA does not prevent you from reusing, nor re-licensing your code elsewhere including under commercial licenses.


## I want to add a single tiny function but I don't know where to put it.

Ok, no worry, as I said all contributions are welcome.
The first thing you can do is just guessing in which file it should go, worst case it was not the good one the development team will point you the right file or just tell you to create a new one, with naming possibilities.
Otherwise, just create a file named `avocado.ml` put your function inside it along with appropriate description and relevant unit tests, then press the __merge request__ button and that is it.
Optionally, you can add your name at the beginning as follows :
```ocaml
(* @FirstnameLastname
 * [my_function a b = c] where
 * [a:int] is an integer
 * [b:int] is a non-negative integer
 * [c:int list] is the list of number between [a] and [b] (both included)
 *)
```
Otherwise just put `@Avocado` to remain anonymous.
All functions with unspecified contributor have been written by the main contributor(s) of the file (mentioned in the header of this file), which is I (Joan Thibault) if not mentioned otherwise.

## Install


### Via `opam`

Using the package manager [opam](https://opam.ocaml.org/),

```shell
	opam install guacaml
```


To install the current development version:

```shell
	opam pin add https://gitlab.com/boreal-ldd/guacaml.git#master
```


### From Sources

Installation command for GuaCaml

```
cd guacaml
make install
```

#### Compilation Error

By default, should any compilation error occur, first try cleaning the compilation unit and start again:
```
make clean
make install
```

##### Inconsistent Library
if ocamlbuild returns an error containing the following message:
```
make inconsistent assumptions
```

cf. clean the compilation unit and restart
if the same problem persists contact the development team


##### Another Error ? 
First, make sure that your current version is up to date, clean the compilation unit and restart:
If your repository is up to date and you already tried cleaning the compilation unit, then contact the development team.
