# release-0.04 [CURRENT]

# 2021/11/20 Naive MinMax Algorithm (@author Santiago Bautista)
- **NEW** added Naive MinMax algorithm (file `minmax.ml`)

# release-0.03

## 2021/10/05 : Added Signed Int to ToB and OfB (in BTools)

## 2021/09/28 : Boolean RLE
- **NEW** BRLE module (Boolean Run Length Encoding)
- **NEW** added marginal ounit testing

## 2021/04/02 : Single-Hash (hash)Table

## 2021/03/12 : DBBC-related improvements
- **NEW** added auto-profiling functor
- **FIX** fixed Sum snode in DBBC

## 2021/03/09 : wap-related improvements
- **FIX** fixed some bugs in DBBC
- **FIX** set name convention related to 'neighborhood' vs 'neighbors' to 'neighbors'
- **NEW** added `is_clique` to GGLA

## 2021/03/02 : added wap-specific methods and functions

- **NEW** Branch And Bound
- **FIX** DBBC
- **NEW** GGLA clique detection module
- **NEW** GGLA domination
- **NEW** GGLA utils
- **NEW** GGLA vertex separators
- **WIP** GGLA (various work in progress)
- **DEL** Graph Iso Adj
- **DEL** Graph WLA

## 2021/02/27 : implementing RLE

## 2020/11/02 : fixed CHANGE with respect to release-0.02

# release-0.02

## 2020/11/02 : reformatting assertion as named failwith

## 2020/11/01 : added `Tools.opopmax`

## 2020/10/31 : added `OUnit.{print{,_endline}}`

## 2020/10/31 : added a `__check_reverse__` mechanism in `MemoBTable`

## 2020/10/24 : added `Tools.opiter` to `tools.ml`

## 2020/10/13 : added `print_error_endline` to OUnit, imported back some OUnit tests (+ `ounits_bin.sh` and `make ounits`)

## 2020/10/15 : minor improvement in MemoTable.apply

## 2020/10/14 : added Tools.map2, linked MyList.{map, map2} to Tools.{map, map2}

## 2020/10/11 : fixed Assoc.restr

## 2020/10/11 : fixed `wap_rbtf.BPP` (s/supp3/supp2) + removed unnecessary tests

## 2020/10/10 : added Assoc module : manipulation of sorted association list

## 2020/10/09 : added `roman_to_string` in STools.SUtils

## 2020/10/07 : added `apply_haltf3` to `wap_rbtf`

## 2020/10/07 : added filter{A,B} (resp. {GLeaf,GLink}) to AB (resp. TreeUtils)

## 2020/10/06 : fixed error in `wap_exchange_utils.update_tree`, issue with parameter variables

## 2020/10/05 : fixed error in `GGLA.Utils.internal_vertex_edges_rename` (v.index renaming)

## 2020/10/05 : fixed error in SetList.minus

## 2020/10/05 : added ounit\_setlist

## 2020/10/05 : added rev\_flatten to MyList.mli (already in .ml)

## 2020/10/05 : added inclusion test in wap\_exchange\_utils.{check\_input,assert\_input}

## 2020/10/05 : added WAP exchange interface [REMOVED]
-	`wap_exchange.ml`
-	`wap_exchange_utils.ml`
-	`wap_greedy.ml`
-	`wap_rbtf.ml`
-	`wap_rbtf.mli`

## 2020/10/05 : added generic graph modules

# release-0.01
FIXME : many things
