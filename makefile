# LGPL-3.0 Linking Exception
#
# Copyright (c) 2018-2020 Joan Thibault (joan.thibault@irisa.fr)
#
# GuaCaml : Generic Unspecific Algorithmic in OCaml

NPROC=$(shell nproc || echo 1)

OB=ocamlbuild -r -j $(NPROC) -use-ocamlfind

.PHONY: all install uninstall ounits clean

all :
	$(OB) \
		GuaCaml.cma \
		GuaCaml.cmi \
		GuaCaml.cmx \
		GuaCaml.cmxa \
		GuaCaml.cmxs

install : all
	ocamlfind remove  GuaCaml
	ocamlfind install GuaCaml META _build/GuaCaml.*

uninstall : all
	ocamlfind remove  GuaCaml

ounits:
	$(OB) -I src $(shell ls ounits/ounit_*.ml | sed 's/ml/native/')
	rm -rf ounits_bin/ &> /dev/null || true
	mkdir ounits_bin/ || true
	mv *.native *.byte ounits_bin/ &> /dev/null || true
	./ounits_bin.sh

clean:
	ocamlbuild -clean

